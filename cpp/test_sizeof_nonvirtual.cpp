#include <iostream>

class A
{
    int a;
    int b;
    char c;
    char d;
};

class B
{
    int a;
    char c;
    int b;
    char d;
};

int main()
{
    std::cout << "sizeof A: " << sizeof(A) << std::endl;
    std::cout << "sizeof B: " << sizeof(B) << std::endl;
}

