#include "RsaEncryptionFactory.hpp"
#include "RsaEncryption.hpp"

#include <openssl/conf.h>
#include <openssl/err.h>
#include <openssl/rand.h>


namespace ue
{

RsaEncryptionFactory::RsaEncryptionFactory() = default;
RsaEncryptionFactory::~RsaEncryptionFactory() = default;

IEncoderPtr RsaEncryptionFactory::getEncoder()
{
    return std::make_shared<RsaEncryption>(myKey);
}

IDecoderPtr RsaEncryptionFactory::getDecoder()
{
    return std::make_shared<RsaEncryption>(myKey);
}

IDecoderPtr RsaEncryptionFactory::readDecoder(common::IncomingMessage &reader)
{
    return std::make_shared<RsaEncryption>(RsaDetails::KeyPair::read(reader));
}

IEncoderPtr RsaEncryptionFactory::readEncoder(common::IncomingMessage &reader)
{
    return std::make_shared<RsaEncryption>(RsaDetails::KeyPair::read(reader));
}

RsaEncryptionFactory::ErrorStringsGuard::ErrorStringsGuard()
{
    ERR_load_crypto_strings();
}

RsaEncryptionFactory::ErrorStringsGuard::~ErrorStringsGuard()
{
    ERR_free_strings();
}

RsaEncryptionFactory::LibInitGuard::LibInitGuard()
{
    OpenSSL_add_all_algorithms();
//    OPENSSL_config(NULL);
}

RsaEncryptionFactory::LibInitGuard::~LibInitGuard()
{
    /* Removes all digests and ciphers */
    EVP_cleanup();
    /* if you omit the next, a small leak may be left when you make use of the BIO (low level API) for e.g. base64 transformations */
    CRYPTO_cleanup_all_ex_data();
}

}
