#include "BtsPort.hpp"
#include "Messages/IncomingMessage.hpp"
#include "Crypto/NoneEncryption.hpp"
#include <map>

namespace ue
{

BtsPort::BtsPort(IBtsReceiver& receiver,
                 common::PhoneNumber phoneNumber,
                 IEncryptionFactory &encryptionFactory,
                 common::ITransport& ITransport,
                 common::ILogger &iLogger)
    : receiver(receiver),
      phoneNumber(phoneNumber),
      encryptionFactory(encryptionFactory),
      transport(ITransport),
      logger(iLogger, [this](std::ostream& os) { printPrefix(os); })
{
    logger.logInfo("Started");
    transport.registerMessageCallback([this](BinaryMessage message)
    {
        receiveMessage(std::move(message));
    });
}

BtsPort::~BtsPort()
{
    transport.registerMessageCallback([](BinaryMessage message)
    {
        // ignoring
    });
    logger.logInfo("Stopped");
}

class BtsPort::MessageDispatcher
{
public:
    MessageDispatcher(const common::BinaryMessage& message, IEncryptionFactory& encryptionFactory, common::ILogger& logger);

    void dispatchTo(IBtsReceiver& receiver);

private:
    using MessageReader = void (MessageDispatcher::*)(IBtsReceiver& receiver);
    using MessageReaders = std::map<common::MessageId, MessageReader>;

    void readSib(IBtsReceiver& receiver);
    void readAttachResponse(IBtsReceiver& receiver);
    void readSms(IBtsReceiver& receiver);
    void readCallRequest(IBtsReceiver& receiver);
    void readCallAccept(IBtsReceiver& receiver);
    void readCallTalk(IBtsReceiver& receiver);
    void readCallDrop(IBtsReceiver& receiver);
    bool readEncryptionDeprecated();
    IDecoderPtr readDecoder();
    IEncoderPtr readEncoder();

    static MessageReaders messageReaders;
    common::IncomingMessage reader;
    common::MessageHeader header;
    IEncryptionFactory &encryptionFactory;
    common::ILogger& logger;
};

BtsPort::MessageDispatcher::MessageReaders BtsPort::MessageDispatcher::messageReaders{
    {common::MessageId::Sib, &BtsPort::MessageDispatcher::readSib },
    {common::MessageId::AttachResponse, &BtsPort::MessageDispatcher::readAttachResponse },
    {common::MessageId::Sms, &BtsPort::MessageDispatcher::readSms },
    {common::MessageId::CallRequest, &BtsPort::MessageDispatcher::readCallRequest },
    {common::MessageId::CallAccepted, &BtsPort::MessageDispatcher::readCallAccept },
    {common::MessageId::CallDropped, &BtsPort::MessageDispatcher::readCallDrop },
    {common::MessageId::CallTalk, &BtsPort::MessageDispatcher::readCallTalk }
};

void BtsPort::receiveMessage(common::BinaryMessage message)
{
    MessageDispatcher dispatcher(message, encryptionFactory, logger);
    dispatcher.dispatchTo(receiver);
}

void BtsPort::printPrefix(std::ostream &os) const
{
    os << "[BTS] ";
}

BtsPort::MessageDispatcher::MessageDispatcher(const common::BinaryMessage &message, IEncryptionFactory &encryptionFactory, common::ILogger &logger)
    : reader(message),
      header(reader.readMessageHeader()),
      encryptionFactory(encryptionFactory),
      logger(logger)
{
    logger.logDebug("Receiving: ", header.messageId, ", from: ", header.from);
}

void BtsPort::MessageDispatcher::dispatchTo(IBtsReceiver &receiver)
{
    auto it = messageReaders.find(header.messageId);
    if (it == messageReaders.end())
    {
        logger.logInfo("Not implemented handler for: ", header.messageId);
        return;
    }

    (this->*(it->second))(receiver);
}

void BtsPort::MessageDispatcher::readSib(IBtsReceiver &receiver)
{
    receiver.handleSib();
}

void BtsPort::MessageDispatcher::readAttachResponse(IBtsReceiver &receiver)
{
    bool accepted = reader.readNumber<bool>();
    if (accepted)
    {
        receiver.handleAttachAccept();
    }
    else
    {
        receiver.handleAttachReject();
    }
}

void BtsPort::MessageDispatcher::readSms(IBtsReceiver &receiver)
{
    IDecoderPtr smsDecoder = readDecoder();
    if (smsDecoder)
    {
        receiver.handleSmsReceived(header.from, smsDecoder, reader.readRemainingText());
    }
}

void BtsPort::MessageDispatcher::readCallRequest(IBtsReceiver &receiver)
{
    IEncoderPtr talkEncoder = readEncoder();
    if (talkEncoder)
    {
        receiver.handleCallRequestFrom(header.from, talkEncoder);
    }
}

void BtsPort::MessageDispatcher::readCallAccept(IBtsReceiver &receiver)
{
    IEncoderPtr talkEncoder = readEncoder();
    if (talkEncoder)
    {
        receiver.handleCallAcceptFrom(header.from, talkEncoder);
    }
}

void BtsPort::MessageDispatcher::readCallTalk(IBtsReceiver &receiver)
{
    receiver.handleCallTalkFrom(header.from, reader.readRemainingText());
}

void BtsPort::MessageDispatcher::readCallDrop(IBtsReceiver &receiver)
{
    receiver.handleCallDropFrom(header.from);
}

bool BtsPort::MessageDispatcher::readEncryptionDeprecated()
{
    auto encryption = reader.readNumber<std::uint8_t>();
    if (encryption)
    {
        logger.logError("Encrypted ", header.messageId, " with code: ", int(encryption), ", from ", header.from, ", encryption not implemented!");
        return false;
    }
    return true;
}

IDecoderPtr BtsPort::MessageDispatcher::readDecoder()
{
    IDecoderPtr decoder = encryptionFactory.readDecoder(reader);
    logger.logDebug("Read decoder: ", *decoder);
    return decoder;
}

IEncoderPtr BtsPort::MessageDispatcher::readEncoder()
{
    IEncoderPtr encoder = encryptionFactory.readEncoder(reader);
    logger.logDebug("Read encoder: ", *encoder);
    return encoder;
}

}
