#pragma once

#include "IBtsSender.hpp"
#include "Logger/PrefixedLogger.hpp"
#include "ITransport.hpp"
#include "Messages.hpp"

namespace ue
{

using common::PhoneNumber;
using common::ILogger;
using common::ITransport;
using common::BinaryMessage;

class BtsSender : public IBtsSender
{
public:
    BtsSender(PhoneNumber phoneNumber, ITransport& iTransport, ILogger& iLogger);
    ~BtsSender() override;

    void sendAttachRequest() override;
    void sendSms(common::PhoneNumber to, IEncoderPtr smsEncoder, const std::string &smsText) override;
    void sendCallRequest(common::PhoneNumber to, IDecoderPtr talkDecoder) override;
    void sendCallDrop(common::PhoneNumber to) override;
    void sendCallAccept(common::PhoneNumber to, IDecoderPtr talkDecoder) override;
    void sendCallTalk(common::PhoneNumber to, const std::string& callTalk) override;

private:
    void printPrefix(std::ostream& os) const;
    const PhoneNumber phoneNumber;
    ITransport& transport;
    common::PrefixedLogger logger;
};

}
