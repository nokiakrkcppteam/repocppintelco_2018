#pragma once

#include <gmock/gmock.h>
#include "Timer/ITimeoutsReceiver.hpp"

namespace ue
{

struct ITimeoutsReceiverMock : public ITimeoutsReceiver
{
    ITimeoutsReceiverMock();
    ~ITimeoutsReceiverMock() override;

    MOCK_METHOD0(handleAttachTimeout, void());
    MOCK_METHOD0(handlePeerCallingTimeout, void());
    MOCK_METHOD0(handleUserCallingTimeout, void());
    MOCK_METHOD0(handleTalkingTimeout, void());
};

}
