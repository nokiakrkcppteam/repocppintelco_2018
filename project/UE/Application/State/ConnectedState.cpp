#include "ConnectedState.hpp"

namespace ue
{

ConnectedState::ConnectedState(StateContext& stateContext)
    : BaseState(stateContext, "[Connected] ")
{
    user.showConnected();
}

void ConnectedState::handleViewSmses()
{
    logger.logInfo("View smses");
    setViewSmsesState();
}

void ConnectedState::handleEditSms(IEncoderPtr smsEncoder)
{
    logger.logInfo("Edit Sms, with: ", *smsEncoder);
    setEditSmsState(smsEncoder);
}

void ConnectedState::handleCallRequestFrom(common::PhoneNumber from, IEncoderPtr callEncoder)
{
    logger.logInfo("Peer call request from: ", from, ", enc: ", *callEncoder);
    setPeerCallingState(from, callEncoder);
}

void ConnectedState::handleDialing(IDecoderPtr talkDecoder)
{
    logger.logInfo("User wants to dial, dec: ", *talkDecoder);
    setDialingState(talkDecoder);
}


}
