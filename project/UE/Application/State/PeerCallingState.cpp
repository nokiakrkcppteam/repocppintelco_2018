#include "PeerCallingState.hpp"

namespace ue
{

PeerCallingState::PeerCallingState(StateContext &stateContext,
                                   common::PhoneNumber from,
                                   IEncoderPtr talkEncoder)
    : BaseState(stateContext, "[CallRequest from: " + to_string(from) + "] "),
      from(from),
      talkEncoder(talkEncoder)
{
    logger.logDebug("CallRequest from: ", from);
    timers.startPeerCallingTimer();
    user.showPeerCalling(from);
}

void PeerCallingState::handleUserAccept()
{
    logger.logInfo("Go to talking view");
    timers.stopPeerCallingTimer();

    // although not a must - we try to have the same encryption kind as requested by peer (it could be different kind)
    EncryptionKind encryptionSelectedByPeer = talkEncoder->getKind();
    IDecoderPtr talkDecoder = encryptionFactory.getDecoderFor(encryptionSelectedByPeer);

    btsSender.sendCallAccept(from, talkDecoder);
    setTalkingState(from, talkEncoder, talkDecoder);
}

void PeerCallingState::handleExitView()
{
    logger.logInfo("Back to connected state");
    btsSender.sendCallDrop(from);
    timers.stopPeerCallingTimer();
    setConnectedState();
}

void PeerCallingState::handleCallDropFrom(common::PhoneNumber from)
{
    if (from == this->from)
    {
        logger.logInfo("Back to connected state, peer user dropped call");
        timers.stopPeerCallingTimer();
        setConnectedState();
    }
    else
    {
        logger.logError("Unexpected call drop from: ", from);
    }
}

void PeerCallingState::handlePeerCallingTimeout()
{
    logger.logInfo("Back to connected state, timeout");
    btsSender.sendCallDrop(from);
    setConnectedState();
}

}
