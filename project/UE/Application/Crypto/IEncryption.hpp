#pragma once

#include <string>
#include <memory>
#include <iostream>
#include "EncryptionKind.hpp"

namespace common
{
class IncomingMessage;
class OutgoingMessage;
}

namespace ue
{

class IEncoder
{
public:
    virtual ~IEncoder() = default;

    virtual std::string encodeSms(const std::string& text) const = 0;
    virtual std::string encodeTalk(const std::string& text) const = 0;
    virtual EncryptionKind getKind() const = 0;
    virtual void writeTo(common::OutgoingMessage&) const = 0;
    virtual void printData(std::ostream& os) const = 0;
};
using IEncoderPtr = std::shared_ptr<IEncoder>;

inline std::ostream& operator << (std::ostream& os, const IEncoder& encoder)
{
    encoder.printData(os);
    return os;
}


class IDecoder
{
public:
    virtual ~IDecoder() = default;

    virtual std::string decodeSms(const std::string& text) const = 0;
    virtual std::string decodeTalk(const std::string& text) const = 0;
    virtual void writeTo(common::OutgoingMessage&) const = 0;
    virtual void printData(std::ostream& os) const = 0;
};
using IDecoderPtr = std::shared_ptr<IDecoder>;

inline std::ostream& operator << (std::ostream& os, const IDecoder& decoder)
{
    decoder.printData(os);
    return os;
}

}
