#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "Timer/Timers.hpp"
#include "Mocks/ILoggerMock.hpp"
#include "Mocks/ITimeoutsReceiverMock.hpp"
#include "Mocks/ITickerMock.hpp"

namespace ue
{
using namespace ::testing;
using namespace std::chrono_literals;

struct TimersTestSuite : public Test
{
    struct SingleTimerTestData
    {
        using StartStopFunction = std::function<void(ITimers&)>;
        using TimeoutExpectation = std::function<Expectation(ITimeoutsReceiverMock&)>;
        StartStopFunction startTimer;
        StartStopFunction stopTimer;
        TimeoutExpectation expectTimeout;
        ITicker::Duration expectedDuration;

        Expectation expectTimerStarted(ITickerMock& tickerMock, const ITicker::TimePoint& when) const
        {
            return EXPECT_CALL(tickerMock, getTimeAfter(expectedDuration)).WillOnce(Return(when));
        }

    };

    static const SingleTimerTestData attachTimerData;
    static const SingleTimerTestData peerCallingTimerData;
    static const SingleTimerTestData userCallingTimerData;
    static const SingleTimerTestData talkingTimerData;

    const ITicker::TimePoint ATTACH_POINT = 123ms;
    const ITicker::TimePoint PEER_CALLING_POINT = 234ms;
    const ITicker::TimePoint USER_CALLING_POINT = 348ms;
    const ITicker::TimePoint TALKING_POINT = 412ms;

    NiceMock<common::ILoggerMock> loggerMock;
    StrictMock<ITimeoutsReceiverMock> receiverMock;
    StrictMock<ITickerMock> tickerMock;

    Expectation tickerNotExpiringByDefault = EXPECT_CALL(tickerMock, isExpired(_)).WillRepeatedly(Return(false));


    Timers objectUnderTest{tickerMock, loggerMock};
    Timers::ReceiverConnector connectTimersReceiver{objectUnderTest, receiverMock};
};

const TimersTestSuite::SingleTimerTestData TimersTestSuite::attachTimerData{
    [](ITimers& timers) { timers.startAttachTimer(); },
    [](ITimers& timers) { timers.stopAttachTimer(); },
    [](ITimeoutsReceiverMock& receiverMock) -> Expectation { return EXPECT_CALL(receiverMock, handleAttachTimeout()); },
    Timers::ATTACH_TIMER_DURATION};

const TimersTestSuite::SingleTimerTestData TimersTestSuite::peerCallingTimerData{
    [](ITimers& timers) { timers.startPeerCallingTimer(); },
    [](ITimers& timers) { timers.stopPeerCallingTimer(); },
    [](ITimeoutsReceiverMock& receiverMock) -> Expectation { return EXPECT_CALL(receiverMock, handlePeerCallingTimeout()); },
    Timers::PEER_CALLING_TIMER_DURATION};

const TimersTestSuite::SingleTimerTestData TimersTestSuite::userCallingTimerData{
    [](ITimers& timers) { timers.startUserCallingTimer(); },
    [](ITimers& timers) { timers.stopUserCallingTimer(); },
    [](ITimeoutsReceiverMock& receiverMock) -> Expectation { return EXPECT_CALL(receiverMock, handleUserCallingTimeout()); },
    Timers::USER_CALLING_TIMER_DURATION};

const TimersTestSuite::SingleTimerTestData TimersTestSuite::talkingTimerData{
    [](ITimers& timers) { timers.startTalkingTimer(); },
    [](ITimers& timers) { timers.stopTalkingTimer(); },
    [](ITimeoutsReceiverMock& receiverMock) -> Expectation { return EXPECT_CALL(receiverMock, handleTalkingTimeout()); },
    Timers::TALKING_TIMER_DURATION};


TEST_F(TimersTestSuite, shallStartStopAllTimeouts)
{
    // give it few tries
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();

    attachTimerData.expectTimerStarted(tickerMock, ATTACH_POINT);
    attachTimerData.startTimer(objectUnderTest);

    // give it few tries
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();

    attachTimerData.stopTimer(objectUnderTest);
    peerCallingTimerData.expectTimerStarted(tickerMock, PEER_CALLING_POINT);
    peerCallingTimerData.startTimer(objectUnderTest);


    // give it few tries
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();


    peerCallingTimerData.stopTimer(objectUnderTest);
    userCallingTimerData.expectTimerStarted(tickerMock, USER_CALLING_POINT);
    userCallingTimerData.startTimer(objectUnderTest);

    // give it few tries
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();

    userCallingTimerData.stopTimer(objectUnderTest);
    talkingTimerData.expectTimerStarted(tickerMock, TALKING_POINT);
    talkingTimerData.startTimer(objectUnderTest);

    // give it few tries
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();

    talkingTimerData.stopTimer(objectUnderTest);

    // give it few tries
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();
}

TEST_F(TimersTestSuite, shallHandleAllTimeouts)
{
    // give it few tries
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();

    attachTimerData.expectTimerStarted(tickerMock, ATTACH_POINT);
    attachTimerData.startTimer(objectUnderTest);

    // give it few tries
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();

    attachTimerData.expectTimeout(receiverMock);
    EXPECT_CALL(tickerMock, isExpired(ATTACH_POINT)).WillOnce(Return(true)).RetiresOnSaturation();
    objectUnderTest.timeoutExpiredTimers();
    peerCallingTimerData.expectTimerStarted(tickerMock, PEER_CALLING_POINT);
    peerCallingTimerData.startTimer(objectUnderTest);


    // give it few tries
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();


    peerCallingTimerData.expectTimeout(receiverMock);
    EXPECT_CALL(tickerMock, isExpired(PEER_CALLING_POINT)).WillOnce(Return(true)).RetiresOnSaturation();
    objectUnderTest.timeoutExpiredTimers();
    userCallingTimerData.expectTimerStarted(tickerMock, USER_CALLING_POINT);
    userCallingTimerData.startTimer(objectUnderTest);

    // give it few tries
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();

    userCallingTimerData.expectTimeout(receiverMock);
    EXPECT_CALL(tickerMock, isExpired(USER_CALLING_POINT)).WillOnce(Return(true)).RetiresOnSaturation();

    objectUnderTest.timeoutExpiredTimers();
    talkingTimerData.expectTimerStarted(tickerMock, TALKING_POINT);
    talkingTimerData.startTimer(objectUnderTest);

    // give it few tries
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();

    talkingTimerData.expectTimeout(receiverMock);
    EXPECT_CALL(tickerMock, isExpired(TALKING_POINT)).WillOnce(Return(true)).RetiresOnSaturation();

    objectUnderTest.timeoutExpiredTimers();

    // give it few tries
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();
}

TEST_F(TimersTestSuite, shallQuickHandleAllTimeouts)
{
    attachTimerData.expectTimerStarted(tickerMock, ATTACH_POINT);
    attachTimerData.startTimer(objectUnderTest);
    attachTimerData.expectTimeout(receiverMock);
    EXPECT_CALL(tickerMock, isExpired(ATTACH_POINT)).WillOnce(Return(true)).RetiresOnSaturation();
    objectUnderTest.timeoutExpiredTimers();
    peerCallingTimerData.expectTimerStarted(tickerMock, PEER_CALLING_POINT);
    peerCallingTimerData.startTimer(objectUnderTest);

    peerCallingTimerData.expectTimeout(receiverMock);
    EXPECT_CALL(tickerMock, isExpired(PEER_CALLING_POINT)).WillOnce(Return(true)).RetiresOnSaturation();
    objectUnderTest.timeoutExpiredTimers();
    userCallingTimerData.expectTimerStarted(tickerMock, USER_CALLING_POINT);
    userCallingTimerData.startTimer(objectUnderTest);

    userCallingTimerData.expectTimeout(receiverMock);
    EXPECT_CALL(tickerMock, isExpired(USER_CALLING_POINT)).WillOnce(Return(true)).RetiresOnSaturation();
    objectUnderTest.timeoutExpiredTimers();
    talkingTimerData.expectTimerStarted(tickerMock, TALKING_POINT);
    talkingTimerData.startTimer(objectUnderTest);

    talkingTimerData.expectTimeout(receiverMock);
    EXPECT_CALL(tickerMock, isExpired(TALKING_POINT)).WillOnce(Return(true)).RetiresOnSaturation();
    objectUnderTest.timeoutExpiredTimers();
}

TEST_F(TimersTestSuite, shallHandleAllTimeoutsAtOnce)
{
    // give it few tries
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();


    attachTimerData.expectTimerStarted(tickerMock, ATTACH_POINT);
    attachTimerData.startTimer(objectUnderTest);

    peerCallingTimerData.expectTimerStarted(tickerMock, PEER_CALLING_POINT);
    peerCallingTimerData.startTimer(objectUnderTest);

    userCallingTimerData.expectTimerStarted(tickerMock, USER_CALLING_POINT);
    userCallingTimerData.startTimer(objectUnderTest);

    talkingTimerData.expectTimerStarted(tickerMock, TALKING_POINT);
    talkingTimerData.startTimer(objectUnderTest);

    // give it few tries
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();


    attachTimerData.expectTimeout(receiverMock);
    peerCallingTimerData.expectTimeout(receiverMock);
    userCallingTimerData.expectTimeout(receiverMock);
    talkingTimerData.expectTimeout(receiverMock);

    EXPECT_CALL(tickerMock, isExpired(ATTACH_POINT)).WillOnce(Return(true)).RetiresOnSaturation();
    EXPECT_CALL(tickerMock, isExpired(PEER_CALLING_POINT)).WillOnce(Return(true)).RetiresOnSaturation();
    EXPECT_CALL(tickerMock, isExpired(USER_CALLING_POINT)).WillOnce(Return(true)).RetiresOnSaturation();
    EXPECT_CALL(tickerMock, isExpired(TALKING_POINT)).WillOnce(Return(true)).RetiresOnSaturation();

    objectUnderTest.timeoutExpiredTimers();

    // give it few tries
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();
}

TEST_F(TimersTestSuite, shallQuickHandleAllTimeoutsAtOnce)
{
    attachTimerData.expectTimerStarted(tickerMock, ATTACH_POINT);
    attachTimerData.startTimer(objectUnderTest);

    peerCallingTimerData.expectTimerStarted(tickerMock, PEER_CALLING_POINT);
    peerCallingTimerData.startTimer(objectUnderTest);

    userCallingTimerData.expectTimerStarted(tickerMock, USER_CALLING_POINT);
    userCallingTimerData.startTimer(objectUnderTest);

    talkingTimerData.expectTimerStarted(tickerMock, TALKING_POINT);
    talkingTimerData.startTimer(objectUnderTest);

    attachTimerData.expectTimeout(receiverMock);
    peerCallingTimerData.expectTimeout(receiverMock);
    userCallingTimerData.expectTimeout(receiverMock);
    talkingTimerData.expectTimeout(receiverMock);

    EXPECT_CALL(tickerMock, isExpired(ATTACH_POINT)).WillOnce(Return(true)).RetiresOnSaturation();
    EXPECT_CALL(tickerMock, isExpired(PEER_CALLING_POINT)).WillOnce(Return(true)).RetiresOnSaturation();
    EXPECT_CALL(tickerMock, isExpired(USER_CALLING_POINT)).WillOnce(Return(true)).RetiresOnSaturation();
    EXPECT_CALL(tickerMock, isExpired(TALKING_POINT)).WillOnce(Return(true)).RetiresOnSaturation();

    objectUnderTest.timeoutExpiredTimers();
}

class SingleTimerTestSuite : public TimersTestSuite, public WithParamInterface<TimersTestSuite::SingleTimerTestData>
{
protected:
    const ITicker::TimePoint START_POINT = 123ms;
    const ITicker::TimePoint RESTART_POINT = 234ms;

    void startTimer()
    {
        GetParam().startTimer(objectUnderTest);
    }
    void stopTimer()
    {
        GetParam().stopTimer(objectUnderTest);
    }
    Expectation expectTimeout()
    {
        return GetParam().expectTimeout(receiverMock);
    }
    Expectation expectTimerStarted(const ITicker::TimePoint& when)
    {
        return GetParam().expectTimerStarted(tickerMock, when);
    }
};

TEST_P(SingleTimerTestSuite, shallStartTimer)
{
    expectTimerStarted(START_POINT);
    startTimer();
}

TEST_P(SingleTimerTestSuite, shallStartStopTimerWithinItsDuration)
{
    expectTimerStarted(START_POINT);
    startTimer();

    // give it few tries
    objectUnderTest.timeoutExpiredTimers();

    stopTimer();

    // give it 3 tries
    EXPECT_CALL(tickerMock, isExpired(START_POINT)).Times(0);
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();
}

TEST_P(SingleTimerTestSuite, shallQuickStartStopTimer)
{
    expectTimerStarted(START_POINT);
    startTimer();
    stopTimer();
    EXPECT_CALL(tickerMock, isExpired(START_POINT)).Times(0);
    objectUnderTest.timeoutExpiredTimers();
}

TEST_P(SingleTimerTestSuite, shallTimerExpire)
{
    expectTimerStarted(START_POINT);
    startTimer();

    // give it few tries
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();

    // timeout
    expectTimeout();
    EXPECT_CALL(tickerMock, isExpired(START_POINT)).WillOnce(Return(true)).RetiresOnSaturation();
    objectUnderTest.timeoutExpiredTimers();

    // give it next tries
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();
}

TEST_P(SingleTimerTestSuite, shallQuickTimerExpire)
{
    expectTimerStarted(START_POINT);
    startTimer();
    expectTimeout();
    EXPECT_CALL(tickerMock, isExpired(START_POINT)).WillOnce(Return(true)).RetiresOnSaturation();
    objectUnderTest.timeoutExpiredTimers();
}

TEST_P(SingleTimerTestSuite, shallTimerExpireOnlyOnce)
{
    expectTimerStarted(START_POINT);
    startTimer();

    // give it few tries
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();

    // timeout
    expectTimeout();
    EXPECT_CALL(tickerMock, isExpired(START_POINT)).WillOnce(Return(true)).RetiresOnSaturation();
    objectUnderTest.timeoutExpiredTimers();

    // no timeout for subsequent tries
    EXPECT_CALL(tickerMock, isExpired(START_POINT)).Times(0);
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();
}

TEST_P(SingleTimerTestSuite, shallTimerRestart)
{
    expectTimerStarted(START_POINT);

    startTimer();

    // give it few tries
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();

    // Restart
    expectTimerStarted(RESTART_POINT);
    startTimer();

    // give it few tries
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();

    // Expire on new time-point
    expectTimeout();
    EXPECT_CALL(tickerMock, isExpired(RESTART_POINT)).WillOnce(Return(true)).RetiresOnSaturation();
    objectUnderTest.timeoutExpiredTimers();

    // no timeout for subsequent tries
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();
    objectUnderTest.timeoutExpiredTimers();
}

INSTANTIATE_TEST_CASE_P(AttachTimer,
                        SingleTimerTestSuite,
                        Values(TimersTestSuite::attachTimerData));

INSTANTIATE_TEST_CASE_P(PeerCallingTimer,
                        SingleTimerTestSuite,
                        Values(TimersTestSuite::peerCallingTimerData));

INSTANTIATE_TEST_CASE_P(UserCallingTimer,
                        SingleTimerTestSuite,
                        Values(TimersTestSuite::userCallingTimerData));

INSTANTIATE_TEST_CASE_P(TalkingTimer,
                        SingleTimerTestSuite,
                        Values(TimersTestSuite::talkingTimerData));

}

