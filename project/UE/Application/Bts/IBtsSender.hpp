#pragma once

#include "Messages.hpp"
#include "Crypto/IEncryption.hpp"
#include <string>

namespace ue
{

class  IBtsSender
{
public:
    virtual ~IBtsSender() = default;

    virtual void sendAttachRequest() = 0;
    virtual void sendSms(common::PhoneNumber to, IEncoderPtr smsEncoder, const std::string& smsText) = 0;
    virtual void sendCallRequest(common::PhoneNumber to, IDecoderPtr talkDecoder) = 0;
    virtual void sendCallDrop(common::PhoneNumber to) = 0;
    virtual void sendCallAccept(common::PhoneNumber to, IDecoderPtr talkDecoder) = 0;
    virtual void sendCallTalk(common::PhoneNumber to, const std::string& talkText) = 0;
};

}
