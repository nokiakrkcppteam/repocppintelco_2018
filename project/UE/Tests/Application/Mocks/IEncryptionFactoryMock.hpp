#pragma once

#include <gmock/gmock.h>
#include "Crypto/IEncryptionFactory.hpp"
#include "Messages/IncomingMessage.hpp"

namespace ue
{

struct IEncryptionFactoryMock : IEncryptionFactory
{
    IEncryptionFactoryMock();
    ~IEncryptionFactoryMock() override;

    MOCK_METHOD1(getEncoderFor, IEncoderPtr(EncryptionKind kind));
    MOCK_METHOD1(readDecoder, IDecoderPtr(common::IncomingMessage&));
    MOCK_METHOD1(readEncoder, IEncoderPtr(common::IncomingMessage&));
    MOCK_METHOD1(getDecoderFor, IDecoderPtr(EncryptionKind kind));
};

}
