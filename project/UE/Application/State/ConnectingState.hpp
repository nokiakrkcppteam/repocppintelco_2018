#pragma once

#include "State/BaseState.hpp"

namespace ue
{

class ConnectingState : public BaseState
{
public:
    ConnectingState(StateContext& stateContext);

    void handleSib() override;
    void handleAttachAccept() override;
    void handleAttachReject() override;
    void handleAttachTimeout() override;
};

}
