#include "UserCallingState.hpp"

namespace ue
{

UserCallingState::UserCallingState(StateContext &stateContext,
                                   common::PhoneNumber to,
                                   IDecoderPtr talkDecoder)
    : BaseState(stateContext, "[CALLING " + to_string(to) + "] "),
      to(to),
      talkDecoder(talkDecoder)
{
    user.showUserCalling(to);
    timers.startUserCallingTimer();
    btsSender.sendCallRequest(to, talkDecoder);
}

void UserCallingState::handleCallAcceptFrom(common::PhoneNumber from, IEncoderPtr talkEncoder)
{
    if (to == from)
    {
        logger.logInfo("Call accepted");
        timers.stopUserCallingTimer();
        setTalkingState(to, talkEncoder, talkDecoder);
    }
    else
    {
        logger.logError("Unexpected accept from: ", from);
    }
}

void UserCallingState::handleCallDropFrom(common::PhoneNumber from)
{
    if (to == from)
    {
        logger.logInfo("Peer user dropped call");
        timers.stopUserCallingTimer();
        setConnectedState();
    }
    else
    {
        logger.logError("Unexpected drop from: ", from);
    }

}

void UserCallingState::handleExitView()
{
    logger.logInfo("User dropped call");
    timers.stopUserCallingTimer();
    btsSender.sendCallDrop(to);
    setConnectedState();
}

void UserCallingState::handleUserAccept()
{
    logger.logDebug("Ignored accept in this state");
}

void UserCallingState::handleUserCallingTimeout()
{
    logger.logInfo("Timeout dropped call");
    btsSender.sendCallDrop(to);
    setConnectedState();
}

}
