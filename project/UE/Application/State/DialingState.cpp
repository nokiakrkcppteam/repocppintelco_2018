#include "DialingState.hpp"

namespace ue
{

DialingState::DialingState(StateContext &stateContext, IDecoderPtr talkDecoder)
    : BaseState(stateContext, "[DIALING] "),
      talkDecoder(talkDecoder)
{
    user.showDialing();
}

void DialingState::handleUserCalling(common::PhoneNumber to)
{
    logger.logInfo("Calling ", to);
    setUserCallingState(to, talkDecoder);
}

void DialingState::handleExitView()
{
    logger.logInfo("Cancelled");
    setConnectedState();
}

}
