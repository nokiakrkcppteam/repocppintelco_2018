#pragma once

#include "Logger/PrefixedLogger.hpp"
#include "Crypto/IEncryptionFactory.hpp"
#include "ITransport.hpp"
#include "IBtsReceiver.hpp"
#include "Messages.hpp"

namespace ue
{

using common::PhoneNumber;
using common::ILogger;
using common::ITransport;
using common::BinaryMessage;

class BtsPort
{
public:
    BtsPort(IBtsReceiver& receiver, PhoneNumber phoneNumber, IEncryptionFactory& encryptionFactory, ITransport& iTransport, ILogger& iLogger);
    ~BtsPort();

private:
    class MessageDispatcher;
    void receiveMessage(BinaryMessage message);
    void printPrefix(std::ostream& os) const;

    IBtsReceiver& receiver;
    const PhoneNumber phoneNumber;
    IEncryptionFactory& encryptionFactory;
    ITransport& transport;
    common::PrefixedLogger logger;
};

}
