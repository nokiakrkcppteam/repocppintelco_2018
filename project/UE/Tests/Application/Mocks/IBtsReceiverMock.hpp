#pragma once

#include <gmock/gmock.h>
#include "Bts/IBtsReceiver.hpp"

namespace ue
{

struct IBtsReceiverMock : public IBtsReceiver
{
    IBtsReceiverMock();
    ~IBtsReceiverMock() override;

    MOCK_METHOD0(handleSib, void());
    MOCK_METHOD0(handleAttachAccept, void());
    MOCK_METHOD0(handleAttachReject, void());
    MOCK_METHOD3(handleSmsReceived, void(common::PhoneNumber from, IDecoderPtr smsDecoder, const std::string& text));
    MOCK_METHOD2(handleCallRequestFrom, void(common::PhoneNumber from, IEncoderPtr callEncoder));
    MOCK_METHOD2(handleCallAcceptFrom, void(common::PhoneNumber from, IEncoderPtr callEncoder));
    MOCK_METHOD2(handleCallTalkFrom, void(common::PhoneNumber from, const std::string& text));
    MOCK_METHOD1(handleCallDropFrom, void(common::PhoneNumber from));
};

}
