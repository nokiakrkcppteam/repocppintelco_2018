#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <vector>

#include "Bts/BtsPort.hpp"
#include "Mocks/ILoggerMock.hpp"
#include "Mocks/ITransportMock.hpp"
#include "Mocks/IBtsReceiverMock.hpp"
#include "Mocks/IEncryptionMock.hpp"
#include "Mocks/IEncryptionFactoryMock.hpp"
#include "Messages.hpp"
#include "Messages/OutgoingMessage.hpp"

namespace ue
{
using namespace ::testing;

class BtsPortTestSuite : public Test
{
protected:
    const common::PhoneNumber PHONE_NUMBER{112};
    const common::PhoneNumber PEER_PHONE_NUMBER{18};
    const common::BtsId BTS_ID{997};

    using EncryptionData = std::vector<std::uint8_t>;
    const EncryptionData SMS_ENCRYPTION_DATA{0x23, 0x45, 0x66};
    const EncryptionData CALL_ENCRYPTION_DATA{0x11, 0x22};
    const std::string ENCRYPTED_SMS_TEXT = "0983029  984jlkj@$#%$@";
    const std::string CALL_TEXT = "call text";

    NiceMock<common::ILoggerMock> loggerMock;
    StrictMock<common::ITransportMock> transportMock;
    StrictMock<IBtsReceiverMock> receiverMock;
    StrictMock<IEncryptionFactoryMock> encryptionFactoryMock;
    std::shared_ptr<IDecoderMock> smsDecoderMock = std::make_shared<StrictMock<IDecoderMock>>();
    Expectation expectSmsDecoderMockPrints = EXPECT_CALL(*smsDecoderMock, printData(_)).Times(AnyNumber());
    std::shared_ptr<IEncoderMock> talkEncoderMock = std::make_shared<StrictMock<IEncoderMock>>();
    Expectation expectTalkEncoderMockPrints = EXPECT_CALL(*talkEncoderMock, printData(_)).Times(AnyNumber());

    common::ITransport::MessageCallback receiveCallback;
    Expectation expectRegisterOnReceive = EXPECT_CALL(transportMock, registerMessageCallback(_)).WillOnce(SaveArg<0>(&receiveCallback))
                                                                                                .WillOnce(Return()); // in destructor
    BtsPort objectUnderTest{receiverMock,
                            PHONE_NUMBER,
                            encryptionFactoryMock,
                            transportMock,
                            loggerMock};


    common::BinaryMessage prepareSib();
    common::BinaryMessage prepareAttachResponse(bool accept);
    common::BinaryMessage prepareSms();
    common::BinaryMessage prepareCallRequest();
    common::BinaryMessage prepareCallAccept();
    common::BinaryMessage prepareCallTalk();
    common::BinaryMessage prepareCallDrop();
    void writeEncryption(common::OutgoingMessage &builder, const EncryptionData &encryptionData);
    void expectEncryption(common::IncomingMessage &reader, const EncryptionData &encryptionData);
};

common::BinaryMessage BtsPortTestSuite::prepareSib()
{
    common::OutgoingMessage builder(common::MessageId::Sib, {}, PHONE_NUMBER);
    builder.writeBtsId(BTS_ID);
    return builder.getMessage();
}

common::BinaryMessage BtsPortTestSuite::prepareAttachResponse(bool accept)
{
    common::OutgoingMessage builder(common::MessageId::AttachResponse, {}, PHONE_NUMBER);
    builder.writeNumber(accept);
    return builder.getMessage();
}

common::BinaryMessage BtsPortTestSuite::prepareSms()
{
    common::OutgoingMessage builder(common::MessageId::Sms, PEER_PHONE_NUMBER, PHONE_NUMBER);
    writeEncryption(builder, SMS_ENCRYPTION_DATA);
    builder.writeText(ENCRYPTED_SMS_TEXT);
    return builder.getMessage();
}

void BtsPortTestSuite::writeEncryption(common::OutgoingMessage& builder, const EncryptionData& encryptionData)
{
    for (auto eb : encryptionData)
    {
        builder.writeNumber(eb);
    }
}

void BtsPortTestSuite::expectEncryption(common::IncomingMessage &reader, const EncryptionData &encryptionData)
{
    for (auto eb : encryptionData)
    {
        EXPECT_EQ(eb, reader.readNumber<EncryptionData::value_type>());
    }
}

common::BinaryMessage BtsPortTestSuite::prepareCallRequest()
{
    common::OutgoingMessage builder(common::MessageId::CallRequest, PEER_PHONE_NUMBER, PHONE_NUMBER);
    writeEncryption(builder, CALL_ENCRYPTION_DATA);
    return builder.getMessage();
}

common::BinaryMessage BtsPortTestSuite::prepareCallAccept()
{
    common::OutgoingMessage builder(common::MessageId::CallAccepted, PEER_PHONE_NUMBER, PHONE_NUMBER);
    writeEncryption(builder, CALL_ENCRYPTION_DATA);
    return builder.getMessage();
}

common::BinaryMessage BtsPortTestSuite::prepareCallTalk()
{
    common::OutgoingMessage builder(common::MessageId::CallTalk, PEER_PHONE_NUMBER, PHONE_NUMBER);
    builder.writeText(CALL_TEXT);
    return builder.getMessage();
}

common::BinaryMessage BtsPortTestSuite::prepareCallDrop()
{
    common::OutgoingMessage builder(common::MessageId::CallDropped, PEER_PHONE_NUMBER, PHONE_NUMBER);
    return builder.getMessage();
}

TEST_F(BtsPortTestSuite, shallReceiveSib)
{
    EXPECT_CALL(receiverMock, handleSib());
    receiveCallback(prepareSib());
}

TEST_F(BtsPortTestSuite, shallReceiveAttachAccept)
{
    EXPECT_CALL(receiverMock, handleAttachAccept());
    receiveCallback(prepareAttachResponse(true));
}

TEST_F(BtsPortTestSuite, shallReceiveAttachReject)
{
    EXPECT_CALL(receiverMock, handleAttachReject());
    receiveCallback(prepareAttachResponse(false));
}

TEST_F(BtsPortTestSuite, shallReceiveSms)
{
    EXPECT_CALL(encryptionFactoryMock, readDecoder(_)).WillOnce(
                DoAll(Invoke([this] (common::IncomingMessage& reader) { expectEncryption(reader, SMS_ENCRYPTION_DATA); }),
                      Return(smsDecoderMock)));
    EXPECT_CALL(receiverMock, handleSmsReceived(PEER_PHONE_NUMBER, Eq<IDecoderPtr>(smsDecoderMock), ENCRYPTED_SMS_TEXT));
    receiveCallback(prepareSms());
}

TEST_F(BtsPortTestSuite, shallReceiveCallRequest)
{
    EXPECT_CALL(encryptionFactoryMock, readEncoder(_)).WillOnce(
                DoAll(Invoke([this] (common::IncomingMessage& reader) { expectEncryption(reader, CALL_ENCRYPTION_DATA); }),
                      Return(talkEncoderMock)));

    EXPECT_CALL(receiverMock, handleCallRequestFrom(PEER_PHONE_NUMBER, Eq<IEncoderPtr>(talkEncoderMock)));
    receiveCallback(prepareCallRequest());
}

TEST_F(BtsPortTestSuite, shallReceiveCallAccept)
{
    EXPECT_CALL(encryptionFactoryMock, readEncoder(_)).WillOnce(
                DoAll(Invoke([this] (common::IncomingMessage& reader) { expectEncryption(reader, CALL_ENCRYPTION_DATA); }),
                      Return(talkEncoderMock)));

    EXPECT_CALL(receiverMock, handleCallAcceptFrom(PEER_PHONE_NUMBER, Eq<IEncoderPtr>(talkEncoderMock)));
    receiveCallback(prepareCallAccept());
}

TEST_F(BtsPortTestSuite, shallReceiveCallTalk)
{
    EXPECT_CALL(receiverMock, handleCallTalkFrom(PEER_PHONE_NUMBER, CALL_TEXT));
    receiveCallback(prepareCallTalk());
}

TEST_F(BtsPortTestSuite, shallReceiveCallDrop)
{
    EXPECT_CALL(receiverMock, handleCallDropFrom(PEER_PHONE_NUMBER));
    receiveCallback(prepareCallDrop());
}

}

