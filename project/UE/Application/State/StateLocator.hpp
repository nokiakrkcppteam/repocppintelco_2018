#pragma once

#include "IEventsReceiver.hpp"
#include <memory>

namespace ue
{

class StateLocator
{
public:
    using State = std::unique_ptr<IEventsReceiver>;
    void setState(State newState);
    IEventsReceiver& getState();

private:
    std::unique_ptr<IEventsReceiver> currentState;
};

}
