#pragma once

#include "BaseState.hpp"

namespace ue
{

class DialingState : public BaseState
{
public:
    DialingState(StateContext& stateContext, IDecoderPtr talkDecoder);

    void handleUserCalling(common::PhoneNumber to) override;
    void handleExitView() override;

private:
    IDecoderPtr talkDecoder;
};

}
