#include <iostream>

struct A
{

};

struct B: A
{

};

struct A_virtual
{
    virtual ~A_virtual(){} // good practice to always define virtual destructor if at least one virtual method is defined
    virtual void f() = 0;
};

struct B_virtual : A_virtual
{
    void f(int){ std::cout << "B_virtual" << std::endl;}
};

struct C_virtual : B_virtual
{
    void f() override {std::cout << "C_virtual" << std::endl;} //good practice to add override
};

int main()
{
    B b;
    A& a = b;
    //B& b_ref = dynamic_cast<B&>(a); error: in order to use dynamic downcast rtti is needed, rtti is stored with virtual functions table

    //A_virtual a_virtual; //error: can't create instance of abstract class
    //B_virtual b_virtual; //error: can't create instance of abstract class
    C_virtual c_virtual;
    c_virtual.f();
    B_virtual& b_virtual = c_virtual;
    //b_virtual.f(); // error: to few arguments
    b_virtual.f(0);
    //b_virtual.A_virtual::f(); // error: linker error undefined reference
    A_virtual& a_virtual = b_virtual;
    a_virtual.f();
    C_virtual& c_ref = dynamic_cast<C_virtual&>(a_virtual);
    c_ref.f();
    c_ref.B_virtual::f(1);

    std::cout << sizeof (A) << std::endl;
    std::cout << sizeof (A_virtual) << std::endl;
    std::cout << sizeof (B_virtual) << std::endl;
    std::cout << sizeof (C_virtual) << std::endl;
    std::cout << sizeof (int) << std::endl;
    std::cout << sizeof (int*) << std::endl;
}
