#include "TimersRunner.hpp"

namespace ue
{

TimersRunner::TimersRunner(ITicker &ticker, ITimers &timers, common::ILogger &logger)
    : ticker(ticker),
      timers(timers),
      logger(logger, "[TimersRunner] ")
{
    this->logger.logDebug("Constructed");
    localThread = std::thread([this] { localThreadBody(); });
}

TimersRunner::~TimersRunner()
{
    running = false;
    localThread.join();
    logger.logDebug("Destructed");
}

void TimersRunner::localThreadBody()
{
    logger.logInfo("Loop started");
    while (running)
    {
        ticker.waitOneTick();
        timers.timeoutExpiredTimers();
    }
    logger.logInfo("Loop stopped");
}

}
