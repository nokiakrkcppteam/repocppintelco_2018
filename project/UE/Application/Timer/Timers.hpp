#pragma once

#include "ITimers.hpp"
#include "ITicker.hpp"
#include "ITimeoutsReceiver.hpp"
#include "Logger/PrefixedLogger.hpp"

#include <mutex>
#include <functional>
#include <vector>

namespace ue
{

class Timers : public ITimers
{
public:
    static constexpr ITicker::Duration ATTACH_TIMER_DURATION = std::chrono::milliseconds{500};
    static constexpr ITicker::Duration PEER_CALLING_TIMER_DURATION = std::chrono::seconds{30};
    static constexpr ITicker::Duration USER_CALLING_TIMER_DURATION  = std::chrono::seconds{60};
    static constexpr ITicker::Duration TALKING_TIMER_DURATION = std::chrono::minutes{2};

    Timers(ITicker& ticker, common::ILogger& logger);
    ~Timers() override;

    class ReceiverConnector
    {
    public:
        ReceiverConnector(Timers& timers, ITimeoutsReceiver& receiver);
        ~ReceiverConnector();
    private:
        Timers& timers;
    };

    void startAttachTimer() override;
    void stopAttachTimer() override;

    void startPeerCallingTimer() override;
    void stopPeerCallingTimer() override;

    void startUserCallingTimer() override;
    void stopUserCallingTimer() override;

    void startTalkingTimer() override;
    void stopTalkingTimer() override;

    void timeoutExpiredTimers() override;

private:
    void setReceiver(ITimeoutsReceiver&);
    void resetReceiver();

    class NullReceiver : public ITimeoutsReceiver
    {
    public:
        NullReceiver(common::ILogger& logger);
        void handleAttachTimeout() override;
        void handlePeerCallingTimeout() override;
        void handleUserCallingTimeout() override;
        void handleTalkingTimeout() override;
    private:
        common::PrefixedLogger logger;
    };
    NullReceiver nullReceiver{logger};
    std::reference_wrapper<ITimeoutsReceiver> receiver{nullReceiver};
    ITicker& ticker;
    common::PrefixedLogger logger;

    class Timer
    {
    public:
        using Handler = void (ITimeoutsReceiver::*)();

        Timer(Handler handler);

        void startTimerFor(ITicker& ticker, ITicker::Duration duration);
        void stopTimer();
        Handler* tryExpire(ITicker& ticker);

    private:
        Handler handler;
        bool isSet = false;
        ITicker::TimePoint pointOfExpire{};
    };

    Timer attachTimer;
    Timer peerCallingTimer;
    Timer userCallingTimer;
    Timer talkingTimer;
    std::vector<std::reference_wrapper<Timer>> timers;

    std::mutex guard;
    template <typename Callable>
    auto guardedOperation(Callable callable)
    {
        std::lock_guard<std::mutex> lock(guard);
        return callable();
    }
};

}
