#pragma once

#include "BaseState.hpp"

namespace ue
{

class ViewSmsesState : public BaseState
{
public:
    ViewSmsesState(StateContext& stateContext);

    void handleViewSms(ISmsDB::Index smsIndex) override;
    void handleExitView() override;
};

}
