#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <sstream>
#include <vector>

#include "Crypto/EncryptionFactory.hpp"
#include "Messages.hpp"
#include "Messages/OutgoingMessage.hpp"
#include "Messages/IncomingMessage.hpp"
#include "Mocks/IApplicationEnvironmentMock.hpp"
#include "Mocks/ILoggerMock.hpp"
#include "Messages/PhoneNumber.hpp"

namespace ue
{
using namespace ::testing;

class EncryptionFactoryTestSuite : public Test
{
protected:
    NiceMock<common::ILoggerMock> loggerMock;
    StrictMock<IApplicationEnvironmentMock> applicationEnvironmentMock;

    EncryptionFactory objectUnderTest{applicationEnvironmentMock, loggerMock};

    template <typename Object>
    void assertPrintout(const Object& object, Matcher<std::string> asExpected)
    {
        std::ostringstream oss;
        oss << object;
        std::string actual = oss.str();
        EXPECT_THAT(actual, asExpected);
    }
};


class NoneEncryptionTestSuite : public EncryptionFactoryTestSuite
{
protected:
    Matcher<std::string> expectedPrintoutWithNameOnly = "NoneEncryption";
    const std::string TEXT = "Hello World!";
    common::BinaryMessage encryption{{ static_cast<std::uint8_t>(EncryptionKind::None) }};
    common::IncomingMessage readerForSms{encryption};
    common::IncomingMessage readerForTalk{encryption};
    common::OutgoingMessage builder{};

    IDecoderPtr smsDecoderUnderTest = objectUnderTest.readDecoder(readerForSms);
    IEncoderPtr smsEncoderUnderTest = objectUnderTest.getEncoderFor(EncryptionKind::None);
    IEncoderPtr talkEncoderUnderTest = objectUnderTest.readEncoder(readerForTalk);
    IDecoderPtr talkDecoderUnderTest = objectUnderTest.getDecoderFor(EncryptionKind::None);
    void validateWrittenEncryption();
};

TEST_F(NoneEncryptionTestSuite, shallCreate)
{
    ASSERT_THAT(smsDecoderUnderTest, NotNull());
    ASSERT_THAT(smsEncoderUnderTest, NotNull());
    ASSERT_THAT(talkEncoderUnderTest, NotNull());
    ASSERT_THAT(talkDecoderUnderTest, NotNull());
}

TEST_F(NoneEncryptionTestSuite, shallHaveProperKind)
{
    EXPECT_EQ(EncryptionKind::None, smsEncoderUnderTest->getKind());
    EXPECT_EQ(EncryptionKind::None, talkEncoderUnderTest->getKind());
}

void NoneEncryptionTestSuite::validateWrittenEncryption()
{
    common::BinaryMessage message = builder.getMessage();
    ASSERT_EQ(1, message.value.size());
    ASSERT_EQ(EncryptionKind::None, static_cast<EncryptionKind>(message.value.front()));
}

TEST_F(NoneEncryptionTestSuite, shallSmsEncoderWriteOnlyKind)
{
    smsEncoderUnderTest->writeTo(builder);
    validateWrittenEncryption();
}

TEST_F(NoneEncryptionTestSuite, shallTalkDecoderWriteOnlyKind)
{
    talkDecoderUnderTest->writeTo(builder);
    validateWrittenEncryption();
}

TEST_F(NoneEncryptionTestSuite, shallNoEncodeSms)
{
    ASSERT_EQ(TEXT, smsEncoderUnderTest->encodeSms(TEXT));
}

TEST_F(NoneEncryptionTestSuite, shallNoDecodeSms)
{
    ASSERT_EQ(TEXT, smsDecoderUnderTest->decodeSms(TEXT));
}

TEST_F(NoneEncryptionTestSuite, shallNoEncodeTalk)
{
    ASSERT_EQ(TEXT, talkEncoderUnderTest->encodeTalk(TEXT));
}

TEST_F(NoneEncryptionTestSuite, shallNoDecodeTalk)
{
    ASSERT_EQ(TEXT, talkDecoderUnderTest->decodeTalk(TEXT));
}

TEST_F(NoneEncryptionTestSuite, shallSmsEncoderPrintOnlyItsName)
{
    assertPrintout(*smsEncoderUnderTest, expectedPrintoutWithNameOnly);
}

TEST_F(NoneEncryptionTestSuite, shallSmsDecoderPrintOnlyItsName)
{
    assertPrintout(*smsDecoderUnderTest, expectedPrintoutWithNameOnly);
}

TEST_F(NoneEncryptionTestSuite, shallTalkEncoderPrintOnlyItsName)
{
    assertPrintout(*talkEncoderUnderTest, expectedPrintoutWithNameOnly);
}

TEST_F(NoneEncryptionTestSuite, shallTalkDecoderPrintOnlyItsName)
{
    assertPrintout(*talkDecoderUnderTest, expectedPrintoutWithNameOnly);
}

class XorEncryptionTestSuite : public EncryptionFactoryTestSuite
{
protected:
    const std::uint8_t XOR_KEY = 0x12;
    const std::string XOR_KEY_IN_HEX = "0x12";
    Matcher<std::string> expectedXorAndKeyInHex = AllOf(
                HasSubstr("Xor"),
                HasSubstr(XOR_KEY_IN_HEX));

    const std::string TEXT = "HA BA";
    const std::vector<std::uint8_t> ENCODED_BYTES {
                static_cast<std::uint8_t>('H' ^ XOR_KEY),
                static_cast<std::uint8_t>('A' ^ XOR_KEY),
                static_cast<std::uint8_t>(' ' ^ XOR_KEY),
                static_cast<std::uint8_t>('B' ^ XOR_KEY),
                static_cast<std::uint8_t>('A' ^ XOR_KEY)
    };
    const std::string ENCODED_TEXT{ENCODED_BYTES.begin(), ENCODED_BYTES.end()};

    common::BinaryMessage encryption{{static_cast<std::uint8_t>(EncryptionKind::Xor),
                                      XOR_KEY}};
    common::IncomingMessage readerForSmsDecoder{encryption};
    common::IncomingMessage readerForTalkEncoder{encryption};
    common::OutgoingMessage builder{};

    IDecoderPtr smsDecoderUnderTest = objectUnderTest.readDecoder(readerForSmsDecoder);
    IEncoderPtr talkEncoderUnderTest = objectUnderTest.readEncoder(readerForTalkEncoder);

    Expectation expectReadXorKeyForSmsEncoder = EXPECT_CALL(applicationEnvironmentMock, getProperty("enc_xor", _)).WillOnce(Return(XOR_KEY));
    IEncoderPtr smsEncoderUnderTest = objectUnderTest.getEncoderFor(EncryptionKind::Xor);

    Expectation expectReadXorKeyForTalkDecoder = EXPECT_CALL(applicationEnvironmentMock, getProperty("dec_xor", _)).WillOnce(Return(XOR_KEY));
    IDecoderPtr talkDecoderUnderTest = objectUnderTest.getDecoderFor(EncryptionKind::Xor);
    void validateWrittenEncryption();
};

TEST_F(XorEncryptionTestSuite, shallCreate)
{
    ASSERT_THAT(smsDecoderUnderTest, NotNull());
    ASSERT_THAT(smsEncoderUnderTest, NotNull());
    ASSERT_THAT(talkEncoderUnderTest, NotNull());
    ASSERT_THAT(talkDecoderUnderTest, NotNull());
}

TEST_F(XorEncryptionTestSuite, shallHaveProperKind)
{
    EXPECT_EQ(EncryptionKind::Xor, smsEncoderUnderTest->getKind());
    EXPECT_EQ(EncryptionKind::Xor, talkEncoderUnderTest->getKind());
}

void XorEncryptionTestSuite::validateWrittenEncryption()
{
    common::BinaryMessage message = builder.getMessage();
    ASSERT_EQ(2, message.value.size());
    ASSERT_EQ(EncryptionKind::Xor, static_cast<EncryptionKind>(message.value[0]));
    ASSERT_EQ(XOR_KEY, message.value[1]);
}

TEST_F(XorEncryptionTestSuite, shallSmsEncoderWriteCodeAndKey)
{
    smsEncoderUnderTest->writeTo(builder);
    validateWrittenEncryption();
}

TEST_F(XorEncryptionTestSuite, shallTalkDecoderWriteCodeAndKey)
{
    talkDecoderUnderTest->writeTo(builder);
    validateWrittenEncryption();
}

TEST_F(XorEncryptionTestSuite, shallEncodeSms)
{
    ASSERT_EQ(ENCODED_TEXT, smsEncoderUnderTest->encodeSms(TEXT));
}

TEST_F(XorEncryptionTestSuite, shallDecodeSms)
{
    ASSERT_EQ(TEXT, smsDecoderUnderTest->decodeSms(ENCODED_TEXT));
}

TEST_F(XorEncryptionTestSuite, shallEncodeTalk)
{
    ASSERT_EQ(ENCODED_TEXT, talkEncoderUnderTest->encodeTalk(TEXT));
}

TEST_F(XorEncryptionTestSuite, shallDecodeTalk)
{
    ASSERT_EQ(TEXT, talkDecoderUnderTest->decodeTalk(ENCODED_TEXT));
}

TEST_F(XorEncryptionTestSuite, shallSmsEncoderPrintNameAndKey)
{
    assertPrintout(*smsEncoderUnderTest, expectedXorAndKeyInHex);
}

TEST_F(XorEncryptionTestSuite, shallSmsDecoderPrintNameAndKey)
{
    assertPrintout(*smsDecoderUnderTest, expectedXorAndKeyInHex);
}

TEST_F(XorEncryptionTestSuite, shallTalkEncoderPrintNameAndKey)
{
    assertPrintout(*talkEncoderUnderTest, expectedXorAndKeyInHex);
}

TEST_F(XorEncryptionTestSuite, shallTalkDecoderPrintNameAndKey)
{
    assertPrintout(*talkDecoderUnderTest, expectedXorAndKeyInHex);
}


class CaesarEncryptionTestSuite : public EncryptionFactoryTestSuite
{
protected:
    const std::uint8_t CAESAR_KEY = 21;
    const std::string CAESAR_KEY_STR = std::to_string(CAESAR_KEY);
    Matcher<std::string> expectedCaesarAndKeyStr = AllOf(
                HasSubstr("Caesar"),
                HasSubstr(CAESAR_KEY_STR));

    const std::string TEXT = "HA BA";
    const std::vector<std::uint8_t> ENCODED_BYTES {
                static_cast<std::uint8_t>('H' + CAESAR_KEY),
                static_cast<std::uint8_t>('A' + CAESAR_KEY),
                static_cast<std::uint8_t>(' ' + CAESAR_KEY),
                static_cast<std::uint8_t>('B' + CAESAR_KEY),
                static_cast<std::uint8_t>('A' + CAESAR_KEY)
    };
    const std::string ENCODED_TEXT{ENCODED_BYTES.begin(), ENCODED_BYTES.end()};

    common::BinaryMessage encryption{{static_cast<std::uint8_t>(EncryptionKind::Caesar),
                                      CAESAR_KEY}};
    common::IncomingMessage readerForSmsDecoder{encryption};
    common::IncomingMessage readerForTalkEncoder{encryption};
    common::OutgoingMessage builder{};

    IDecoderPtr smsDecoderUnderTest = objectUnderTest.readDecoder(readerForSmsDecoder);

    Expectation expectReadXorKeyForSmsEncoder = EXPECT_CALL(applicationEnvironmentMock, getProperty("enc_caesar", _)).WillOnce(Return(CAESAR_KEY));
    IEncoderPtr smsEncoderUnderTest = objectUnderTest.getEncoderFor(EncryptionKind::Caesar);

    IEncoderPtr talkEncoderUnderTest = objectUnderTest.readEncoder(readerForTalkEncoder);

    Expectation expectReadXorKeyForTalkDecoder = EXPECT_CALL(applicationEnvironmentMock, getProperty("dec_caesar", _)).WillOnce(Return(CAESAR_KEY));
    IDecoderPtr talkDecoderUnderTest = objectUnderTest.getDecoderFor(EncryptionKind::Caesar);
    void validateWrittenEncryption();
};

TEST_F(CaesarEncryptionTestSuite, shallCreate)
{
    ASSERT_THAT(smsDecoderUnderTest, NotNull());
    ASSERT_THAT(smsEncoderUnderTest, NotNull());
    ASSERT_THAT(talkEncoderUnderTest, NotNull());
    ASSERT_THAT(talkDecoderUnderTest, NotNull());
}

TEST_F(CaesarEncryptionTestSuite, shallHaveProperKind)
{
    EXPECT_EQ(EncryptionKind::Caesar, smsEncoderUnderTest->getKind());
    EXPECT_EQ(EncryptionKind::Caesar, talkEncoderUnderTest->getKind());
}

void CaesarEncryptionTestSuite::validateWrittenEncryption()
{
    common::BinaryMessage message = builder.getMessage();
    ASSERT_EQ(2, message.value.size());
    ASSERT_EQ(EncryptionKind::Caesar, static_cast<EncryptionKind>(message.value[0]));
    ASSERT_EQ(CAESAR_KEY, message.value[1]);
}

TEST_F(CaesarEncryptionTestSuite, shallSmsEncoderWriteCodeAndKey)
{
    smsEncoderUnderTest->writeTo(builder);
    validateWrittenEncryption();
}

TEST_F(CaesarEncryptionTestSuite, shallTalkDecoderWriteCodeAndKey)
{
    talkDecoderUnderTest->writeTo(builder);
    validateWrittenEncryption();
}

TEST_F(CaesarEncryptionTestSuite, shallEncodeSms)
{
    ASSERT_EQ(ENCODED_TEXT, smsEncoderUnderTest->encodeSms(TEXT));
}

TEST_F(CaesarEncryptionTestSuite, shallDecodeSms)
{
    ASSERT_EQ(TEXT, smsDecoderUnderTest->decodeSms(ENCODED_TEXT));
}

TEST_F(CaesarEncryptionTestSuite, shallEncodeTalk)
{
    ASSERT_EQ(ENCODED_TEXT, talkEncoderUnderTest->encodeTalk(TEXT));
}

TEST_F(CaesarEncryptionTestSuite, shallDecodeTalk)
{
    ASSERT_EQ(TEXT, talkDecoderUnderTest->decodeTalk(ENCODED_TEXT));
}

TEST_F(CaesarEncryptionTestSuite, shallSmsEncoderPrintNameAndKey)
{
    assertPrintout(*smsEncoderUnderTest, expectedCaesarAndKeyStr);
}

TEST_F(CaesarEncryptionTestSuite, shallSmsDecoderPrintNameAndKey)
{
    assertPrintout(*smsDecoderUnderTest, expectedCaesarAndKeyStr);
}

TEST_F(CaesarEncryptionTestSuite, shallTalkEncoderPrintNameAndKey)
{
    assertPrintout(*talkEncoderUnderTest, expectedCaesarAndKeyStr);
}

TEST_F(CaesarEncryptionTestSuite, shallTalkDecoderPrintNameAndKey)
{
    assertPrintout(*talkDecoderUnderTest, expectedCaesarAndKeyStr);
}

class RsaSmsEncoderTestSuite : public EncryptionFactoryTestSuite
{
protected:
    // must be longer than 256 bytes
    const std::string VERY_LONG_TEXT =
"[*][*]_010""[*][*]_020""[*][*]_030""[*][*]_040""[*][*]_050""[*][*]_060""[*][*]_070""[*][*]_080""[*][*]_090""[*][*]_100"
"[*][*]_110""[*][*]_120""[*][*]_130""[*][*]_140""[*][*]_150""[*][*]_160""[*][*]_170""[*][*]_180""[*][*]_190""[*][*]_200"
"[*][*]_210""[*][*]_220""[*][*]_230""[*][*]_240""[*][*]_250""[*][*]_260""[*][*]_270""[*][*]_280""[*][*]_290""[*][*]_300"
"[*][*]_310""[*][*]_320""[*][*]_330""[*][*]_340""[*][*]_350""[*][*]_360""[*][*]_370""[*][*]_380""[*][*]_390""[*][*]_400"
"[*][*]_410""[*][*]_420""[*][*]_430""[*][*]_440""[*][*]_450""[*][*]_460""[*][*]_470""[*][*]_480""[*][*]_490""[*][*]_500"
"[*][*]_510""[*][*]_520""[*][*]_530""[*][*]_540""[*][*]_550""[*][*]_560""[*][*]_570""[*][*]_580""[*][*]_590""[*][*]_600"
"hmmm";

    Matcher<std::string> expectedRsaAndBothKeys =
            AllOf(HasSubstr("Rsa"),

                HasSubstr("privkey: "),
                Not(HasSubstr("privkey: 0")),

                HasSubstr("pubkey: "),
                Not(HasSubstr("pubkey: 0")));

    Matcher<std::string> expectedRsaAndPubKeyOnly =
            AllOf(HasSubstr("Rsa"),

                HasSubstr("privkey: 0"),

                HasSubstr("pubkey: "),
                Not(HasSubstr("pubkey: 0")));

    const std::string TEXT = "HA BA";
    IEncoderPtr smsEncoderUnderTest = objectUnderTest.getEncoderFor(EncryptionKind::Rsa);

    void validateWrittenEncyption(common::OutgoingMessage &builder);
};

TEST_F(RsaSmsEncoderTestSuite, shallCreate)
{
    ASSERT_THAT(smsEncoderUnderTest, NotNull());
}

TEST_F(RsaSmsEncoderTestSuite, shallHaveProperKind)
{
    EXPECT_EQ(EncryptionKind::Rsa, smsEncoderUnderTest->getKind());
}

TEST_F(RsaSmsEncoderTestSuite, shallEncodeSms)
{
    std::string encodedSms = smsEncoderUnderTest->encodeSms(TEXT);
    EXPECT_NE(TEXT, encodedSms);
}

TEST_F(RsaSmsEncoderTestSuite, shallEncodeLongSms)
{
    std::string encodedLongSms = smsEncoderUnderTest->encodeSms(VERY_LONG_TEXT);
    EXPECT_NE(VERY_LONG_TEXT, encodedLongSms);
}

void RsaSmsEncoderTestSuite::validateWrittenEncyption(common::OutgoingMessage& builder)
{
    common::BinaryMessage message = builder.getMessage();
    std::size_t requiredHeaderSize = sizeof(EncryptionKind) + sizeof(std::uint16_t);
    ASSERT_GT(message.value.size(), requiredHeaderSize);

    common::IncomingMessage reader(message);
    EncryptionKind actualWrittenKind = static_cast<EncryptionKind>(reader.readNumber<std::uint8_t>());
    EXPECT_EQ(EncryptionKind::Rsa, actualWrittenKind);
    std::uint16_t actualWrittenSize = reader.readNumber<std::uint16_t>();
    ASSERT_EQ(actualWrittenSize + requiredHeaderSize, message.value.size());
    std::string actualKey = reader.readRemainingText();
    ASSERT_THAT(actualKey, HasSubstr("BEGIN RSA PUBLIC KEY"));
    ASSERT_THAT(actualKey, HasSubstr("END RSA PUBLIC KEY"));
}

TEST_F(RsaSmsEncoderTestSuite, shallWriteCodeAndPublicKey)
{
    common::OutgoingMessage builder;
    smsEncoderUnderTest->writeTo(builder);
    validateWrittenEncyption(builder);
}

TEST_F(RsaSmsEncoderTestSuite, shallSmsEncoderDescribeItself)
{
    assertPrintout(*smsEncoderUnderTest, expectedRsaAndBothKeys);
}

class RsaSmsDecoderTestSuite : public RsaSmsEncoderTestSuite
{
protected:
    const std::string ENCODED_SMS_TEXT = smsEncoderUnderTest->encodeSms(TEXT);
    const std::string ENCODED_VERY_LONG_SMS_TEXT = smsEncoderUnderTest->encodeSms(VERY_LONG_TEXT);

    RsaSmsDecoderTestSuite()
    {
        common::OutgoingMessage builder{};
        smsEncoderUnderTest->writeTo(builder);

        common::BinaryMessage encryption = builder.getMessage();
        common::IncomingMessage reader{encryption};

         smsDecoderUnderTest = objectUnderTest.readDecoder(reader);
    }

    IDecoderPtr smsDecoderUnderTest;
};

TEST_F(RsaSmsDecoderTestSuite, shallCreate)
{
    ASSERT_THAT(smsDecoderUnderTest, NotNull());
}

TEST_F(RsaSmsDecoderTestSuite, shallDecodeSmsInSymetricWay)
{
    ASSERT_EQ(TEXT, smsDecoderUnderTest->decodeSms(ENCODED_SMS_TEXT));
}

TEST_F(RsaSmsDecoderTestSuite, shallDecodeLongSmsInSymetricWay)
{
    ASSERT_EQ(VERY_LONG_TEXT, smsDecoderUnderTest->decodeSms(ENCODED_VERY_LONG_SMS_TEXT));
}

TEST_F(RsaSmsDecoderTestSuite, shallSmsDecoderDescribeItself)
{
    assertPrintout(*smsDecoderUnderTest, expectedRsaAndPubKeyOnly);
}

class RsaTalkEncoderTestSuite : public RsaSmsEncoderTestSuite
{
protected:
    const std::string ENCODED_SMS_TEXT = smsEncoderUnderTest->encodeSms(TEXT);

    RsaTalkEncoderTestSuite()
    {
        common::OutgoingMessage builder{};
        smsEncoderUnderTest->writeTo(builder);

        common::BinaryMessage encryption = builder.getMessage();
        common::IncomingMessage reader{encryption};

         talkEncoderUnderTest = objectUnderTest.readEncoder(reader);
    }

    IEncoderPtr talkEncoderUnderTest;
};

TEST_F(RsaTalkEncoderTestSuite, shallCreate)
{
    ASSERT_THAT(talkEncoderUnderTest, NotNull());
}

TEST_F(RsaTalkEncoderTestSuite, shallHaveProperKind)
{
    EXPECT_EQ(EncryptionKind::Rsa, talkEncoderUnderTest->getKind());
}

TEST_F(RsaTalkEncoderTestSuite, shallEncodeTalk)
{
    std::string encodedTalk = talkEncoderUnderTest->encodeTalk(TEXT);
    EXPECT_NE(TEXT, encodedTalk);
}

TEST_F(RsaTalkEncoderTestSuite, shallEncodeLongTalk)
{
    std::string encodedTalk = talkEncoderUnderTest->encodeTalk(VERY_LONG_TEXT);
    EXPECT_NE(VERY_LONG_TEXT, encodedTalk);
}

TEST_F(RsaTalkEncoderTestSuite, shallWriteCodeAndPublicKey)
{
    common::OutgoingMessage builder;
    talkEncoderUnderTest->writeTo(builder);
    validateWrittenEncyption(builder);
}

TEST_F(RsaTalkEncoderTestSuite, shallTalkEncoderDescribeItself)
{
    assertPrintout(*talkEncoderUnderTest, expectedRsaAndPubKeyOnly);
}

class RsaTalkDecoderTestSuite : public RsaTalkEncoderTestSuite
{
protected:
    const std::string ENCODED_TEXT = talkEncoderUnderTest->encodeTalk(TEXT);
    const std::string ENCODED_VERY_LONG_TEXT = talkEncoderUnderTest->encodeTalk(VERY_LONG_TEXT);
    IDecoderPtr talkDecoderUnderTest = objectUnderTest.getDecoderFor(EncryptionKind::Rsa);
};

TEST_F(RsaTalkDecoderTestSuite, shallCreate)
{
    ASSERT_THAT(talkDecoderUnderTest, NotNull());
}

TEST_F(RsaTalkDecoderTestSuite, shallDecodeTalk)
{
    std::string decodedTalk = talkDecoderUnderTest->decodeTalk(ENCODED_TEXT);
    EXPECT_EQ(TEXT, decodedTalk);
}

TEST_F(RsaTalkDecoderTestSuite, shallDecodeLongTalk)
{
    std::string decodedTalk = talkDecoderUnderTest->decodeTalk(ENCODED_VERY_LONG_TEXT);
    EXPECT_EQ(VERY_LONG_TEXT, decodedTalk);
}

TEST_F(RsaTalkDecoderTestSuite, shallWriteCodeAndPublicKey)
{
    common::OutgoingMessage builder;
    talkDecoderUnderTest->writeTo(builder);
    validateWrittenEncyption(builder);
}

TEST_F(RsaTalkDecoderTestSuite, shallTalkDecoderDescribeItself)
{
    assertPrintout(*talkDecoderUnderTest, expectedRsaAndBothKeys);
}

}
