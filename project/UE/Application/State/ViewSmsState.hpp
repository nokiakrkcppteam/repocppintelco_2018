#pragma once

#include "BaseState.hpp"

namespace ue
{

class ViewSmsState : public BaseState
{
public:
    ViewSmsState(StateContext& stateContext, ISmsDB::Index smsIndex);
    void handleExitView() override;
};

}
