#pragma once

#include "ITicker.hpp"
#include "Logger/PrefixedLogger.hpp"

#include <chrono>

namespace ue
{

class SteadyClockTicker : public ITicker
{
public:
    SteadyClockTicker(ITicker::Duration oneTick, common::ILogger& logger);

    TimePoint getTimeAfter(Duration duration) const;
    bool isExpired(TimePoint timePoint) const;
    void waitOneTick();

private:
    ITicker::Duration oneTick;
    common::PrefixedLogger logger;
};

}
