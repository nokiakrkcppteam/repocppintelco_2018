#include "ViewSmsState.hpp"

namespace ue
{

ViewSmsState::ViewSmsState(StateContext& stateContext, ISmsDB::Index smsIndex)
    : BaseState(stateContext, "[VIEW-SMS] ")
{
    const auto& text = smsDB.getSmsFullRepresentation(smsIndex);
    logger.logDebug("Show: ", smsIndex);
    user.showSms(text);
}

void ViewSmsState::handleExitView()
{
    logger.logInfo("Back to view Smses");
    setViewSmsesState();
}

}
