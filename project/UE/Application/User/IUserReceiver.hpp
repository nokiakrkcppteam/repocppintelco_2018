#pragma once

#include "Messages.hpp"
#include "Sms/ISmsDB.hpp"
#include "Crypto/IEncryption.hpp"

namespace ue
{

class  IUserReceiver
{
public:
    virtual ~IUserReceiver() = default;

    virtual void handleExitView() = 0;
    virtual void handleSendSms(common::PhoneNumber to, const std::string& text) = 0;
    virtual void handleEditSms(IEncoderPtr smsEncoder) = 0;
    virtual void handleViewSms(ISmsDB::Index smsIndex) = 0;
    virtual void handleViewSmses() = 0;
    virtual void handleUserAccept() = 0;
    virtual void handleUserTalk(const std::string& text) = 0;
    virtual void handleDialing(IDecoderPtr talkDecoder) = 0;
    virtual void handleUserCalling(common::PhoneNumber to) = 0;
};

}
