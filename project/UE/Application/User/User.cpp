#include "User.hpp"
#include "UeGui/IListViewMode.hpp"
#include "UeGui/ITextMode.hpp"
#include "UeGui/ISmsComposeMode.hpp"
#include "UeGui/ICallMode.hpp"
#include "UeGui/IDialMode.hpp"
#include "IUserReceiver.hpp"
#include <algorithm>

namespace ue
{


const std::size_t User::VIEW_SMSES_MENU_INDEX;
const std::size_t User::SEND_SMS_MENU_INDEX;
const std::size_t User::SEND_SMS_XOR_MENU_INDEX;
const std::size_t User::CALL_MENU_INDEX;

User::User(common::PhoneNumber phoneNumber,
           IUeGui &ueGui,
           ISmsDB &smsDB,
           IEncryptionFactory &encryptionFactory,
           common::ILogger &iLogger)
    : phoneNumber(phoneNumber),
      ueGui(ueGui),
      smsDB(smsDB),
      logger(iLogger, [this](std::ostream& os) { printPrefix(os); }),
      idleReceiver(logger),
      receiver(idleReceiver)
{
    logger.logInfo("Started");
    ueGui.setTitle("UE:" + to_string(phoneNumber));
    addMenuOption(VIEW_SMSES_MENU_INDEX,
                  [this]{ getReceiver().handleViewSmses(); },
                  "View Smses");
    addMenuOption(SEND_SMS_MENU_INDEX,
                  [this, encoder = encryptionFactory.getEncoderFor(EncryptionKind::None)]
                  {
                      getReceiver().handleEditSms(encoder);
                  },
                  "Send Sms");
    addMenuOption(SEND_SMS_XOR_MENU_INDEX,
                  [this, encoder = encryptionFactory.getEncoderFor(EncryptionKind::Xor)]
                  {
                      getReceiver().handleEditSms(encoder);
                  },
                  "Send Sms (Xor)");
    addMenuOption(SEND_SMS_CAESAR_MENU_INDEX,
                  [this, encoder = encryptionFactory.getEncoderFor(EncryptionKind::Caesar)]
                  {
                      getReceiver().handleEditSms(encoder);
                  },
                  "Send Sms (Caesar)");
    addMenuOption(SEND_SMS_RSA_MENU_INDEX,
                  [this, encoder = encryptionFactory.getEncoderFor(EncryptionKind::Rsa)]
                  {
                      getReceiver().handleEditSms(encoder);
                  },
                  "Send Sms (Rsa)");
    addMenuOption(CALL_MENU_INDEX,
                  [this, decoder = encryptionFactory.getDecoderFor(EncryptionKind::None)]
                  {
                      getReceiver().handleDialing(decoder);
                  },
                  "Call");
    addMenuOption(CALL_XOR_MENU_INDEX,
                  [this, decoder = encryptionFactory.getDecoderFor(EncryptionKind::Xor)]
                  {
                      getReceiver().handleDialing(decoder);
                  },
                  "Call (Xor)");
    addMenuOption(CALL_CAESAR_MENU_INDEX,
                  [this, decoder = encryptionFactory.getDecoderFor(EncryptionKind::Caesar)]
                  {
                      getReceiver().handleDialing(decoder);
                  },
                  "Call (Caesar)");
    addMenuOption(CALL_RSA_MENU_INDEX,
                  [this, decoder = encryptionFactory.getDecoderFor(EncryptionKind::Rsa)]
                  {
                      getReceiver().handleDialing(decoder);
                  },
                  "Call (Rsa)");

    ueGui.setRejectCallback([this]{
        getReceiver().handleExitView();
    });
}

User::~User()
{
    logger.logInfo("Stopped");
}

void User::showNotConnected()
{
    logger.logDebug("Not Connected");
    ueGui.showNotConnected();
    ueGui.setAcceptCallback([this]{
        logger.logDebug("Accept ignored in Not Connected!");
    });
}

void User::showConnecting()
{
    logger.logDebug("Connecting");
    ueGui.showConnecting();
    ueGui.setAcceptCallback([this]{
        logger.logDebug("Accept ignored in Connecting!");
    });
}

void User::showConnected()
{
    logger.logDebug("Main Menu");
    IUeGui::IListViewMode& listView = ueGui.setListViewMode();
    listView.clearSelectionList();
    for (auto&& option : menuOptions)
    {
        listView.addSelectionListItem(option.name, option.tip);
    }
    ueGui.setAcceptCallback([this, &listView] {
        auto currentSelection = listView.getCurrentItemIndex();
        if (currentSelection.first)
        {
            menuOptions.at(currentSelection.second).handler();
        }
    });
}

void User::showSmses()
{
    logger.logDebug("All Smses");
    IUeGui::IListViewMode& listView = ueGui.setListViewMode();
    std::vector<ISmsDB::Index> smsIndices;
    listView.clearSelectionList();
    smsDB.visitAll([this, &listView, &smsIndices](auto index, auto&& text)
    {
        listView.addSelectionListItem(text, "");
        smsIndices.push_back(index);
        return true;
    });
    ueGui.setAcceptCallback([this, &listView, indices = std::move(smsIndices)] {
        auto currentSelection = listView.getCurrentItemIndex();
        if (currentSelection.first)
        {
            auto smsIdenx = indices.at(currentSelection.second);
            getReceiver().handleViewSms(smsIdenx);
        }
    });

}

void User::showSms(const std::string &text)
{
    logger.logDebug("Show Sms: ", text);
    ueGui.setViewTextMode().setText(text);
}

void User::editSms()
{
    IUeGui::ISmsComposeMode& composeMode = ueGui.setSmsComposeMode();
    composeMode.clearSmsText();
    ueGui.setAcceptCallback([this, &composeMode] {
        auto to = composeMode.getPhoneNumber();
        if (to != common::PhoneNumber{})
        {
            auto text = composeMode.getSmsText();
            logger.logDebug("Send sms to: ", to);
            getReceiver().handleSendSms(to, text);

        }
        else
        {
            logger.logDebug("Ignoring invalid number in edit sms mode");
        }
    });
}

void User::showPeerCalling(common::PhoneNumber from)
{
    logger.logDebug("Call from: ", from);
    ueGui.setAlertMode().setText("Call from " + to_string(from) + "...");
    ueGui.setAcceptCallback([this] {
        getReceiver().handleUserAccept();
    });
}

void User::showTalkingView(common::PhoneNumber to)
{
    logger.logDebug("Talking with: ", to);
    auto& callMode = ueGui.setCallMode();
    callMode.clearOutgoingText();
    ueGui.setAcceptCallback([this, &callMode] {
        getReceiver().handleUserTalk(callMode.getOutgoingText());
        callMode.clearOutgoingText();
    });
}

void User::addPeerUserTalk(common::PhoneNumber from, const std::string &text)
{
    logger.logDebug("Add peer user talk: ", text);
    ueGui.setCallMode().appendIncomingText("[" + to_string(from) + "] =>> " + text);
}

void User::addUserTalk(common::PhoneNumber to, const std::string &text)
{
    logger.logDebug("Add  user talk: ", text);
    ueGui.setCallMode().appendIncomingText("[" + to_string(to) + "] <<= " + text);
}

void User::showDialing()
{
    logger.logDebug("Show dialing");
    auto& dialMode = ueGui.setDialMode();
    ueGui.setAcceptCallback([this, &dialMode] {
        auto peerPhone = dialMode.getPhoneNumber();
        if (peerPhone.isValid())
        {
            getReceiver().handleUserCalling(peerPhone);
        }
    });
}

void User::showUserCalling(common::PhoneNumber to)
{
    logger.logDebug("Dialing to: ", to);
    ueGui.setAlertMode().setText("Calling " + to_string(to) + "...");
    ueGui.setAcceptCallback([this] {
        getReceiver().handleUserAccept();
    });
}



void User::setReceiver(IUserReceiver &newReceiver)
{
    receiver = newReceiver;
}

void User::resetReceiver()
{
    receiver = idleReceiver;
}

IUserReceiver &User::getReceiver()
{
    return receiver.get();
}

void User::printPrefix(std::ostream &os) const
{
    os << "[User] ";
}

void User::addMenuOption(MenuIndex index, MenuHandler handler, const std::string &optionName, const std::string &optionTip)
{
    menuOptions.emplace_back(MenuOption{optionName, optionTip, std::move(handler)});
    if (index + 1 != menuOptions.size())
    {
        throw std::logic_error("Construction of menu failed: index("
                               + std::to_string(index) + ") + 1 != size(" + std::to_string(menuOptions.size())
                               + ") for \"" + optionName + "\"");
    }
}

User::IdleReceiver::IdleReceiver(common::ILogger &iLogger)
    : logger(iLogger, "[IDLE] ")
{}

void User::IdleReceiver::handleExitView()
{
    logger.logDebug("handleExitView()");
}

void User::IdleReceiver::handleSendSms(common::PhoneNumber to, const std::string &text)
{
    logger.logDebug("handleSendSms(", to, ", ", text.substr(10), "*)");
}

void User::IdleReceiver::handleEditSms(IEncoderPtr smsEncoder)
{
    logger.logDebug("handleEditSms(", *smsEncoder, ")");
}

void User::IdleReceiver::handleViewSms(ISmsDB::Index smsIndex)
{
    logger.logDebug("handleViewSms(", smsIndex, ")");
}

void User::IdleReceiver::handleViewSmses()
{
    logger.logDebug("handleViewSmses()");
}

void User::IdleReceiver::handleUserAccept()
{
    logger.logDebug("handleUserAccept()");
}

void User::IdleReceiver::handleUserTalk(const std::string &text)
{
    logger.logDebug("handleUserTalk(", text, ")");
}

void User::IdleReceiver::handleDialing(IDecoderPtr talkDecoder)
{
    logger.logDebug("handleUserDialing(", *talkDecoder, ")");
}

void User::IdleReceiver::handleUserCalling(common::PhoneNumber to)
{
    logger.logDebug("handleUserCalling(", to, ")");
}

}
