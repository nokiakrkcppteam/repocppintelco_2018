#include "StateContext.hpp"

namespace ue
{

void StateContext::setNotConnectedState()
{
    stateLocator.setState(stateFactory.createNotConnectedState(*this));
}

void StateContext::setConnectingState()
{
    stateLocator.setState(stateFactory.createConnectingState(*this));
}

void StateContext::setConnectedState()
{
    stateLocator.setState(stateFactory.createConnectedState(*this));
}

void StateContext::setEditSmsState(IEncoderPtr smsEncoder)
{
    stateLocator.setState(stateFactory.createEditSmsState(*this, smsEncoder));
}

void StateContext::setViewSmsState(ISmsDB::Index smsIndex)
{
    stateLocator.setState(stateFactory.createViewSmsState(*this, smsIndex));
}

void StateContext::setViewSmsesState()
{
    stateLocator.setState(stateFactory.createViewSmsesState(*this));
}

void StateContext::setPeerCallingState(common::PhoneNumber from, IEncoderPtr talkEncoder)
{
    stateLocator.setState(stateFactory.createPeerCallingState(*this, from, talkEncoder));
}

void StateContext::setTalkingState(common::PhoneNumber to, IEncoderPtr talkEncoder, IDecoderPtr talkDecoder)
{
    stateLocator.setState(stateFactory.createTalkingState(*this, to, talkEncoder, talkDecoder));
}

void StateContext::setDialingState(IDecoderPtr talkDecoder)
{
    stateLocator.setState(stateFactory.createDialingState(*this, talkDecoder));
}

void StateContext::setUserCallingState(common::PhoneNumber to, IDecoderPtr talkDecoder)
{
    stateLocator.setState(stateFactory.createUserCallingState(*this, to, talkDecoder));
}

}
