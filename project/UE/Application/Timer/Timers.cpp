#include "Timers.hpp"

namespace ue
{

constexpr ITicker::Duration Timers::ATTACH_TIMER_DURATION;
constexpr ITicker::Duration Timers::PEER_CALLING_TIMER_DURATION;
constexpr ITicker::Duration Timers::USER_CALLING_TIMER_DURATION;
constexpr ITicker::Duration Timers::TALKING_TIMER_DURATION;

Timers::Timers(ITicker &ticker, common::ILogger &logger)
    : ticker(ticker),
      logger(logger, "[Timers] "),
      attachTimer(&ITimeoutsReceiver::handleAttachTimeout),
      peerCallingTimer(&ITimeoutsReceiver::handlePeerCallingTimeout),
      userCallingTimer(&ITimeoutsReceiver::handleUserCallingTimeout),
      talkingTimer(&ITimeoutsReceiver::handleTalkingTimeout),
      timers{attachTimer, peerCallingTimer, userCallingTimer, talkingTimer}
{}

Timers::~Timers() = default;

void Timers::startAttachTimer()
{
    guardedOperation([this]{
        attachTimer.startTimerFor(ticker, ATTACH_TIMER_DURATION);
    });
    logger.logInfo("startAttachTimer");
}

void Timers::stopAttachTimer()
{
    guardedOperation([this]{
        attachTimer.stopTimer();
    });
    logger.logInfo("stopAttachTimer");
}

void Timers::startPeerCallingTimer()
{
    guardedOperation([this]{
        peerCallingTimer.startTimerFor(ticker, PEER_CALLING_TIMER_DURATION);
    });
    logger.logInfo("startPeerCallingTimer");
}

void Timers::stopPeerCallingTimer()
{
    guardedOperation([this]{
        peerCallingTimer.stopTimer();
    });
    logger.logInfo("stopPeerCallingTimer");
}

void Timers::startUserCallingTimer()
{
    guardedOperation([this]{
        userCallingTimer.startTimerFor(ticker, USER_CALLING_TIMER_DURATION);
    });
    logger.logInfo("startUserCallingTimer");
}

void Timers::stopUserCallingTimer()
{
    guardedOperation([this]{
        userCallingTimer.stopTimer();
    });
    logger.logInfo("stopUserCallingTimer");
}

void Timers::startTalkingTimer()
{
    guardedOperation([this]{
        talkingTimer.startTimerFor(ticker, TALKING_TIMER_DURATION);
    });
    logger.logInfo("startTalkingTimer");
}

void Timers::stopTalkingTimer()
{
    guardedOperation([this]{
        talkingTimer.stopTimer();
    });
    logger.logInfo("stopTalkingTimer");
}

void Timers::timeoutExpiredTimers()
{
    for (auto& timer : timers)
    {
        Timer::Handler* handler = guardedOperation([this, &timer = timer.get()]
        {
            return timer.tryExpire(ticker);
        });
        if (handler)
        {
            // not guarded because timeout might trigger start/stop other timers
            (receiver.get().**handler)();
        }
    }
}

void Timers::setReceiver(ITimeoutsReceiver &receiver)
{
    this->receiver = receiver;
}

void Timers::resetReceiver()
{
    receiver = nullReceiver;
}

Timers::ReceiverConnector::ReceiverConnector(Timers &timers, ITimeoutsReceiver &receiver)
    : timers(timers)
{
    timers.setReceiver(receiver);
}

Timers::ReceiverConnector::~ReceiverConnector()
{
    timers.resetReceiver();
}

Timers::Timer::Timer(Timers::Timer::Handler handler)
    : handler(handler)
{}

void Timers::Timer::startTimerFor(ITicker &ticker, ITicker::Duration duration)
{
    pointOfExpire = ticker.getTimeAfter(duration);
    isSet = true;
}

void Timers::Timer::stopTimer()
{
    isSet = false;
}

Timers::Timer::Handler *Timers::Timer::tryExpire(ITicker &ticker)
{
    if (isSet and ticker.isExpired(pointOfExpire))
    {
        isSet = false;
        return &handler;
    }
    else
    {
        return nullptr;
    }
}

Timers::NullReceiver::NullReceiver(common::ILogger &logger)
    : logger(logger, "[NULL-Receiver] ")
{}

void Timers::NullReceiver::handleAttachTimeout()
{
    logger.logDebug("handleAttachTimeout");
}

void Timers::NullReceiver::handlePeerCallingTimeout()
{
    logger.logDebug("handlePeerCallingTimeout");
}

void Timers::NullReceiver::handleUserCallingTimeout()
{
    logger.logDebug("handleUserCallingTimeout");
}

void Timers::NullReceiver::handleTalkingTimeout()
{
    logger.logDebug("handleTalkingTimeout");
}

}
