#include "ConnectingState.hpp"

namespace ue
{

ConnectingState::ConnectingState(StateContext& stateContext)
    : BaseState(stateContext, "[Connecting] ")
{
    user.showConnecting();
    timers.startAttachTimer();
}

void ConnectingState::handleSib()
{
    logger.logError("Unexpected SIB - procedure restarted");
    timers.startAttachTimer();
    btsSender.sendAttachRequest();
}

void ConnectingState::handleAttachAccept()
{
    timers.stopAttachTimer(); // it is short timer - needs to be called first
    logger.logInfo("Handling Attach Accepted");
    setConnectedState();
}

void ConnectingState::handleAttachReject()
{
    logger.logError("Attach failed!");
    timers.stopAttachTimer();
    setNotConnectedState();
}

void ConnectingState::handleAttachTimeout()
{
    logger.logError("Attach timeout!");
    setNotConnectedState();
}

}
