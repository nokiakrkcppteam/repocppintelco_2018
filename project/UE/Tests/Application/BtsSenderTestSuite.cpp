#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <type_traits>

#include "Bts/BtsSender.hpp"
#include "Mocks/ILoggerMock.hpp"
#include "Mocks/ITransportMock.hpp"
#include "Mocks/IEncryptionMock.hpp"
#include "Messages.hpp"
#include "Messages/IncomingMessage.hpp"
#include "Messages/OutgoingMessage.hpp"
#include "Crypto/EncryptionKind.hpp"

namespace ue
{
using namespace ::testing;

class BtsSenderTestSuite : public Test
{
protected:
    const common::PhoneNumber PHONE_NUMBER{112};
    const common::PhoneNumber PEER_PHONE_NUMBER{18};
    const std::string SMS_TEXT = "Some SMS Text \n with many lines... \n \n";
    const std::string ENCRYPTED_SMS_TEXT = "#@%$@FWFQWF !%T#@%VF  $$#Q";
    const std::string CALL_TALK_TEXT = "Advanced - WoW";
    NiceMock<common::ILoggerMock> loggerMock;
    StrictMock<common::ITransportMock> transportMock;

    const EncryptionKind SMS_ENCRYPTION_KIND = EncryptionKind::Caesar;
    const std::uint32_t SMS_ENCRYPTION_DATA = 123456u;
    std::shared_ptr<IEncoderMock> smsEncoderMock = std::make_shared<StrictMock<IEncoderMock>>();
    Expectation expectPrintFromSmsEncoder = EXPECT_CALL(*smsEncoderMock, printData(_)).Times(AnyNumber());

    const EncryptionKind TALK_ENCRYPTION_KIND = EncryptionKind::Rsa;
    const std::uint16_t TALK_ENCRYPTION_DATA = 2356u;
    std::shared_ptr<IDecoderMock> talkDecoderMock = std::make_shared<StrictMock<IDecoderMock>>();
    Expectation expectPrintFromTalkDecoder = EXPECT_CALL(*talkDecoderMock, printData(_)).Times(AnyNumber());

    common::BinaryMessage message;
    BtsSender objectUnderTest{PHONE_NUMBER,
                              transportMock,
                              loggerMock};

    void expectSend();
    template <typename Mock, typename EncryptionData>
    void expectWriteEncryption(Mock& mock, EncryptionKind encryptionKind, EncryptionData encryptionData);
    template <typename EncryptionData>
    void verifyWrittenEncryption(common::IncomingMessage& reader, EncryptionKind expectedKind, EncryptionData expectedData);
    common::IncomingMessage verifySentMessage(common::MessageId expectedMsg, common::PhoneNumber expectedTo = common::PhoneNumber{});
};

void BtsSenderTestSuite::expectSend()
{
    EXPECT_CALL(transportMock, sendMessage(_)).WillOnce(SaveArg<0>(&message));
}

common::IncomingMessage BtsSenderTestSuite::verifySentMessage(common::MessageId expectedMsg, common::PhoneNumber expectedTo)
{
    common::IncomingMessage reader(message);
    auto header = reader.readMessageHeader();
    EXPECT_EQ(expectedMsg, header.messageId);
    EXPECT_EQ(PHONE_NUMBER, header.from);
    EXPECT_EQ(expectedTo, header.to);
    return reader;
}

template <typename Mock, typename EncryptionData>
void BtsSenderTestSuite::expectWriteEncryption(Mock& mock, EncryptionKind encryptionKind, EncryptionData encryptionData)
{
    EXPECT_CALL(mock, writeTo(_)).WillOnce(Invoke([=](common::OutgoingMessage& builder)
    {
        builder.writeNumber(static_cast<std::uint8_t>(encryptionKind));
        builder.writeNumber(encryptionData);
    }));
}

template <typename EncryptionData>
void BtsSenderTestSuite::verifyWrittenEncryption(common::IncomingMessage& reader, EncryptionKind expectedKind, EncryptionData expectedData)
{
    EncryptionKind encryptionKind = static_cast<EncryptionKind>(reader.readNumber<std::uint8_t>());
    EXPECT_EQ(expectedKind, encryptionKind);
    auto encryptionData = reader.readNumber<EncryptionData>();
    EXPECT_EQ(expectedData, encryptionData);
}


TEST_F(BtsSenderTestSuite, shallSendAttachRequest)
{
    expectSend();

    objectUnderTest.sendAttachRequest();

    common::IncomingMessage reader = verifySentMessage(common::MessageId::AttachRequest);
    ASSERT_NO_THROW(reader.checkEndOfMessage());
}

TEST_F(BtsSenderTestSuite, shallSendSms)
{
    EXPECT_CALL(*smsEncoderMock, encodeSms(SMS_TEXT)).WillOnce(Return(ENCRYPTED_SMS_TEXT));
    expectWriteEncryption(*smsEncoderMock, SMS_ENCRYPTION_KIND, SMS_ENCRYPTION_DATA);
    expectSend();

    objectUnderTest.sendSms(PEER_PHONE_NUMBER, smsEncoderMock, SMS_TEXT);

    common::IncomingMessage reader = verifySentMessage(common::MessageId::Sms,
                                                       PEER_PHONE_NUMBER);
    verifyWrittenEncryption(reader, SMS_ENCRYPTION_KIND, SMS_ENCRYPTION_DATA);
    std::string smsText = reader.readRemainingText();
    EXPECT_EQ(ENCRYPTED_SMS_TEXT, smsText);
}

TEST_F(BtsSenderTestSuite, shallSendCallDrop)
{
    expectSend();

    objectUnderTest.sendCallDrop(PEER_PHONE_NUMBER);

    common::IncomingMessage reader = verifySentMessage(common::MessageId::CallDropped,
                                                       PEER_PHONE_NUMBER);
    ASSERT_NO_THROW(reader.checkEndOfMessage());
}

TEST_F(BtsSenderTestSuite, shallSendCallAccept)
{
    expectWriteEncryption(*talkDecoderMock, TALK_ENCRYPTION_KIND, TALK_ENCRYPTION_DATA);
    expectSend();

    objectUnderTest.sendCallAccept(PEER_PHONE_NUMBER, talkDecoderMock);

    common::IncomingMessage reader = verifySentMessage(common::MessageId::CallAccepted,
                                                       PEER_PHONE_NUMBER);
    verifyWrittenEncryption(reader, TALK_ENCRYPTION_KIND, TALK_ENCRYPTION_DATA);
    ASSERT_NO_THROW(reader.checkEndOfMessage());
}

TEST_F(BtsSenderTestSuite, shallSendCallTalk)
{
    expectSend();

    objectUnderTest.sendCallTalk(PEER_PHONE_NUMBER, CALL_TALK_TEXT);

    common::IncomingMessage reader = verifySentMessage(common::MessageId::CallTalk,
                                                       PEER_PHONE_NUMBER);
    std::string talkText = reader.readRemainingText();
    EXPECT_EQ(CALL_TALK_TEXT, talkText);
}

TEST_F(BtsSenderTestSuite, shallSendCallRequest)
{
    expectWriteEncryption(*talkDecoderMock, TALK_ENCRYPTION_KIND, TALK_ENCRYPTION_DATA);
    expectSend();

    objectUnderTest.sendCallRequest(PEER_PHONE_NUMBER, talkDecoderMock);

    common::IncomingMessage reader = verifySentMessage(common::MessageId::CallRequest,
                                                       PEER_PHONE_NUMBER);
    verifyWrittenEncryption(reader, TALK_ENCRYPTION_KIND, TALK_ENCRYPTION_DATA);
    ASSERT_NO_THROW(reader.checkEndOfMessage());
}

}

