#pragma once

#include "Messages.hpp"
#include <string>
#include <chrono>

namespace ue
{

struct Sms
{
    Sms(common::PhoneNumber from, common::PhoneNumber to, const std::string& text);

    std::string toFullString(common::PhoneNumber host) const;
    std::string toShortString(common::PhoneNumber host) const;

    common::PhoneNumber from;
    common::PhoneNumber to;
    std::string text;
    std::chrono::system_clock::time_point when;

private:
    void toPhoneNumbers(std::ostream& sstream, common::PhoneNumber host) const;
    void toShortWhen(std::ostream &sstream) const;
    void toShortText(std::ostream &sstream) const;
};

}
