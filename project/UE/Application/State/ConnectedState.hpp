#pragma once

#include "State/BaseState.hpp"

namespace ue
{

class ConnectedState : public BaseState
{
public:
    ConnectedState(StateContext& stateContext);

    void handleViewSmses() override;
    void handleEditSms(IEncoderPtr smsEncoder) override;
    void handleCallRequestFrom(common::PhoneNumber from, IEncoderPtr callEncoder) override;
    void handleDialing(IDecoderPtr talkDecoder) override;
};

}
