#include "IEncryptionMock.hpp"

namespace ue
{

IEncoderMock::IEncoderMock() = default;
IEncoderMock::~IEncoderMock() = default;

IDecoderMock::IDecoderMock() = default;
IDecoderMock::~IDecoderMock() = default;

}
