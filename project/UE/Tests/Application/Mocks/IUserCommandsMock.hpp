#pragma once

#include <gmock/gmock.h>
#include "User/IUserCommands.hpp"
#include "User/IUserReceiver.hpp"

namespace ue
{

struct IUserCommandsMock : public IUserCommands
{
    IUserCommandsMock();
    ~IUserCommandsMock() override;

    MOCK_METHOD1(setReceiver, void(IUserReceiver&));
    MOCK_METHOD0(resetReceiver, void());
};

}
