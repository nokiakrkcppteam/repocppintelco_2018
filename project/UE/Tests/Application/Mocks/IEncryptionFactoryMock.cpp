#include "IEncryptionFactoryMock.hpp"

namespace ue
{

IEncryptionFactoryMock::IEncryptionFactoryMock() = default;
IEncryptionFactoryMock::~IEncryptionFactoryMock() = default;

}
