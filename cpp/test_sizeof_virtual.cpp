#include <iostream>
#include "Verbose.hpp"

class A
{
public:
    void printA() const { std::cout << a;   }
    void printB() const { std::cout << b;   }

private:
    int a;
    char b;
};

class BV;
void foo(BV& bv);

class BV
{
public:
    ~BV()
    {
        foo(*this);
    }
    virtual void doNothing() = 0;
};

void BV::doNothing()
{
    Verbose g("BV::doNothing()");
}

void foo(BV& bv)
{
    bv.doNothing();
}

class B : public BV
{
public:
    B()
    {
         Verbose g("B::B");
         foo(*this);
    }
    virtual void doNothing()
    {
        Verbose g("B::doNothing()");
    }

private:
    Verbose a{"B::a"};
    Verbose b{"B::b"};
};

class B2 : public B
{
public:
    B2()
    {
         Verbose g("B2::B2");
         doNothing();
    }
    virtual void doNothing()
    {
        Verbose g("B2::doNothing()");
    }

private:
    Verbose c{"B2::c"};
};

int main()
{
    std::cout << "sizeof A: " << sizeof(A) << std::endl;
    std::cout << "sizeof B: " << sizeof(B) << std::endl;

    B2 b2;
}

