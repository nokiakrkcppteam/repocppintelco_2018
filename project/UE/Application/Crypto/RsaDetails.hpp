#pragma once

#include <openssl/bn.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/bio.h>
#include <openssl/x509.h>

#include <memory>
#include <vector>
#include <stdexcept>

#include "Messages/IncomingMessage.hpp"
#include "Messages/OutgoingMessage.hpp"

namespace ue
{

namespace RsaDetails
{

template <class Type,
          class CreateFuncType, CreateFuncType CreateFunc,
          class DelFuncType, DelFuncType DelFunc>
class MemPtrDef
{
public:
    using Ptr = std::unique_ptr<Type, DelFuncType>;
    using RawPtr = Type*;
    static Ptr create(Type* arg)
    {
        return Ptr(arg, DelFunc);
    }
    template <typename ...Arg>
    static Ptr create(Arg&& ...arg)
    {
        return create(CreateFunc(std::forward<Arg>(arg)...));
    }
};

#define DEFINE_MEMPTRDEF(type, createFun, freeFun) MemPtrDef<type, decltype(&createFun), createFun, decltype(&freeFun), freeFun>;

using Bignum = DEFINE_MEMPTRDEF(BIGNUM, ::BN_new, ::BN_free);
using Rsa = DEFINE_MEMPTRDEF(RSA, ::RSA_new, ::RSA_free);
using Bio = DEFINE_MEMPTRDEF(BIO, ::BIO_new, ::BIO_free);
using Bytes = std::vector<char>;


struct EncodeError : std::runtime_error
{
    using std::runtime_error::runtime_error;
};

class KeyPair
{
public:
    static constexpr int KEYBITLENGTH = 2048;
    KeyPair();
    KeyPair(Bytes publicKey);

    std::string encryptWithPublicKey(std::string text) const;
    std::string encryptWithPrivateKey(std::string text) const;
    std::string decryptWithPublicKey(std::string text) const;
    std::string decryptWithPrivateKey(std::string text) const;

    void dump(common::OutgoingMessage&builder) const;
    static KeyPair read(common::IncomingMessage& reader);
    void dump(std::ostream&) const;

private:
    Bytes publicKey;
    Bytes privateKey;

    Rsa::Ptr generateRsaKeyPair();
    Bytes extractPublicKey(Rsa::RawPtr rsaKeyPair);
    Bytes readKeyFromBio(Bio::RawPtr bio);
    Bytes extractPrivateKey(Rsa::RawPtr rsaKeyPair);
    Rsa::Ptr createRsaFromPublicKey() const;
    Rsa::Ptr createRsaFromPrivateKey() const;

    template <class EncodeMethod, class PaddingType>
    std::string encode(std::string text, Rsa::Ptr key, EncodeMethod, PaddingType, std::size_t extraPaddingSize, const std::string& name) const;

    template<class DecodeMethod, typename PaddingType>
    std::string decode(std::string text, Rsa::Ptr key, DecodeMethod, PaddingType, std::size_t extraPaddingSize, const std::string &name) const;
};

}

}
