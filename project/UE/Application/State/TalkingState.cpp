#include "TalkingState.hpp"

namespace ue
{

TalkingState::TalkingState(StateContext &stateContext,
                           common::PhoneNumber to,
                           IEncoderPtr talkEncoder, IDecoderPtr talkDecoder)
    : BaseState(stateContext, "[Talking to: " + to_string(to) + "] "),
      to(to),
      talkEncoder(talkEncoder),
      talkDecoder(talkDecoder)
{
    logger.logInfo("Talking to: ", to);
    timers.startTalkingTimer();
    user.showTalkingView(to);
}

void TalkingState::handleExitView()
{
    logger.logInfo("User drop call");
    timers.stopTalkingTimer();
    btsSender.sendCallDrop(to);
    setConnectedState();
}

void TalkingState::handleUserTalk(const std::string &text)
{
    logger.logInfo("My talk: \"", text, "\"");
    user.addUserTalk(to, text);
    btsSender.sendCallTalk(to, talkEncoder->encodeTalk(text));
    timers.startTalkingTimer(); // re-start
}

void TalkingState::handleCallTalkFrom(common::PhoneNumber from, const std::string &text)
{
    if (from == to)
    {
        std::string decodedText = talkDecoder->decodeTalk(text);
        logger.logDebug("Other talk: \"", decodedText, "\"");
        user.addPeerUserTalk(from, decodedText);
        timers.startTalkingTimer(); // re-start
    }
    else
    {
        logger.logError("Unknown call-talk from: ", from, " text: ", text);
    }
}

void TalkingState::handleCallDropFrom(common::PhoneNumber from)
{
    if (from == to)
    {
        logger.logDebug("Peer user drop call");
        timers.stopTalkingTimer();
        setConnectedState();
    }
    else
    {
        logger.logError("Unknown call-drop from: ", from);
    }
}

void TalkingState::handleTalkingTimeout()
{
    logger.logInfo("Talking drop by timeout");
    btsSender.sendCallDrop(to);
    setConnectedState();
}

}
