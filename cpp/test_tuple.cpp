#include <iostream>
#include <tuple>
#include <algorithm>

struct Person {
    std::string name;
    bool male;
    unsigned int age;
};
std::ostream& operator << (std::ostream& os, const Person& person) {
    return os << (person.male ? "male: " : "female: ") << person.name
              << " of age: " << (person.male ? person.age : (std::min(18u, person.age)));
}

using PersonT = std::tuple<std::string, bool, unsigned int>;
std::ostream& operator << (std::ostream& os, const PersonT& person) {
    return os << (std::get<1>(person) ? "male: " : "female: ") << std::get<0>(person)
              << " of age: " << (std::get<1>(person) ? std::get<2>(person) : (std::min(18u, std::get<2>(person))));
}

// TODO
PersonT convertToTuple(const Person&);
Person convertToStruct(const PersonT&);

int main() {
    Person john23{"John", true, 23 };
    PersonT mary31{"Mary", false, 31};

    std::cout << john23 << '\n';
    std::cout << mary31 << '\n';
    john23.male = false;
    std::get<1>(mary31) = true;
    std::cout << john23 << '\n';
    std::cout << mary31 << '\n';
}
