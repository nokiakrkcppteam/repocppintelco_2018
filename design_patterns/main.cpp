#include <iostream>
#include <memory>

class Engine
{
public:
    virtual ~Engine(){}
    virtual int calculateNewSpeed(int, int) = 0;
};

class LinearEngine : public Engine
{
public:
    int calculateNewSpeed(int currentSpeed, int) override
    {
        currentSpeed += 20;
        if (currentSpeed > 150)
            currentSpeed  =150;
        return currentSpeed;
    }
};

class NonLinearEngine : public Engine
{
public:
    int calculateNewSpeed(int currentSpeed, int i) override
    {
        currentSpeed += 20/i;
        return currentSpeed;
    }
};

class Brakes
{
public:
    virtual ~Brakes(){}
    virtual int calculateNewSpeed(int, int&) = 0;
};

class NonLinearBrakes: public Brakes
{
public:
    int calculateNewSpeed(int currentSpeed, int& i) override
    {
        if (i > 1)
            i--;
        currentSpeed -= 20/i;
        if (currentSpeed < 0 )
            currentSpeed = 0;
        return currentSpeed;
    }
};

class LinearBrakes : public Brakes
{
public:
    int calculateNewSpeed(int currentSpeed, int&) override
    {
        currentSpeed -= 50;
        if (currentSpeed < 0)
            currentSpeed = 0;
        return currentSpeed;
    }
};

class Car
{
public:
    Car(std::shared_ptr<Engine>& engine, std::shared_ptr<Brakes>& brakes)
        :engine(engine),
         brakes(brakes)
    {

    }
    int getCurrentSpeed()
    {
        return currentSpeed;
    }
    void accelerate()
    {
        currentSpeed = engine->calculateNewSpeed(currentSpeed, i);
        i++;
    }
    void brake()
    {
        currentSpeed = brakes->calculateNewSpeed(currentSpeed, i);
    }
private:
    std::shared_ptr<Engine> engine;
    std::shared_ptr<Brakes> brakes;
    int currentSpeed = 0;
    int i = 1;
};




class TestTrack
{
public:
    bool testFiat(Car& fiat)
    {
        bool result = true;
        result &= fiat.getCurrentSpeed() == 0;
        fiat.accelerate();
        result &= fiat.getCurrentSpeed() == 20;
        fiat.accelerate();
        fiat.accelerate();
        fiat.accelerate();
        fiat.accelerate();
        result &= fiat.getCurrentSpeed() == 100;
        fiat.accelerate();
        fiat.accelerate();
        result &= fiat.getCurrentSpeed() == 140;
        fiat.accelerate();
        result &= fiat.getCurrentSpeed() == 150;

        fiat.brake();
        result &= fiat.getCurrentSpeed() == 100;
        fiat.brake();
        fiat.brake();
        result &= fiat.getCurrentSpeed() == 0;
        fiat.brake();
        result &= fiat.getCurrentSpeed() == 0;
        return result;
    }

    bool testSkoda(Car& skoda)
    {
        bool result = true;
        result &= skoda.getCurrentSpeed() == 0;
        skoda.accelerate();
        result &= skoda.getCurrentSpeed() == 20;
        skoda.accelerate();
        result &= skoda.getCurrentSpeed() == 30;

        skoda.brake();
        result &= skoda.getCurrentSpeed() == 20;
        skoda.brake();
        skoda.brake();
        result &= skoda.getCurrentSpeed() == 0;

        skoda.accelerate();
        result &= skoda.getCurrentSpeed() == 20;


        return result;
    }

    bool testSeat(Car& car)
    {
        bool result = true;
        result &= car.getCurrentSpeed() == 0;
        car.accelerate();
        result &= car.getCurrentSpeed() == 20;
        car.accelerate();
        result &= car.getCurrentSpeed() == 40;

        car.brake();
        result &= car.getCurrentSpeed() == 30;
        car.brake();
        result &= car.getCurrentSpeed() == 10;
        car.brake();
        result &= car.getCurrentSpeed() == 0;

        car.accelerate();
        result &= car.getCurrentSpeed() == 20;


        return result;
    }
};

class CarFactory
{
public:
    Car createFiat()
    {
        return Car {linearEngine, linearBrakes};
    }

    Car createSkoda()
    {
        return Car {nonLinearEngine, nonLinearBrakes};
    }

    Car createSeat()
    {
        return Car {linearEngine, nonLinearBrakes};
    }
private:
    std::shared_ptr<Engine> linearEngine = std::make_shared<LinearEngine>();
    std::shared_ptr<Engine> nonLinearEngine = std::make_shared<NonLinearEngine>();
    std::shared_ptr<Brakes> linearBrakes = std::make_shared<LinearBrakes>();
    std::shared_ptr<Brakes> nonLinearBrakes = std::make_shared<NonLinearBrakes>();
};
int main()
{
    CarFactory carFactory;
    Car fiat = carFactory.createFiat();
    Car skoda = carFactory.createSkoda();
    Car seat = carFactory.createSeat();

    TestTrack testTrack;
    if (testTrack.testFiat(fiat))
        std::cout<< "Test succesful" << std::endl;
    else
        std::cout<< "Test failed" << std::endl;

    if (testTrack.testSkoda(skoda))
        std::cout<< "Test succesful" << std::endl;
    else
        std::cout<< "Test failed" << std::endl;

    if (testTrack.testSeat(seat))
        std::cout<< "Test succesful" << std::endl;
    else
        std::cout<< "Test failed" << std::endl;

    return 0;
}








