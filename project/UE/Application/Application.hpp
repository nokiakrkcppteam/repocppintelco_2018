#pragma once

#include "State/StateContext.hpp"
#include "IEventsReceiver.hpp"
#include "Logger/PrefixedLogger.hpp"
#include <mutex>

namespace ue
{

using common::PhoneNumber;
using common::ILogger;

class Application : public IEventsReceiver
{
public:
    Application(PhoneNumber phoneNumber,
                ISmsDB& smsDB,
                IBtsSender& btsSender,
                IUser& user,
                ITimers &timers,
                IEncryptionFactory &encryptionFactory,
                ILogger& iLogger);
    ~Application();

    void handleSib() override;
    void handleAttachAccept() override;
    void handleAttachReject() override;
    void handleSmsReceived(common::PhoneNumber from, IDecoderPtr smsDecoder, const std::string& text) override;
    void handleCallRequestFrom(common::PhoneNumber from, IEncoderPtr callEncoder) override;
    void handleCallAcceptFrom(common::PhoneNumber from, IEncoderPtr callEncoder) override;
    void handleCallTalkFrom(common::PhoneNumber from, const std::string& text) override;
    void handleCallDropFrom(common::PhoneNumber from) override;

    void handleExitView() override;
    void handleSendSms(common::PhoneNumber to, const std::string &text) override;
    void handleEditSms(IEncoderPtr smsEncoder) override;
    void handleViewSms(ISmsDB::Index smsIndex) override;
    void handleViewSmses() override;
    void handleUserAccept() override;
    void handleUserTalk(const std::string& text) override;
    void handleDialing(IDecoderPtr talkDecoder) override;
    void handleUserCalling(common::PhoneNumber to) override;

    void handleAttachTimeout() override;
    void handlePeerCallingTimeout() override;
    void handleUserCallingTimeout() override;
    void handleTalkingTimeout() override;

private:
    IEventsReceiver &getState();

    common::PrefixedLogger logger;
    StateFactory stateFactory;
    StateLocator stateLocator;
    StateContext stateContext;

    std::mutex handlersGuard;
    using HandlersGuardLock = std::lock_guard<std::mutex>;
};

}
