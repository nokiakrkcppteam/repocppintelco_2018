#pragma once

#include "IEncryptionFactory.hpp"
#include "IApplicationEnvironment.hpp"
#include "Logger/PrefixedLogger.hpp"

namespace ue
{

class NoneEncryption;
class RsaEncryptionFactory;

class EncryptionFactory : public IEncryptionFactory
{
public:
    EncryptionFactory(IApplicationEnvironment& environment, ILogger& baseLogger);
    ~EncryptionFactory() override;

    IEncoderPtr getEncoderFor(EncryptionKind kind) override;
    IDecoderPtr readDecoder(common::IncomingMessage &reader) override;
    IEncoderPtr readEncoder(common::IncomingMessage &reader) override;
    IDecoderPtr getDecoderFor(EncryptionKind kind) override;

private:
    common::PrefixedLogger logger;
    IApplicationEnvironment& environment;
    std::shared_ptr<NoneEncryption> noneEncryption;
    static RsaEncryptionFactory rsaEncryptionFactory;
};

}
