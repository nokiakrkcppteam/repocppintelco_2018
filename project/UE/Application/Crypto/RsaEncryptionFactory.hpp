#pragma once

#include "IEncryption.hpp"
#include "RsaDetails.hpp"

namespace common
{
class IncomingMessage;
}

namespace ue
{

class RsaEncryptionFactory
{
public:
    RsaEncryptionFactory();
    ~RsaEncryptionFactory();

    IEncoderPtr getEncoder();
    IDecoderPtr getDecoder();
    IDecoderPtr readDecoder(common::IncomingMessage &reader);
    IEncoderPtr readEncoder(common::IncomingMessage &reader);

private:
    // follow https://wiki.openssl.org/index.php/Libcrypto_API to initialize
    class ErrorStringsGuard
    {
    public:
        ErrorStringsGuard();
        ~ErrorStringsGuard();
    };
    class LibInitGuard
    {
    public:
        LibInitGuard();
        ~LibInitGuard();
    };

    ErrorStringsGuard errorStrings;
    LibInitGuard libInitializer;

    RsaDetails::KeyPair myKey;
};

}
