#pragma once

#include <vector>

class AA_mutate
{
public:
  auto begin() const { return aa.begin(); }
  auto end() const { return aa.end(); }

  void push_back(int a) const { aa.push_back(a); }

private:
  mutable std::vector<int> aa;
};

