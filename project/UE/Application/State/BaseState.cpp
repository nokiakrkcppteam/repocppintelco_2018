#include "BaseState.hpp"

namespace ue
{

BaseState::BaseState(StateContext& stateContext, const std::string& stateName)
    : StateContext(stateContext),
      logger(StateContext::logger, stateName)
{}

void BaseState::handleSib()
{
    logger.logError("Unexpected SIB");
}

void BaseState::handleAttachAccept()
{
    logger.logError("Unexpected Attach Accept");
}

void BaseState::handleAttachReject()
{
    logger.logError("Unexpected Attach Reject");
}

void BaseState::handleSmsReceived(common::PhoneNumber from, IDecoderPtr smsDecoder, const std::string &text)
{
    auto index = smsDB.addReceivedSms(from, smsDecoder->decodeSms(text));
    logger.logDebug("Handling SMS Received: ", smsDB.getSmsShortRepresentation(index));
}

void BaseState::handleCallRequestFrom(common::PhoneNumber from, IEncoderPtr callEncoder)
{
    logger.logError("Unexpected Call Request from: ", from, ", enc: ", *callEncoder);
}

void BaseState::handleCallAcceptFrom(common::PhoneNumber from, IEncoderPtr callEncoder)
{
    logger.logError("Unexpected Call Accept from: ", from, ", enc: ", *callEncoder);
}

void BaseState::handleCallTalkFrom(common::PhoneNumber from, const std::string &text)
{
    logger.logError("Unexpected Call Talk from: ", from, ", text: ", text);
}

void BaseState::handleCallDropFrom(common::PhoneNumber from)
{
    logger.logError("Unexpected Call Drop from: ", from);
}

void BaseState::handleExitView()
{
    logger.logDebug("Unexpected Exit View");
}

void BaseState::handleSendSms(common::PhoneNumber to, const std::string &text)
{
    logger.logError("Unexpected Sms Received to: ", to);
}

void BaseState::handleEditSms(IEncoderPtr smsEncoder)
{
    logger.logError("Unexpected Edit Sms");
}

void BaseState::handleViewSms(ISmsDB::Index smsIndex)
{
    logger.logError("Unexpected Sms View - nr: ", smsIndex);
}

void BaseState::handleViewSmses()
{
    logger.logError("Unexpected Smses View");
}

void BaseState::handleUserAccept()
{
    logger.logError("Unexpected User Accept call");
}

void BaseState::handleUserTalk(const std::string &text)
{
    logger.logError("Unexpected User Talk: ", text);
}

void BaseState::handleDialing(IDecoderPtr talkDecoder)
{
    logger.logError("Unexpected User Dialing: ", *talkDecoder);
}

void BaseState::handleUserCalling(common::PhoneNumber to)
{
    logger.logError("Unexpected User Calling to: ", to);
}

void BaseState::handleAttachTimeout()
{
    logger.logError("Unexpected Attach Timeout");
}

void BaseState::handlePeerCallingTimeout()
{
    logger.logError("Unexpected Peer Calling Timeout");
}

void BaseState::handleUserCallingTimeout()
{
    logger.logError("Unexpected User Calling Timeout");
}

void BaseState::handleTalkingTimeout()
{
    logger.logError("Unexpected Talking Timeout");
}

}
