#include <iostream>
#include "AA.hpp"
#include <memory>

void printAll(AA& aa)
{
    aa.push_back(3);
    for (int a : aa)
    {
        std::cout << a << '-';
    }
    std::cout << '\n';
}

std::unique_ptr<AA> printAll(std::unique_ptr<AA> aa) 
{
    aa->push_back(3);
    for (int a : *aa)
    {
        std::cout << a << '-';
    }
    std::cout << '\n';
    return aa;
}


int main() 
{
    std::unique_ptr<AA> aa = std::make_unique<AA>();
    aa->push_back(4);
    printAll(*aa); // access to pointerd object with lvalue-ref
    aa->push_back(2);
    aa = printAll(std::move(aa));
    aa->push_back(6);
    printAll(std::move(aa));
}
