#include "RsaDetails.hpp"

#include <cassert>
#include <ctime>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/rand.h>

#include "Messages/OutgoingMessage.hpp"

#define doOpenSslAssert(operation) \
    if (false) {} else { \
        int res = operation; \
        assert((#operation, res) == 1); \
    }

namespace ue
{

namespace RsaDetails
{

KeyPair::KeyPair()
{
    Rsa::Ptr rsaKeyPair = generateRsaKeyPair();

    privateKey = extractPrivateKey(rsaKeyPair.get());
    publicKey = extractPublicKey(rsaKeyPair.get());
}

KeyPair::KeyPair(Bytes publicKey)
   : publicKey(publicKey)
{}

template<class EncodeMethod, typename PaddingType>
std::string KeyPair::encode(std::string text,
                            Rsa::Ptr key,
                            EncodeMethod encodeMethod,
                            PaddingType padding,
                            std::size_t extraPaddingSize,
                            const std::string& name) const
{
    const std::size_t textSize = text.size() + 1;
    const std::size_t chunkSize = RSA_size(key.get());
    const std::size_t actualChunkSize = chunkSize - extraPaddingSize;

    const std::size_t chunkCount = (textSize + actualChunkSize - 1) / actualChunkSize; // ceil(A / B) == (A + B - 1) / B
    std::string encodedBytes(chunkSize * chunkCount, char{});

    const unsigned char* textBegin = (const unsigned char*)text.data();
    const unsigned char* textEnd = textBegin + textSize;
    unsigned char* encodedBegin = (unsigned char*)encodedBytes.data();

    for (std::size_t chunk = 0; chunk < chunkCount; ++chunk)
    {
        const bool lastChunk = (chunk + 1 == chunkCount);
        std::size_t textLen = lastChunk ? (textEnd - textBegin) : actualChunkSize;
        int encryptedLength = encodeMethod(textBegin, textLen, encodedBegin, key.get(), padding);
        if (encryptedLength < 0)
        {
            std::unique_ptr<char[]> err = std::make_unique<char[]>(130);
            ERR_error_string(ERR_get_error(), err.get());
            throw EncodeError("@Cannot " + name + ":" + err.get() + "@" + text.substr(0, 10) + "...@");
        }
        textBegin  += textLen;
        encodedBegin += chunkSize;
    }

    return encodedBytes;
}

template<class DecodeMethod, typename PaddingType>
std::string KeyPair::decode(std::string text,
                            Rsa::Ptr key,
                            DecodeMethod decodeMethod,
                            PaddingType padding,
                            std::size_t /* extraPaddingSize */,
                            const std::string& name) const
{
    std::string decodedChunk(RSA_size(key.get()), char{});

    unsigned char* decodedChunkBegin = (unsigned char*)decodedChunk.data();
    const unsigned char* textBegin = (const unsigned char*)text.data();
    const unsigned char* textEnd = textBegin + text.size();

    std::string decodedText;

    for (; textBegin < textEnd; textBegin += decodedChunk.size())
    {
        const bool lastChunk = (textBegin + decodedChunk.size()) >= textEnd;
        int decryptedLength = decodeMethod(textBegin, decodedChunk.size(), decodedChunkBegin, key.get(), padding);
        if (decryptedLength < 1)
        {
            std::unique_ptr<char[]> err = std::make_unique<char[]>(130);
            ERR_error_string(ERR_get_error(), err.get());
            throw EncodeError("@Cannot " + name + ":" + err.get() + "@" + text.substr(0, 10) + "...@");
        }
        decodedText += decodedChunk.substr(0, decryptedLength - (lastChunk ? 1 : 0));
    }
    return decodedText;
}


std::string KeyPair::encryptWithPublicKey(std::string text) const
{
    return encode(std::move(text),
                  createRsaFromPublicKey(),
                  [](const unsigned char* input, std::size_t inputSize, unsigned char* output, Rsa::RawPtr key, auto padding)
                  {
                      return RSA_public_encrypt(inputSize, input, output, key, padding);
                  }, RSA_PKCS1_PADDING, RSA_PKCS1_PADDING_SIZE, "rsa-pub-enc");
}

std::string KeyPair::encryptWithPrivateKey(std::string text) const
{
    return encode(std::move(text),
                  createRsaFromPrivateKey(),
                  [](const unsigned char* input, std::size_t inputSize, unsigned char* output, Rsa::RawPtr key, auto padding)
                  {
                      return RSA_private_encrypt(inputSize, input, output, key, padding);
                  }, RSA_PKCS1_PADDING, RSA_PKCS1_PADDING_SIZE, "rsa-priv-enc");
}

std::string KeyPair::decryptWithPublicKey(std::string text) const
{
    return decode(std::move(text),
                  createRsaFromPublicKey(),
                  [](const unsigned char* input, std::size_t inputSize, unsigned char* output, Rsa::RawPtr key, auto padding)
                  {
                      return RSA_public_decrypt(inputSize, input, output, key, padding);
                  }, RSA_PKCS1_PADDING, RSA_PKCS1_PADDING_SIZE, "rsa-pub-dec");
}

std::string KeyPair::decryptWithPrivateKey(std::string text) const
{
    return decode(std::move(text),
                  createRsaFromPrivateKey(),
                  [](const unsigned char* input, std::size_t inputSize, unsigned char* output, Rsa::RawPtr key, auto padding)
                  {
                      return RSA_private_decrypt(inputSize, input, output, key, padding);
                  }, RSA_PKCS1_PADDING, RSA_PKCS1_PADDING_SIZE, "rsa-priv-dec");
}

void KeyPair::dump(common::OutgoingMessage &builder) const
{
    const std::uint16_t pubKeySize = publicKey.size();
    builder.writeNumber(pubKeySize);
    builder.writeText(std::string(publicKey.begin(), publicKey.end()));
}

KeyPair KeyPair::read(common::IncomingMessage &reader)
{
    auto size = reader.readNumber<std::uint16_t>();
    auto keyAsText = reader.readText(size);
    Bytes publicKey(keyAsText.begin(), keyAsText.end());
    return KeyPair(publicKey);
}

void KeyPair::dump(std::ostream &os) const
{
    os << "pubkey: " << publicKey.size() << ", privkey: " << privateKey.size();
}

Rsa::Ptr KeyPair::generateRsaKeyPair()
{
    Rsa::Ptr rsa = Rsa::create();
    assert(rsa.get());
    Bignum::Ptr bignum = Bignum::create();
    assert(bignum.get());

    {
        std::time_t random_buffer = std::time(nullptr);
        RAND_seed(&random_buffer, sizeof(random_buffer));
    }
    doOpenSslAssert(BN_set_word(bignum.get(), RSA_F4));
    doOpenSslAssert(RSA_generate_key_ex(rsa.get(), KEYBITLENGTH, bignum.get(), nullptr));

    return rsa;
}

Bytes KeyPair::readKeyFromBio(Bio::RawPtr bio)
{
    Bytes key(BIO_pending(bio));
    BIO_read(bio, key.data(), key.size());
    return key;
}

Bytes KeyPair::extractPublicKey(Rsa::RawPtr rsaKeyPair)
{
    Bio::Ptr bio = Bio::create(BIO_s_mem());
    assert(bio.get());
    doOpenSslAssert(PEM_write_bio_RSAPublicKey(bio.get(), rsaKeyPair));

    return readKeyFromBio(bio.get());
}

Bytes KeyPair::extractPrivateKey(Rsa::RawPtr rsaKeyPair)
{
    Bio::Ptr bio = Bio::create(BIO_s_mem());
    assert(bio.get());
    doOpenSslAssert(PEM_write_bio_RSAPrivateKey(bio.get(), rsaKeyPair, nullptr, nullptr, 0, nullptr, nullptr));

    return readKeyFromBio(bio.get());
}

Rsa::Ptr KeyPair::createRsaFromPublicKey() const
{
    Bio::Ptr bio = Bio::create(BIO_new_mem_buf(publicKey.data(), publicKey.size()));
    assert(bio.get());

    Rsa::Ptr rsa = Rsa::create(PEM_read_bio_RSAPublicKey(bio.get(), nullptr, nullptr, nullptr));
    assert(rsa.get());
    return rsa;
}

Rsa::Ptr KeyPair::createRsaFromPrivateKey() const
{
    Bio::Ptr bio = Bio::create(BIO_new_mem_buf(privateKey.data(), privateKey.size()));
    assert(bio.get());


    Rsa::Ptr rsa = Rsa::create(PEM_read_bio_RSAPrivateKey(bio.get(), nullptr, nullptr, nullptr));
    assert(rsa.get());
    return rsa;
}

}

}
