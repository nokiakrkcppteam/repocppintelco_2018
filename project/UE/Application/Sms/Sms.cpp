#include "Sms.hpp"
#include <sstream>
#include <ctime>
#include <iomanip>

namespace ue
{

Sms::Sms(common::PhoneNumber from, common::PhoneNumber to, const std::string &text)
    : from(from),
      to(to),
      text(text),
      when(std::chrono::system_clock::now())
{}

std::string Sms::toFullString(common::PhoneNumber host) const
{
    std::ostringstream sstream;

    toPhoneNumbers(sstream, host);
    auto whenCTime = std::chrono::system_clock::to_time_t(when);
    sstream << "At: " << std::put_time(std::localtime(&whenCTime), "%c %Z")
            << "\n-------------------------------\n"
            << text;

    return sstream.str();
}

std::string Sms::toShortString(common::PhoneNumber host) const
{
    std::ostringstream sstream;

    toPhoneNumbers(sstream, host);
    toShortWhen(sstream);
    toShortText(sstream);

    return sstream.str();

}

void Sms::toPhoneNumbers(std::ostream &sstream, common::PhoneNumber host) const
{
    if (from == host)
    {
        sstream << "To: " << to << " ";
    }
    if (to == host)
    {
        sstream << "From: " << from << " ";
    }
}

void Sms::toShortWhen(std::ostream& sstream) const
{
    auto whenCTime = std::chrono::system_clock::to_time_t(when);
    auto now = std::chrono::system_clock::now();
    auto distance = now - when;
    using namespace std::chrono_literals;
    if (distance < 24h)
    {
        sstream << '(' << std::put_time(std::localtime(&whenCTime), "%H:%M:%S") << ')';
    }
    else
    {
        sstream << '(' << std::put_time(std::localtime(&whenCTime), "%c %Z") << ')';
    }
}

void Sms::toShortText(std::ostream &sstream) const
{
    constexpr std::string::size_type MAX_SUBSTR_SIZE = 20;
    auto endToPrint = text.find_first_of('\n');

    if (endToPrint == std::string::npos)
    {
        endToPrint = std::min(text.length(), MAX_SUBSTR_SIZE);
    }
    else if (endToPrint > MAX_SUBSTR_SIZE)
    {
        endToPrint = MAX_SUBSTR_SIZE;
    }

    sstream << ' ';
    sstream.write(text.data(), endToPrint);
}

}
