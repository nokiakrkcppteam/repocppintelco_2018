#pragma once

#include "Messages.hpp"
#include <string>

#include <functional>

namespace ue
{

class ISmsDB
{
public:
    virtual ~ISmsDB() = default;

    using Index = std::size_t;

    virtual Index addSentSms(common::PhoneNumber to, const std::string& text) = 0;
    virtual Index addReceivedSms(common::PhoneNumber from, const std::string& text) = 0;

    using ContinueVisiting = bool;
    using Visitor = std::function<ContinueVisiting(Index index, const std::string& shortSmsRepresentation)>;

    virtual void visitAll(Visitor) const = 0;

    virtual std::string getSmsShortRepresentation(Index index) const = 0;
    virtual std::string getSmsFullRepresentation(Index index) const = 0;
};

}
