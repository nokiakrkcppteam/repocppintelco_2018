#pragma once

#include <gmock/gmock.h>
#include "Bts/IBtsSender.hpp"

namespace ue
{

struct IBtsSenderMock : public IBtsSender
{
    IBtsSenderMock();
    ~IBtsSenderMock() override;

    MOCK_METHOD0(sendAttachRequest, void());
    MOCK_METHOD3(sendSms, void(common::PhoneNumber to, IEncoderPtr smsEncoder, const std::string& smsText));
    MOCK_METHOD2(sendCallRequest, void(common::PhoneNumber to, IDecoderPtr talkDecoder));
    MOCK_METHOD1(sendCallDrop, void(common::PhoneNumber to));
    MOCK_METHOD2(sendCallAccept, void(common::PhoneNumber to, IDecoderPtr talkDecoder));
    MOCK_METHOD2(sendCallTalk, void(common::PhoneNumber to, const std::string& callTalk));
};

}
