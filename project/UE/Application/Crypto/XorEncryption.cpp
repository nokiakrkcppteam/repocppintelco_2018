#include "XorEncryption.hpp"
#include "Messages/IncomingMessage.hpp"
#include "Messages/OutgoingMessage.hpp"
#include "EncryptionKind.hpp"
#include <iomanip>

namespace ue
{

XorEncryption::XorEncryption(XorEncryption::KeyType key)
    : key(key)
{}

XorEncryption::XorEncryption(common::IncomingMessage &reader)
    : XorEncryption(reader.readNumber<decltype(key)>())
{}

EncryptionKind XorEncryption::getKind() const
{
    return EncryptionKind::Xor;
}

std::string XorEncryption::xorText(std::string text) const
{
    for (char& c : text)
    {
        c = static_cast<char>(c ^ key);
    }
    return text;
}

std::string XorEncryption::encodeSms(const std::string &text) const
{
    return xorText(text);
}

std::string XorEncryption::decodeSms(const std::string &text) const
{
    return xorText(text);
}

std::string XorEncryption::encodeTalk(const std::string &text) const
{
    return xorText(text);
}

std::string XorEncryption::decodeTalk(const std::string &text) const
{
    return xorText(text);
}

void XorEncryption::writeTo(common::OutgoingMessage &builder) const
{
    builder.writeNumber(static_cast<std::uint8_t>(EncryptionKind::Xor));
    builder.writeNumber(key);
}

void XorEncryption::printData(std::ostream &os) const
{
    auto flags = os.flags();
    os << "XorEncryption(0x"
       << std::hex << std::setw(2) << std::setfill('0')
       << static_cast<unsigned>(key)
       << ")";
    os.flags(flags);
}

}
