#pragma once

#include "State/BaseState.hpp"

namespace ue
{

class NotConnectedState : public BaseState
{
public:
    NotConnectedState(StateContext& stateContext);

    void handleSib() override;
};

}
