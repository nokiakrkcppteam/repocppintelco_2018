#include "CaesarEncryption.hpp"
#include "Messages/IncomingMessage.hpp"
#include "Messages/OutgoingMessage.hpp"
#include "EncryptionKind.hpp"
#include <iomanip>

namespace ue
{

CaesarEncryption::CaesarEncryption(KeyType key)
    : key(key)
{}

CaesarEncryption::CaesarEncryption(common::IncomingMessage &reader)
    : CaesarEncryption(reader.readNumber<decltype(key)>())
{}

EncryptionKind CaesarEncryption::getKind() const
{
    return EncryptionKind::Caesar;
}

std::string CaesarEncryption::encodeText(std::string text, KeyType key) const
{
    for (char& c : text)
    {
        c = static_cast<char>(c + key);
    }
    return text;
}

std::string CaesarEncryption::encodeSms(const std::string &text) const
{
    return encodeText(text, key);
}

std::string CaesarEncryption::decodeSms(const std::string &text) const
{
    return encodeText(text, -key);
}

std::string CaesarEncryption::encodeTalk(const std::string &text) const
{
    return encodeText(text, key);
}

std::string CaesarEncryption::decodeTalk(const std::string &text) const
{
    return encodeText(text, -key);
}

void CaesarEncryption::writeTo(common::OutgoingMessage &builder) const
{
    builder.writeNumber(static_cast<std::uint8_t>(EncryptionKind::Caesar));
    builder.writeNumber(key);
}

void CaesarEncryption::printData(std::ostream &os) const
{
    auto flags = os.flags();
    os << "CaesarEncryption("
       << std::dec << static_cast<unsigned>(key)
       << ")";
    os.flags(flags);
}

}
