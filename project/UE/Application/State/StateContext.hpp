#pragma once

#include "Logger/ILogger.hpp"
#include "Bts/IBtsSender.hpp"
#include "Sms/ISmsDB.hpp"
#include "User/IUser.hpp"
#include "Timer/ITimers.hpp"
#include "Crypto/IEncryptionFactory.hpp"
#include "Messages/PhoneNumber.hpp"
#include "StateFactory.hpp"
#include "StateLocator.hpp"

namespace ue
{

using common::PhoneNumber;
using common::ILogger;

struct StateContext
{
    const PhoneNumber phoneNumber;
    ISmsDB& smsDB;
    IBtsSender& btsSender;
    IUser& user;
    ITimers& timers;
    IEncryptionFactory& encryptionFactory;
    common::ILogger& logger;
    StateFactory& stateFactory;
    StateLocator& stateLocator;

    void setNotConnectedState();
    void setConnectingState();
    void setConnectedState();
    void setEditSmsState(IEncoderPtr smsEncoder);
    void setViewSmsState(ISmsDB::Index smsIndex);
    void setViewSmsesState();
    void setPeerCallingState(common::PhoneNumber from, IEncoderPtr talkEncoder);
    void setTalkingState(common::PhoneNumber to, IEncoderPtr talkEncoder, IDecoderPtr talkDecoder);
    void setDialingState(IDecoderPtr talkDecoder);
    void setUserCallingState(common::PhoneNumber to, IDecoderPtr talkDecoder);
};

}
