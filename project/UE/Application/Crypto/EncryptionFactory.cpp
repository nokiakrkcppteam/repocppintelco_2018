#include "EncryptionFactory.hpp"
#include "NoneEncryption.hpp"
#include "XorEncryption.hpp"
#include "CaesarEncryption.hpp"
#include "RsaEncryptionFactory.hpp"
#include "Messages/IncomingMessage.hpp"
#include <type_traits>

namespace ue
{

RsaEncryptionFactory EncryptionFactory::rsaEncryptionFactory;

EncryptionFactory::EncryptionFactory(IApplicationEnvironment &environment, ILogger &baseLogger)
  : logger(baseLogger, "[CYPHER] "),
    environment(environment),
    noneEncryption(std::make_shared<NoneEncryption>())
{}

EncryptionFactory::~EncryptionFactory() = default;

IEncoderPtr EncryptionFactory::getEncoderFor(EncryptionKind kind)
{
    switch (kind)
    {
    case EncryptionKind::None:
    {
        logger.logDebug("get encoder for none-encryption");
        return noneEncryption;
    }

    case EncryptionKind::Xor:
    {
        auto xorValue = environment.getProperty("enc_xor", 0b11001001);
        logger.logDebug("create encoder for xor-encryption: ", xorValue);
        return std::make_shared<XorEncryption>(xorValue);
    }

    case EncryptionKind::Caesar:
    {
        auto caesarValue = environment.getProperty("enc_caesar", 27);
        logger.logDebug("create encoder for caeser-encryption: ", caesarValue);
        return std::make_shared<CaesarEncryption>(caesarValue);
    }

    case EncryptionKind::Rsa:
        logger.logDebug("create encoder for rsa-encryption");
        return rsaEncryptionFactory.getEncoder();

    default:
        logger.logError("Cannot create encoder for unsupported encryption: ", kind);
        return nullptr;
    }
}

IDecoderPtr EncryptionFactory::readDecoder(common::IncomingMessage &reader)
{
    EncryptionKind kind = static_cast<EncryptionKind>(reader.readNumber<std::underlying_type_t<EncryptionKind>>());
    switch (kind)
    {
    case EncryptionKind::None:
        logger.logDebug("read none decoder");
        return noneEncryption;

    case EncryptionKind::Xor:
        logger.logDebug("read xor decoder");
        return std::make_shared<XorEncryption>(reader);

    case EncryptionKind::Caesar:
        logger.logDebug("read caesar decoder");
        return std::make_shared<CaesarEncryption>(reader);

    case EncryptionKind::Rsa:
        logger.logDebug("read rsa decoder");
        return rsaEncryptionFactory.readDecoder(reader);

    default:
        logger.logError("Cannot read decoder for unsupported kind: ", kind);
        return nullptr;
    }
}

IEncoderPtr EncryptionFactory::readEncoder(common::IncomingMessage &reader)
{
    EncryptionKind kind = static_cast<EncryptionKind>(reader.readNumber<std::underlying_type_t<EncryptionKind>>());
    switch (kind)
    {
    case EncryptionKind::None:
        logger.logDebug("read none encoder");
        return noneEncryption;

    case EncryptionKind::Xor:
        logger.logDebug("read xor encoder");
        return std::make_shared<XorEncryption>(reader);

    case EncryptionKind::Caesar:
        logger.logDebug("read caesar encoder");
        return std::make_shared<CaesarEncryption>(reader);

    case EncryptionKind::Rsa:
        logger.logDebug("read rsa encoder");
        return rsaEncryptionFactory.readEncoder(reader);

    default:
        logger.logError("Cannot read encoder for unsupported kind: ", kind);
        return nullptr;
    }
}

IDecoderPtr EncryptionFactory::getDecoderFor(EncryptionKind kind)
{
    switch (kind)
    {
    case EncryptionKind::None:
    {
        logger.logDebug("get decoder for none-encryption");
        return noneEncryption;
    }

    case EncryptionKind::Xor:
    {
        auto xorValue = environment.getProperty("dec_xor", 0b10011100);
        logger.logDebug("create decoder for xor-encryption: ", xorValue);
        return std::make_shared<XorEncryption>(xorValue);
    }

    case EncryptionKind::Caesar:
    {
        auto caesarValue = environment.getProperty("dec_caesar", 13);
        logger.logDebug("create decoder for caeser-encryption: ", caesarValue);
        return std::make_shared<CaesarEncryption>(caesarValue);
    }

    case EncryptionKind::Rsa:
        logger.logDebug("create decoder for rsa-encryption");
        return rsaEncryptionFactory.getDecoder();

    default:
        logger.logError("Cannot create decoder for unsupported encryption: ", kind);
        return nullptr;
    }
}

}
