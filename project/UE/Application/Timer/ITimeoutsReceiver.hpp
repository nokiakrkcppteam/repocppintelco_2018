#pragma once

#include "Messages.hpp"

namespace ue
{

class ITimeoutsReceiver
{
public:
    virtual ~ITimeoutsReceiver() = default;

    virtual void handleAttachTimeout() = 0;
    virtual void handlePeerCallingTimeout() = 0;
    virtual void handleUserCallingTimeout() = 0;
    virtual void handleTalkingTimeout() = 0;
};

}
