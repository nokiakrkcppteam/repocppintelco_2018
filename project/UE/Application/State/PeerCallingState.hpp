#pragma once

#include "BaseState.hpp"

namespace ue
{

class PeerCallingState : public BaseState
{
public:
    PeerCallingState(StateContext& stateContext,
                     common::PhoneNumber from,
                     IEncoderPtr talkEncoder);
    void handleUserAccept() override;
    void handleExitView() override;
    void handleCallDropFrom(common::PhoneNumber from) override;
    void handlePeerCallingTimeout() override;

private:
    common::PhoneNumber from;
    IEncoderPtr talkEncoder;
};

}
