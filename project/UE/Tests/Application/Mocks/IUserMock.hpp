#pragma once

#include <gmock/gmock.h>
#include "User/IUser.hpp"

namespace ue
{

struct IUserMock : public IUser
{
    IUserMock();
    ~IUserMock() override;

    MOCK_METHOD0(showNotConnected, void());
    MOCK_METHOD0(showConnecting, void());
    MOCK_METHOD0(showConnected, void());
    MOCK_METHOD0(showSmses, void());
    MOCK_METHOD1(showSms, void(const std::string& text));
    MOCK_METHOD0(editSms, void());
    MOCK_METHOD1(showPeerCalling, void(common::PhoneNumber));
    MOCK_METHOD1(showTalkingView, void(common::PhoneNumber to));
    MOCK_METHOD2(addPeerUserTalk, void(common::PhoneNumber from, const std::string& text));
    MOCK_METHOD2(addUserTalk, void(common::PhoneNumber to, const std::string& text));
    MOCK_METHOD0(showDialing, void());
    MOCK_METHOD1(showUserCalling, void(common::PhoneNumber to));
};

}
