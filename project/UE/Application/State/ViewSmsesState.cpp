#include "ViewSmsesState.hpp"

namespace ue
{

ViewSmsesState::ViewSmsesState(StateContext& stateContext)
    : BaseState(stateContext, "[VIEW-SMSES] ")
{
    user.showSmses();
}

void ViewSmsesState::handleViewSms(ISmsDB::Index smsIndex)
{
    logger.logInfo("View Sms at: ", smsIndex);
    setViewSmsState(smsIndex);
}

void ViewSmsesState::handleExitView()
{
    logger.logInfo("Back to connected state");
    setConnectedState();
}

}
