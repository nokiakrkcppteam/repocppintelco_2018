#pragma once

#include <gmock/gmock.h>
#include "Crypto/IEncryption.hpp"
#include "Crypto/EncryptionKind.hpp"
#include "Messages/IncomingMessage.hpp"
#include "Messages/OutgoingMessage.hpp"

namespace ue
{

struct IEncoderMock : IEncoder
{
    IEncoderMock();
    ~IEncoderMock() override;

    MOCK_CONST_METHOD1(encodeSms, std::string(const std::string&));
    MOCK_CONST_METHOD1(encodeTalk, std::string(const std::string&));
    MOCK_CONST_METHOD1(writeTo, void(common::OutgoingMessage &));
    MOCK_CONST_METHOD1(printData, void(std::ostream&));
    MOCK_CONST_METHOD0(getKind, EncryptionKind());
};

struct IDecoderMock : IDecoder
{
    IDecoderMock();
    ~IDecoderMock() override;

    MOCK_CONST_METHOD1(decodeSms, std::string(const std::string&));
    MOCK_CONST_METHOD1(decodeTalk, std::string(const std::string&));
    MOCK_CONST_METHOD1(writeTo, void(common::OutgoingMessage &));
    MOCK_CONST_METHOD1(printData, void(std::ostream&));
};

}
