#include "NotConnectedState.hpp"

namespace ue
{

NotConnectedState::NotConnectedState(StateContext& stateContext)
    : BaseState(stateContext, "[NotConnected] ")
{
    user.showNotConnected();
}

void NotConnectedState::handleSib()
{
    logger.logInfo("Handling SIB");
    btsSender.sendAttachRequest();
    setConnectingState();
}

}
