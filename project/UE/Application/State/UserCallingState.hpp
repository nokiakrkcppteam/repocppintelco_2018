#pragma once

#include "BaseState.hpp"

namespace ue
{

class UserCallingState : public BaseState
{
public:
    UserCallingState(StateContext& stateContext, common::PhoneNumber to, IDecoderPtr talkDecoder);

    void handleCallAcceptFrom(common::PhoneNumber from, IEncoderPtr talkEncoder) override;
    void handleCallDropFrom(common::PhoneNumber from) override;
    void handleExitView() override;
    void handleUserAccept() override;
    void handleUserCallingTimeout() override;

private:
    common::PhoneNumber to;
    IDecoderPtr talkDecoder;
};

}
