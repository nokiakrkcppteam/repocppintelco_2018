#include "Application.hpp"

namespace ue
{

Application::Application(common::PhoneNumber phoneNumber,
                         ISmsDB &smsDB,
                         IBtsSender &btsSender,
                         IUser &user,
                         ITimers &timers,
                         IEncryptionFactory& encryptionFactory,
                         common::ILogger &iLogger)
    : logger(iLogger, "[APP] "),
      stateFactory(),
      stateLocator(),
      stateContext{
        phoneNumber,
        smsDB,
        btsSender,
        user,
        timers,
        encryptionFactory,
        logger,
        stateFactory,
        stateLocator}
{
    logger.logInfo("Started");
    stateContext.setNotConnectedState();
}

Application::~Application()
{
    logger.logInfo("Stopped");
}

void Application::handleSib()
{
    HandlersGuardLock lock(handlersGuard);
    getState().handleSib();
}

void Application::handleAttachAccept()
{
    HandlersGuardLock lock(handlersGuard);
    getState().handleAttachAccept();
}

void Application::handleAttachReject()
{
    HandlersGuardLock lock(handlersGuard);
    getState().handleAttachReject();
}

void Application::handleSmsReceived(common::PhoneNumber from, IDecoderPtr smsDecoder, const std::string &text)
{
    HandlersGuardLock lock(handlersGuard);
    getState().handleSmsReceived(from, smsDecoder, text);
}

void Application::handleCallRequestFrom(common::PhoneNumber from, IEncoderPtr callEncoder)
{
    HandlersGuardLock lock(handlersGuard);
    getState().handleCallRequestFrom(from, callEncoder);
}

void Application::handleCallAcceptFrom(common::PhoneNumber from, IEncoderPtr callEncoder)
{
    HandlersGuardLock lock(handlersGuard);
    getState().handleCallAcceptFrom(from, callEncoder);
}

void Application::handleCallTalkFrom(common::PhoneNumber from, const std::string &text)
{
    HandlersGuardLock lock(handlersGuard);
    getState().handleCallTalkFrom(from, text);
}

void Application::handleCallDropFrom(common::PhoneNumber from)
{
    HandlersGuardLock lock(handlersGuard);
    getState().handleCallDropFrom(from);
}

void Application::handleExitView()
{
    HandlersGuardLock lock(handlersGuard);
    getState().handleExitView();
}

void Application::handleSendSms(common::PhoneNumber to, const std::string &text)
{
    HandlersGuardLock lock(handlersGuard);
    getState().handleSendSms(to, text);
}

void Application::handleEditSms(IEncoderPtr smsEncoder)
{
    HandlersGuardLock lock(handlersGuard);
    getState().handleEditSms(smsEncoder);
}

void Application::handleViewSms(ISmsDB::Index smsIndex)
{
    HandlersGuardLock lock(handlersGuard);
    getState().handleViewSms(smsIndex);
}

void Application::handleViewSmses()
{
    HandlersGuardLock lock(handlersGuard);
    getState().handleViewSmses();
}

void Application::handleUserAccept()
{
    HandlersGuardLock lock(handlersGuard);
    getState().handleUserAccept();
}

void Application::handleUserTalk(const std::string &text)
{
    HandlersGuardLock lock(handlersGuard);
    getState().handleUserTalk(text);
}

void Application::handleDialing(IDecoderPtr talkDecoder)
{
    HandlersGuardLock lock(handlersGuard);
    getState().handleDialing(talkDecoder);
}

void Application::handleUserCalling(common::PhoneNumber to)
{
    HandlersGuardLock lock(handlersGuard);
    getState().handleUserCalling(to);
}

void Application::handleAttachTimeout()
{
    HandlersGuardLock lock(handlersGuard);
    getState().handleAttachTimeout();
}

void Application::handlePeerCallingTimeout()
{
    HandlersGuardLock lock(handlersGuard);
    getState().handlePeerCallingTimeout();
}

void Application::handleUserCallingTimeout()
{
    HandlersGuardLock lock(handlersGuard);
    getState().handleUserCallingTimeout();
}

void Application::handleTalkingTimeout()
{
    HandlersGuardLock lock(handlersGuard);
    getState().handleTalkingTimeout();
}

IEventsReceiver& Application::getState()
{
    return stateLocator.getState();
}

}
