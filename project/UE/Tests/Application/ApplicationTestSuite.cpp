#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "Application.hpp"
#include "Mocks/ILoggerMock.hpp"
#include "Mocks/ISmsDBMock.hpp"
#include "Mocks/IBtsSenderMock.hpp"
#include "Mocks/IUserMock.hpp"
#include "Mocks/ITimersMock.hpp"
#include "Mocks/IEncryptionMock.hpp"
#include "Mocks/IEncryptionFactoryMock.hpp"
#include "Crypto/EncryptionKind.hpp"
#include "Messages/PhoneNumber.hpp"
#include <memory>

namespace ue
{
using namespace ::testing;

class ApplicationTestSuite : public Test
{
protected:
    const common::PhoneNumber PHONE_NUMBER{112};
    const common::PhoneNumber PEER_PHONE_NUMBER{18};
    const common::PhoneNumber OTHER_PHONE_NUMBER{97};

    const std::string SMS_ENCRYPTED_TEXT = "983[2jfm/lkewf89243";
    const std::string SMS_TEXT = "SMS Text";
    const std::string SHORT_SMS_TEXT = "SMS Text from 18";
    const ISmsDB::Index SMS_INDEX = 2345u;

    const std::string TEXT1 = "Text1";
    const std::string ENCODED_TEXT1 = "$$$$$$$$$$$########$$$$$";
    const std::string TEXT2 = "Text2";
    const std::string ENCODED_TEXT2 = "$$$$$$$$$$$########";

    NiceMock<common::ILoggerMock> loggerMock;
    StrictMock<ISmsDBMock> smsDBMock;
    StrictMock<IBtsSenderMock> btsSenderMock;
    StrictMock<IUserMock> userMock;
    StrictMock<ITimersMock> timersMock;

    StrictMock<IEncryptionFactoryMock> encryptionFactoryMock;

    std::shared_ptr<IDecoderMock> smsDecoderMock = std::make_shared<StrictMock<IDecoderMock>>();
    Expectation expectPrintFromSmsDecoderMock = EXPECT_CALL(*smsDecoderMock, printData(_)).Times(AnyNumber());

    std::shared_ptr<IEncoderMock> smsEncoderMock = std::make_shared<StrictMock<IEncoderMock>>();
    Expectation expectPrintFromSmsEncoderMock = EXPECT_CALL(*smsEncoderMock, printData(_)).Times(AnyNumber());

    const EncryptionKind TALK_ENCRYPTION_KIND = EncryptionKind::Caesar;

    std::shared_ptr<IEncoderMock> talkToPeerEncoderMock = std::make_shared<StrictMock<IEncoderMock>>();
    Expectation expectPrintFromTalkToPeerEncoderMock = EXPECT_CALL(*talkToPeerEncoderMock, printData(_)).Times(AnyNumber());
    Expectation expectTalkToPeerKind = EXPECT_CALL(*talkToPeerEncoderMock, getKind()).WillRepeatedly(Return(TALK_ENCRYPTION_KIND));

    std::shared_ptr<IDecoderMock> talkFromPeerDecoderMock = std::make_shared<StrictMock<IDecoderMock>>();
    Expectation expectPrintFromTalkFromPeerDecoderMock = EXPECT_CALL(*talkFromPeerDecoderMock, printData(_)).Times(AnyNumber());

    Expectation ignoreAllNonErrorLogs = EXPECT_CALL(loggerMock, log(Ne(ILogger::ERROR_LEVEL), _)).Times(AnyNumber());
    Expectation expectShowNotConnectedAtStart = EXPECT_CALL(userMock, showNotConnected());
    Application objectUnderTest{PHONE_NUMBER,
                                smsDBMock,
                                btsSenderMock,
                                userMock,
                                timersMock,
                                encryptionFactoryMock,
                                loggerMock};

    Matcher<ILogger::Level> notError = Ne(ILogger::ERROR_LEVEL);
    Matcher<ILogger::Level> isError = Eq(ILogger::ERROR_LEVEL);
    template <typename TextMatcher, typename LevelMatcher>
    void expectLog(const TextMatcher& text, const LevelMatcher& level);
};

template <typename TextMatcher, typename LevelMatcher>
void ApplicationTestSuite::expectLog(const TextMatcher& text, const LevelMatcher& level)
{
    EXPECT_CALL(loggerMock, log(level, text));
}

TEST_F(ApplicationTestSuite, shallStoreReceivedSms)
{
    EXPECT_CALL(*smsDecoderMock, decodeSms(SMS_ENCRYPTED_TEXT)).WillOnce(Return(SMS_TEXT));
    EXPECT_CALL(smsDBMock, addReceivedSms(PEER_PHONE_NUMBER, SMS_TEXT)).WillOnce(Return(SMS_INDEX));
    EXPECT_CALL(smsDBMock, getSmsShortRepresentation(SMS_INDEX)).WillOnce(Return(SHORT_SMS_TEXT));

    expectLog(HasSubstr(SHORT_SMS_TEXT), notError);

    objectUnderTest.handleSmsReceived(PEER_PHONE_NUMBER, smsDecoderMock, SMS_ENCRYPTED_TEXT);
}

using ApplicationNotConnectedTestSuite = ApplicationTestSuite;
TEST_F(ApplicationNotConnectedTestSuite, shallComplainOnUnexpectedEvent)
{
    expectLog(_, isError);

    objectUnderTest.handleAttachAccept();
}

class ApplicationConnectingTestSuite : public ApplicationNotConnectedTestSuite
{
protected:
    ApplicationConnectingTestSuite()
    {
        EXPECT_CALL(userMock, showConnecting());
        EXPECT_CALL(btsSenderMock, sendAttachRequest());
        EXPECT_CALL(timersMock, startAttachTimer());

        objectUnderTest.handleSib();
    }

};

TEST_F(ApplicationConnectingTestSuite, shallStartAttachOnSib)
{
    // see test-suite constructor
}


TEST_F(ApplicationConnectingTestSuite, shallDisconnectOnAttachReject)
{
    EXPECT_CALL(userMock, showNotConnected());
    expectLog(_, isError);

    EXPECT_CALL(timersMock, stopAttachTimer());
    objectUnderTest.handleAttachReject();
}

TEST_F(ApplicationConnectingTestSuite, shallGiveUpOnTimeout)
{
    EXPECT_CALL(userMock, showNotConnected());
    expectLog(_, isError);

    objectUnderTest.handleAttachTimeout();
}

TEST_F(ApplicationConnectingTestSuite, shallRestartAttachOnSib)
{
    EXPECT_CALL(timersMock, startAttachTimer());
    EXPECT_CALL(btsSenderMock, sendAttachRequest());
    expectLog(_, isError);

    objectUnderTest.handleSib();
}

class ApplicationConnectedTestSuite : public ApplicationConnectingTestSuite
{
protected:
    ApplicationConnectedTestSuite()
    {
        EXPECT_CALL(timersMock, stopAttachTimer());
        EXPECT_CALL(userMock, showConnected());

        objectUnderTest.handleAttachAccept();
    }
};

TEST_F(ApplicationConnectedTestSuite, shallConnectOnAttachAccept)
{
    // see test-suite constructor
}



TEST_F(ApplicationConnectedTestSuite, shallComplainOnSib)
{
    expectLog(_, isError);

    objectUnderTest.handleSib();
}

class ApplicationViewSmsesTestSuite : public ApplicationConnectedTestSuite
{
protected:
    const ISmsDB::Index SMS_INDEX = 17;
    const std::string SMS_TEXT = "Some SMS Text\nWith Few Lines\n...";

    ApplicationViewSmsesTestSuite()
    {
        EXPECT_CALL(userMock, showSmses());
        objectUnderTest.handleViewSmses();
    }
};

TEST_F(ApplicationViewSmsesTestSuite, shallViewSmses)
{
    // see test-suite constructor
}

TEST_F(ApplicationViewSmsesTestSuite, shallExitToConnected)
{
    EXPECT_CALL(userMock, showConnected());
    objectUnderTest.handleExitView();
}

class ApplicationViewSmsTestSuite : public ApplicationViewSmsesTestSuite
{
protected:
    ApplicationViewSmsTestSuite()
    {
        EXPECT_CALL(smsDBMock, getSmsFullRepresentation(SMS_INDEX)).WillOnce(Return(SMS_TEXT));
        EXPECT_CALL(userMock, showSms(SMS_TEXT));
        objectUnderTest.handleViewSms(SMS_INDEX);
    }
};

TEST_F(ApplicationViewSmsTestSuite, shallViewSms)
{
    // see test-suite constructor
}

TEST_F(ApplicationViewSmsTestSuite, shallExitToViewSmses)
{
    EXPECT_CALL(userMock, showSmses());
    objectUnderTest.handleExitView();
}

class ApplicationEditSmsTestSuite : public ApplicationConnectedTestSuite
{
protected:
    ApplicationEditSmsTestSuite()
    {
        EXPECT_CALL(userMock, editSms());
        objectUnderTest.handleEditSms(smsEncoderMock);
    }
};

TEST_F(ApplicationEditSmsTestSuite, shallEditSms)
{
    // see test-suite c-tor
}

TEST_F(ApplicationEditSmsTestSuite, shallSendSms)
{
    EXPECT_CALL(btsSenderMock, sendSms(PEER_PHONE_NUMBER, Eq<IEncoderPtr>(smsEncoderMock), SMS_TEXT));
    EXPECT_CALL(smsDBMock, addSentSms(PEER_PHONE_NUMBER, SMS_TEXT));
    EXPECT_CALL(userMock, showConnected());
    objectUnderTest.handleSendSms(PEER_PHONE_NUMBER, SMS_TEXT);
}

TEST_F(ApplicationEditSmsTestSuite, shallExitToConnected)
{
    EXPECT_CALL(userMock, showConnected());
    objectUnderTest.handleExitView();
}

class ApplicationPeerCallingTestSuite : public ApplicationConnectedTestSuite
{
protected:
    ApplicationPeerCallingTestSuite()
    {
        EXPECT_CALL(userMock, showPeerCalling(PEER_PHONE_NUMBER));
        EXPECT_CALL(timersMock, startPeerCallingTimer());
        objectUnderTest.handleCallRequestFrom(PEER_PHONE_NUMBER, talkToPeerEncoderMock);
    }
};

TEST_F(ApplicationPeerCallingTestSuite, shallWaitForPeerCallRequestAccept)
{
    // see test-suite c-tor
}

TEST_F(ApplicationPeerCallingTestSuite, shallDropCallRequestOnExitView)
{
    EXPECT_CALL(timersMock, stopPeerCallingTimer());
    EXPECT_CALL(userMock, showConnected());
    EXPECT_CALL(btsSenderMock, sendCallDrop(PEER_PHONE_NUMBER));
    objectUnderTest.handleExitView();
}

TEST_F(ApplicationPeerCallingTestSuite, shallDropCallRequestOnTimeout)
{
    EXPECT_CALL(userMock, showConnected());
    EXPECT_CALL(btsSenderMock, sendCallDrop(PEER_PHONE_NUMBER));
    objectUnderTest.handlePeerCallingTimeout();
}

TEST_F(ApplicationPeerCallingTestSuite, shallExitViewOnCallDropFromPeerUser)
{
    EXPECT_CALL(userMock, showConnected());
    EXPECT_CALL(timersMock, stopPeerCallingTimer());
    objectUnderTest.handleCallDropFrom(PEER_PHONE_NUMBER);
}

TEST_F(ApplicationPeerCallingTestSuite, shallComplainOnCallDropFromUnknownUser)
{
    expectLog(HasSubstr(to_string(OTHER_PHONE_NUMBER)), isError);
    objectUnderTest.handleCallDropFrom(OTHER_PHONE_NUMBER);
}

class ApplicationTalkingStartedByPeerUserTestSuite : public ApplicationPeerCallingTestSuite
{
protected:
    ApplicationTalkingStartedByPeerUserTestSuite()
    {
        EXPECT_CALL(timersMock, stopPeerCallingTimer());
        EXPECT_CALL(timersMock, startTalkingTimer());
        EXPECT_CALL(userMock, showTalkingView(PEER_PHONE_NUMBER));
        EXPECT_CALL(encryptionFactoryMock, getDecoderFor(TALK_ENCRYPTION_KIND)).WillOnce(Return(talkFromPeerDecoderMock));
        EXPECT_CALL(btsSenderMock, sendCallAccept(PEER_PHONE_NUMBER, Eq<IDecoderPtr>(talkFromPeerDecoderMock)));
        objectUnderTest.handleUserAccept();
    }
};

TEST_F(ApplicationTalkingStartedByPeerUserTestSuite, shallAcceptCallRequestOnUserAccept)
{
    // see TestSuite c-tor
}

TEST_F(ApplicationTalkingStartedByPeerUserTestSuite, shallSendTextToPeerUser)
{
    EXPECT_CALL(*talkToPeerEncoderMock, encodeTalk(TEXT1)).WillOnce(Return(ENCODED_TEXT1));
    EXPECT_CALL(btsSenderMock, sendCallTalk(PEER_PHONE_NUMBER, ENCODED_TEXT1));
    EXPECT_CALL(userMock, addUserTalk(PEER_PHONE_NUMBER, TEXT1));
    EXPECT_CALL(timersMock, startTalkingTimer()); // actually this is re-start of this timer
    objectUnderTest.handleUserTalk(TEXT1);
}

TEST_F(ApplicationTalkingStartedByPeerUserTestSuite, shallReceiveTextFromPeerUser)
{
    EXPECT_CALL(*talkFromPeerDecoderMock, decodeTalk(ENCODED_TEXT2)).WillOnce(Return(TEXT2));
    EXPECT_CALL(userMock, addPeerUserTalk(PEER_PHONE_NUMBER, TEXT2));
    EXPECT_CALL(timersMock, startTalkingTimer()); // actually this is re-start of this timer
    objectUnderTest.handleCallTalkFrom(PEER_PHONE_NUMBER, ENCODED_TEXT2);
}

TEST_F(ApplicationTalkingStartedByPeerUserTestSuite, shallComplainOnTextFromUknownPeerUser)
{
    expectLog(HasSubstr(to_string(OTHER_PHONE_NUMBER)), isError);
    objectUnderTest.handleCallTalkFrom(OTHER_PHONE_NUMBER, TEXT2);
}

TEST_F(ApplicationTalkingStartedByPeerUserTestSuite, shallExitTalking)
{
    EXPECT_CALL(timersMock, stopTalkingTimer());
    EXPECT_CALL(userMock, showConnected());
    EXPECT_CALL(btsSenderMock, sendCallDrop(PEER_PHONE_NUMBER));
    objectUnderTest.handleExitView();
}

TEST_F(ApplicationTalkingStartedByPeerUserTestSuite, shallHandlePeerDropCall)
{
    EXPECT_CALL(timersMock, stopTalkingTimer());
    EXPECT_CALL(userMock, showConnected());
    objectUnderTest.handleCallDropFrom(PEER_PHONE_NUMBER);
}

TEST_F(ApplicationTalkingStartedByPeerUserTestSuite, shallOnlyComplainOnUnknownDropCall)
{
    expectLog(HasSubstr(to_string(OTHER_PHONE_NUMBER)), isError);
    objectUnderTest.handleCallDropFrom(OTHER_PHONE_NUMBER);
}

TEST_F(ApplicationTalkingStartedByPeerUserTestSuite, shallHandleTalkingTimeout)
{
    EXPECT_CALL(btsSenderMock, sendCallDrop(PEER_PHONE_NUMBER));
    EXPECT_CALL(userMock, showConnected());
    objectUnderTest.handleTalkingTimeout();
}

class ApplicationDialingTestSuite : public ApplicationConnectedTestSuite
{
protected:
    ApplicationDialingTestSuite()
    {
        EXPECT_CALL(userMock, showDialing());
        objectUnderTest.handleDialing(talkFromPeerDecoderMock);
    }
};

TEST_F(ApplicationDialingTestSuite, shallShowDialing)
{
    // see test-suite c-tor
}

TEST_F(ApplicationDialingTestSuite, shallExitDialing)
{
    EXPECT_CALL(userMock, showConnected());
    objectUnderTest.handleExitView();
}

class ApplicationUserCallingTestSuite : public ApplicationDialingTestSuite
{
protected:
    ApplicationUserCallingTestSuite()
    {
        EXPECT_CALL(timersMock, startUserCallingTimer());
        EXPECT_CALL(userMock, showUserCalling(PEER_PHONE_NUMBER));
        EXPECT_CALL(btsSenderMock, sendCallRequest(PEER_PHONE_NUMBER, Eq<IDecoderPtr>(talkFromPeerDecoderMock)));
        objectUnderTest.handleUserCalling(PEER_PHONE_NUMBER);
    }
};

TEST_F(ApplicationUserCallingTestSuite, shallShowUserCalling)
{
    // see test-suite c-tor
}

TEST_F(ApplicationUserCallingTestSuite, shallIgnoreAcceptFromOtherPeerUser)
{
    expectLog(_, isError);
    objectUnderTest.handleCallAcceptFrom(OTHER_PHONE_NUMBER, talkToPeerEncoderMock);
}

TEST_F(ApplicationUserCallingTestSuite, shallDropCallOnExitView)
{
    EXPECT_CALL(timersMock, stopUserCallingTimer());
    EXPECT_CALL(btsSenderMock, sendCallDrop(PEER_PHONE_NUMBER));
    EXPECT_CALL(userMock, showConnected());
    objectUnderTest.handleExitView();
}

TEST_F(ApplicationUserCallingTestSuite, shallDropCallOnTimeout)
{
    EXPECT_CALL(btsSenderMock, sendCallDrop(PEER_PHONE_NUMBER));
    EXPECT_CALL(userMock, showConnected());
    objectUnderTest.handleUserCallingTimeout();
}

TEST_F(ApplicationUserCallingTestSuite, shallIgnoreUserAccept)
{
    objectUnderTest.handleUserAccept();
}

TEST_F(ApplicationUserCallingTestSuite, shallExitViewOnDropCallFromPeerUser)
{
    EXPECT_CALL(timersMock, stopUserCallingTimer());
    EXPECT_CALL(userMock, showConnected());
    objectUnderTest.handleCallDropFrom(PEER_PHONE_NUMBER);
}

TEST_F(ApplicationUserCallingTestSuite, shallComplainOnDropCallFromUnknownUser)
{
    expectLog(HasSubstr(to_string(OTHER_PHONE_NUMBER)), isError);
    objectUnderTest.handleCallDropFrom(OTHER_PHONE_NUMBER);
}


class ApplicationTalkingStartedByUserTestSuite : public ApplicationUserCallingTestSuite
{
protected:
    ApplicationTalkingStartedByUserTestSuite()
    {
        EXPECT_CALL(timersMock, stopUserCallingTimer());
        EXPECT_CALL(timersMock, startTalkingTimer());
        EXPECT_CALL(userMock, showTalkingView(PEER_PHONE_NUMBER));
        objectUnderTest.handleCallAcceptFrom(PEER_PHONE_NUMBER, talkToPeerEncoderMock);
    }
};

TEST_F(ApplicationTalkingStartedByUserTestSuite, shallStartTalking)
{
    // see test-suite c-tor
}

TEST_F(ApplicationTalkingStartedByUserTestSuite, shallSendTextToPeerUser)
{
    EXPECT_CALL(*talkToPeerEncoderMock, encodeTalk(TEXT1)).WillOnce(Return(ENCODED_TEXT1));
    EXPECT_CALL(btsSenderMock, sendCallTalk(PEER_PHONE_NUMBER, ENCODED_TEXT1));
    EXPECT_CALL(userMock, addUserTalk(PEER_PHONE_NUMBER, TEXT1));
    EXPECT_CALL(timersMock, startTalkingTimer()); // actually this is re-start of this timer
    objectUnderTest.handleUserTalk(TEXT1);
}

TEST_F(ApplicationTalkingStartedByUserTestSuite, shallReceiveTextFromPeerUser)
{
    EXPECT_CALL(*talkFromPeerDecoderMock, decodeTalk(ENCODED_TEXT2)).WillOnce(Return(TEXT2));
    EXPECT_CALL(userMock, addPeerUserTalk(PEER_PHONE_NUMBER, TEXT2));
    EXPECT_CALL(timersMock, startTalkingTimer()); // actually this is re-start of this timer
    objectUnderTest.handleCallTalkFrom(PEER_PHONE_NUMBER, ENCODED_TEXT2);
}

TEST_F(ApplicationTalkingStartedByUserTestSuite, shallComplainOnTextFromUknownPeerUser)
{
    expectLog(HasSubstr(to_string(OTHER_PHONE_NUMBER)), isError);
    objectUnderTest.handleCallTalkFrom(OTHER_PHONE_NUMBER, TEXT2);
}

TEST_F(ApplicationTalkingStartedByUserTestSuite, shallExitTalking)
{
    EXPECT_CALL(timersMock, stopTalkingTimer());
    EXPECT_CALL(userMock, showConnected());
    EXPECT_CALL(btsSenderMock, sendCallDrop(PEER_PHONE_NUMBER));
    objectUnderTest.handleExitView();
}

TEST_F(ApplicationTalkingStartedByUserTestSuite, shallHandlePeerDropCall)
{
    EXPECT_CALL(timersMock, stopTalkingTimer());
    EXPECT_CALL(userMock, showConnected());
    objectUnderTest.handleCallDropFrom(PEER_PHONE_NUMBER);
}

TEST_F(ApplicationTalkingStartedByUserTestSuite, shallHandleTalkingTimeout)
{
    EXPECT_CALL(btsSenderMock, sendCallDrop(PEER_PHONE_NUMBER));
    EXPECT_CALL(userMock, showConnected());
    objectUnderTest.handleTalkingTimeout();
}

TEST_F(ApplicationTalkingStartedByUserTestSuite, shallOnlyComplainOnUnknownDropCall)
{
    expectLog(HasSubstr(to_string(OTHER_PHONE_NUMBER)), isError);
    objectUnderTest.handleCallDropFrom(OTHER_PHONE_NUMBER);
}


}

