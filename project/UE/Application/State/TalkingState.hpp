#pragma once

#include "BaseState.hpp"

namespace ue
{

class TalkingState : public BaseState
{
public:
    TalkingState(StateContext& stateContext,
                 common::PhoneNumber to,
                 IEncoderPtr talkEncoder,
                 IDecoderPtr talkDecoder);
    void handleExitView() override;
    void handleUserTalk(const std::string& text) override;
    void handleCallTalkFrom(common::PhoneNumber from, const std::string &text) override;
    void handleCallDropFrom(common::PhoneNumber from) override;
    void handleTalkingTimeout() override;

private:
    common::PhoneNumber to;
    IEncoderPtr talkEncoder;
    IDecoderPtr talkDecoder;
};

}
