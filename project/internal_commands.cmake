#######################
### OpenSSL - BEGIN ###
#######################
if(WIN32 AND MINGW)
    # cmake -DOPENSSL_ROOT_DIR=/usr/local/ssl -DOPENSSL_LIBRARIES=/usr/local/ssl/lib

    set(OPENSSL_INCLUDE_DIR C:/OpenSSL-Win32/include)
    set(OPENSSL_LIBRARIES   crypto-1_1 -LC:/OpenSSL-Win32/lib/MinGW)

    message(STATUS "WIN32+MINGW: Using manual OpenSSL paths [OPENSSL_INCLUDE_DIR ${OPENSSL_INCLUDE_DIR} ,OPENSSL_LIBRARIES ${OPENSSL_LIBRARIES}]")
else()
    find_package(OpenSSL)
    if( OPENSSL_FOUND )
        message(STATUS "Using OpenSSL ${OPENSSL_VERSION}")
    else()
        message(ERROR "OpenSSL not found")
    endif()
endif()
include_directories(${OPENSSL_INCLUDE_DIR})
#######################
### OpenSSL - END   ###
#######################


macro(set_compiler_options)
# set(CMAKE_CXX_FLAGS "-std=c++14 -DGTEST_HAS_PTHREAD=1 -g -Og ${CMAKE_CXX_FLAGS}")
# add_definitions(-std=c++14)
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "-g -Og ${CMAKE_CXX_FLAGS}")
endmacro()

macro(set_qt_options)
set(CMAKE_AUTOMOC ON)
set(CMAKE_CXX_FLAGS ${Qt5Widgets_EXECUTABLE_COMPILE_FLAGS} ${Qt5Network_EXECUTABLE_COMPILE_FLAGS} ${CMAKE_CXX_FLAGS})
find_package(Qt5Widgets REQUIRED)
include_directories(${Qt5Widgets_INCLUDES})
add_definitions(${Qt5Widgets_DEFINITIONS})
find_package(Qt5Network REQUIRED)
include_directories(${Qt5Network_INCLUDES})
add_definitions(${Qt5Network_DEFINITIONS})
endmacro()

macro(target_link_qt)
set_property(TARGET ${PROJECT_NAME} PROPERTY CXX_STANDARD 14)
target_link_libraries(${PROJECT_NAME} ${Qt5Widgets_LIBRARIES} ${Qt5Network_LIBRARIES} pthread ${OPENSSL_LIBRARIES})
endmacro()

macro(set_gtest_options)
set(GMOCK_DIR ${CMAKE_SOURCE_DIR}/gmock)
set(GTEST_DIR ${GMOCK_DIR}/gtest)

include_directories(${GMOCK_DIR}/include)
include_directories(${GTEST_DIR}/include)
include_directories(${GMOCK_DIR})
include_directories(${GTEST_DIR})
endmacro()

macro(target_link_gtest)
target_link_libraries(${PROJECT_NAME} gmock_main)
endmacro()

macro(copy_images)
file(COPY ${IMAGES_DIRECTORY}/images DESTINATION .)
endmacro()


