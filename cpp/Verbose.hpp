#pragma once

#include <iostream>
#include <string>

class Verbose
{
public:
    Verbose()
    {
        std::cout << this << "Verbose();\n";
    }

    Verbose(const std::string& name)
        : name(name)
    {
        std::cout << this << "Verbose(\"" << name << "\")\n";
    }

    Verbose(const Verbose& other)
        : name(other.name)
    {
        std::cout << this << "Verbose(const Verbose& = " << &other << ":\"" << name << "\")\n";
    }

    Verbose(Verbose&& other)
        : name(std::move(other.name))
    {
        std::cout << this << "Verbose(Verbose&& = " << &other << ":\"" << name << "\")\n";
    }

    virtual ~Verbose()
    {
        std::cout << this << "~Verbose():\"" << name << "\")\n";
    }

    Verbose& operator = (const Verbose& other)
    {
        std::cout << this << ":\"" << name << "\"Verbose& operator = (const Verbose& = " << &other << ":\"" << name << "\")\n";
        name = other.name;
        return *this;
    }

    Verbose& operator = (Verbose&& other)
    {
        std::cout << this << ":\"" << name << "\"Verbose& operator = (Verbose&& = " << &other << ":\"" << name << "\")\n";
        name = std::move(other.name);
        return *this;
    }

    bool operator == (const Verbose& other) const
    {
        std::cout << this << ":\"" << name << "\"operator == (const Verbose& = " << &other << ":\"" << name << "\")\n";
        return name == other.name;
    }
    bool operator != (const Verbose& other) const
    {
        std::cout << this << ":\"" << name << "\"operator != (const Verbose& = " << &other << ":\"" << name << "\")\n";
        return name != other.name;
    }
    bool operator < (const Verbose& other) const
    {
        std::cout << this << ":\"" << name << "\"operator < (const Verbose& = " << &other << ":\"" << name << "\")\n";
        return name < other.name;
    }


private:
    std::string name;
};

