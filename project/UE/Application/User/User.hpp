#pragma once

#include "IUser.hpp"
#include "IUserCommands.hpp"
#include "IUserReceiver.hpp"
#include "Logger/PrefixedLogger.hpp"
#include "IUeGui.hpp"
#include "Sms/ISmsDB.hpp"
#include "Crypto/IEncryptionFactory.hpp"

#include <string>
#include <functional>
#include <vector>

namespace ue
{

using common::PhoneNumber;
using common::ILogger;

class User : public IUser, public IUserCommands
{
public:
    User(PhoneNumber phoneNumber, IUeGui& ueGui, ISmsDB& smsDB, IEncryptionFactory& encryptionFactory, ILogger& iLogger);
    ~User() override;

    using MenuIndex = std::size_t;
    static constexpr std::size_t VIEW_SMSES_MENU_INDEX = 0;
    static constexpr std::size_t SEND_SMS_MENU_INDEX = 1;
    static constexpr std::size_t SEND_SMS_XOR_MENU_INDEX = 2;
    static constexpr std::size_t SEND_SMS_CAESAR_MENU_INDEX = 3;
    static constexpr std::size_t SEND_SMS_RSA_MENU_INDEX = 4;
    static constexpr std::size_t CALL_MENU_INDEX = 5;
    static constexpr std::size_t CALL_XOR_MENU_INDEX = 6;
    static constexpr std::size_t CALL_CAESAR_MENU_INDEX = 7;
    static constexpr std::size_t CALL_RSA_MENU_INDEX = 8;

    void showNotConnected() override;
    void showConnecting() override;
    void showConnected() override;
    void showSmses() override;
    void showSms(const std::string& text) override;
    void editSms() override;
    void showPeerCalling(common::PhoneNumber) override;
    void showTalkingView(common::PhoneNumber to) override;
    void addPeerUserTalk(common::PhoneNumber from, const std::string& text) override;
    void addUserTalk(common::PhoneNumber to, const std::string& text) override;
    void showDialing() override;
    void showUserCalling(common::PhoneNumber to) override;

    void setReceiver(IUserReceiver &newReceiver);
    void resetReceiver();

private:
    IUserReceiver& getReceiver();
    void printPrefix(std::ostream& os) const;

    const PhoneNumber phoneNumber;
    IUeGui& ueGui;
    ISmsDB& smsDB;
    common::PrefixedLogger logger;

    class IdleReceiver : public IUserReceiver
    {
    public:
        IdleReceiver(common::ILogger& iLogger);
        void handleExitView() override;
        void handleSendSms(common::PhoneNumber to, const std::string &text) override;
        void handleEditSms(IEncoderPtr smsEncoder) override;
        void handleViewSms(ISmsDB::Index smsIndex) override;
        void handleViewSmses() override;
        void handleUserAccept() override;
        void handleUserTalk(const std::string& text) override;
        void handleDialing(IDecoderPtr talkDecoder) override;
        void handleUserCalling(common::PhoneNumber to) override;

    private:
        common::PrefixedLogger logger;
    };
    IdleReceiver idleReceiver;
    std::reference_wrapper<IUserReceiver> receiver;

    using MenuHandler = std::function<void()>;
    struct MenuOption
    {
        std::string name;
        std::string tip;
        MenuHandler handler;
    };
    std::vector<MenuOption> menuOptions;
    void addMenuOption(MenuIndex index, MenuHandler handler, const std::string& optionName, const std::string& optionTip = "");
};

}
