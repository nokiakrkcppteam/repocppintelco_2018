#include "ITimeoutsReceiverMock.hpp"

namespace ue
{

ITimeoutsReceiverMock::ITimeoutsReceiverMock() = default;
ITimeoutsReceiverMock::~ITimeoutsReceiverMock() = default;

}
