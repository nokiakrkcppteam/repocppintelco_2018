#include "NoneEncryption.hpp"
#include "Messages/OutgoingMessage.hpp"
#include "EncryptionKind.hpp"

namespace ue
{

EncryptionKind NoneEncryption::getKind() const
{
    return EncryptionKind::None;
}

std::string NoneEncryption::encodeSms(const std::string &text) const
{
    return text;
}

std::string NoneEncryption::decodeSms(const std::string &text) const
{
    return text;
}

std::string NoneEncryption::encodeTalk(const std::string &text) const
{
    return text;
}

std::string NoneEncryption::decodeTalk(const std::string &text) const
{
    return text;
}

void NoneEncryption::writeTo(common::OutgoingMessage &builder) const
{
    builder.writeNumber(static_cast<std::uint8_t>(EncryptionKind::None));
}

void NoneEncryption::printData(std::ostream &os) const
{
    os << "NoneEncryption";
}

}
