#include "EditSmsState.hpp"

namespace ue
{

EditSmsState::EditSmsState(StateContext& stateContext, IEncoderPtr smsEncoder)
    : BaseState(stateContext, "[EDIT-SMS] "),
      smsEncoder(smsEncoder)
{
    user.editSms();
}

void EditSmsState::handleSendSms(common::PhoneNumber to, const std::string &text)
{
    logger.logInfo("Send SMS to: ", to);
    btsSender.sendSms(to, smsEncoder, text);
    smsDB.addSentSms(to, text);
    setConnectedState();
}

void EditSmsState::handleExitView()
{
    logger.logInfo("Back to connected state");
    setConnectedState();
}

}
