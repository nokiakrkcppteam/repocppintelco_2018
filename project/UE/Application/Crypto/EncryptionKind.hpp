#pragma once

#include <cstdint>
#include <iostream>

namespace ue
{

enum class EncryptionKind : std::uint8_t
{
    None = 0,
    Xor = 1,
    Caesar  = 2,
    Rsa = 3
};


std::ostream& operator << (std::ostream& os, EncryptionKind kind);

}
