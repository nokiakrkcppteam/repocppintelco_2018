#pragma once

#include "BaseState.hpp"

namespace ue
{

class EditSmsState : public BaseState
{
public:
    EditSmsState(StateContext& stateContext, IEncoderPtr smsEncoder);
    void handleSendSms(common::PhoneNumber to, const std::string& text) override;
    void handleExitView() override;

private:
    IEncoderPtr smsEncoder;
};

}
