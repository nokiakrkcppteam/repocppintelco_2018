#pragma once

#include <string>
#include "Messages.hpp"


namespace ue
{

class  IUser
{
public:
    virtual ~IUser() = default;

    virtual void showNotConnected() = 0;
    virtual void showConnecting() = 0;
    virtual void showConnected() = 0;
    virtual void showSmses() = 0;
    virtual void showSms(const std::string& text) = 0;
    virtual void editSms() = 0;
    virtual void showPeerCalling(common::PhoneNumber) = 0;
    virtual void showTalkingView(common::PhoneNumber to) = 0;
    virtual void addPeerUserTalk(common::PhoneNumber from, const std::string& text) = 0;
    virtual void addUserTalk(common::PhoneNumber to, const std::string& text) = 0;
    virtual void showDialing() = 0;
    virtual void showUserCalling(common::PhoneNumber to) = 0;
};

}
