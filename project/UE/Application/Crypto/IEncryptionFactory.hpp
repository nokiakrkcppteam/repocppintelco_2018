#pragma once

#include "IEncryption.hpp"
#include "EncryptionKind.hpp"

namespace common
{
class IncomingMessage;
class OutgoingMessage;
class PhoneNumber;
}

namespace ue
{

class IEncryptionFactory
{
public:
    virtual ~IEncryptionFactory() = default;

    virtual IEncoderPtr getEncoderFor(EncryptionKind kind) = 0;
    virtual IDecoderPtr readDecoder(common::IncomingMessage&) = 0;
    virtual IEncoderPtr readEncoder(common::IncomingMessage&) = 0;
    virtual IDecoderPtr getDecoderFor(EncryptionKind kind) = 0;
};

}
