#pragma once

#include <gmock/gmock.h>
#include "Sms/ISmsDB.hpp"

namespace ue
{

struct ISmsDBMock : ISmsDB
{
    ISmsDBMock();
    ~ISmsDBMock() override;

    MOCK_METHOD2(addSentSms, Index(common::PhoneNumber to, const std::string &text));
    MOCK_METHOD2(addReceivedSms, Index(common::PhoneNumber from, const std::string &text));
    MOCK_CONST_METHOD1(visitAll, void(Visitor));
    MOCK_CONST_METHOD1(getSmsShortRepresentation, std::string(Index index));
    MOCK_CONST_METHOD1(getSmsFullRepresentation, std::string(Index index));
};

}
