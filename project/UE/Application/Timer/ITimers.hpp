#pragma once

#include "Messages.hpp"

namespace ue
{

class ITimers
{
public:
    virtual ~ITimers() = default;

    virtual void startAttachTimer() = 0;
    virtual void stopAttachTimer() = 0;

    virtual void startPeerCallingTimer() = 0;
    virtual void stopPeerCallingTimer() = 0;

    virtual void startUserCallingTimer() = 0;
    virtual void stopUserCallingTimer() = 0;

    virtual void startTalkingTimer() = 0;
    virtual void stopTalkingTimer() = 0;

    virtual void timeoutExpiredTimers() = 0;
};

}
