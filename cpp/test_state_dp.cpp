#include <iostream>

using namespace std;

struct Box;

struct State
{
    virtual ~State(){}
    virtual void pushButton(Box& box) = 0;
    virtual bool isLit() {return false;}
};

struct OnState : State
{
    void pushButton(Box& box) override;
    bool isLit() override {return true;}
};

struct OffState : State
{
    void pushButton(Box& box) override;
};

struct Box
{
    void pushButton();
    bool isLit() {return currentState->isLit();}
    void turnLEDOn();
    void turnLEDOff();
private:
    OnState onState;
    OffState offState;
    State* currentState = &offState;
};

void Box::pushButton()
{
    currentState->pushButton(*this);
}

void Box::turnLEDOn()
{
    cout << "Turning on" << endl;
    currentState = &onState;
}

void Box::turnLEDOff()
{
    cout << "Turning off" << endl;
    currentState = &offState;
}

int main() {
    Box box;

    cout << box.isLit() << endl;
    box.pushButton();
    cout << box.isLit() << endl;
    box.pushButton();
    cout << box.isLit() << endl;
    box.pushButton();
    cout << box.isLit() << endl;
    box.pushButton();
    cout << box.isLit() << endl;
    box.pushButton();
    cout << box.isLit() << endl;

}

void OnState::pushButton(Box &box)
{
    box.turnLEDOff();
}

void OffState::pushButton(Box &box)
{
    box.turnLEDOn();
}
