#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "User/User.hpp"
#include "Mocks/ILoggerMock.hpp"
#include "Mocks/IUeGuiMock.hpp"
#include "Mocks/IUserReceiverMock.hpp"
#include "Mocks/ISmsDBMock.hpp"
#include "Mocks/IEncryptionMock.hpp"
#include "Mocks/IEncryptionFactoryMock.hpp"

namespace ue
{
using namespace ::testing;

class UserTestSuite : public Test
{
protected:
    const common::PhoneNumber PHONE_NUMBER{112};
    const common::PhoneNumber PEER_PHONE_NUMBER{17};
    const std::string SMS_TEXT = "Some text \n with some information";

    StrictMock<IEncryptionFactoryMock> encryptionFactoryMock;
    struct SmsEncoderData
    {
        std::shared_ptr<IEncoderMock> encoderMock = std::make_shared<StrictMock<IEncoderMock>>();
        Matcher<IEncoderPtr> encoderMatch = Eq<IEncoderPtr>(encoderMock);
        Expectation expectEncoderPrintable = EXPECT_CALL(*encoderMock, printData(_)).Times(AnyNumber());
    };
    struct TalkDecoderData
    {
        std::shared_ptr<IDecoderMock> decoderMock = std::make_shared<StrictMock<IDecoderMock>>();
        Matcher<IDecoderPtr> decoderMatch = Eq<IDecoderPtr>(decoderMock);
        Expectation expectDecoderPrintable = EXPECT_CALL(*decoderMock, printData(_)).Times(AnyNumber());
    };

    SmsEncoderData noneSmsEncryption;
    Expectation expectNoSmsEncryptionCreated = EXPECT_CALL(encryptionFactoryMock, getEncoderFor(EncryptionKind::None)).WillOnce(Return(noneSmsEncryption.encoderMock));

    SmsEncoderData xorSmsEncryption;
    Expectation expectXorSmsEncryptionCreated = EXPECT_CALL(encryptionFactoryMock, getEncoderFor(EncryptionKind::Xor)).WillOnce(Return(xorSmsEncryption.encoderMock));

    SmsEncoderData caesarSmsEncryption;
    Expectation expectCaesarSmsEncryptionCreated = EXPECT_CALL(encryptionFactoryMock, getEncoderFor(EncryptionKind::Caesar)).WillOnce(Return(caesarSmsEncryption.encoderMock));

    SmsEncoderData rsaSmsEncryption;
    Expectation expectRsaSmsEncryptionCreated = EXPECT_CALL(encryptionFactoryMock, getEncoderFor(EncryptionKind::Rsa)).WillOnce(Return(rsaSmsEncryption.encoderMock));

    TalkDecoderData noneCallDecryption;
    Expectation expectNoneCallDecryptionCreated = EXPECT_CALL(encryptionFactoryMock, getDecoderFor(EncryptionKind::None)).WillOnce(Return(noneCallDecryption.decoderMock));

    TalkDecoderData xorCallDecryption;
    Expectation expectXorCallDecryptionCreated = EXPECT_CALL(encryptionFactoryMock, getDecoderFor(EncryptionKind::Xor)).WillOnce(Return(xorCallDecryption.decoderMock));

    TalkDecoderData caesarCallDecryption;
    Expectation expectCaesarCallDecryptionCreated = EXPECT_CALL(encryptionFactoryMock, getDecoderFor(EncryptionKind::Caesar)).WillOnce(Return(caesarCallDecryption.decoderMock));

    TalkDecoderData rsaCallDecryption;
    Expectation expectRsaCallDecryptionCreated = EXPECT_CALL(encryptionFactoryMock, getDecoderFor(EncryptionKind::Rsa)).WillOnce(Return(rsaCallDecryption.decoderMock));

    NiceMock<common::ILoggerMock> loggerMock;
    StrictMock<ISmsDBMock> smsDBMock;
    StrictMock<IUserReceiverMock> userReceiverMock;
    StrictMock<IUeGuiMock> ueGuiMock;
    StrictMock<IListViewModeMock> listViewModeMock;
    StrictMock<ITextModeMock> textModeMock;
    StrictMock<ISmsComposeModeMock> smsComposeModeMock;
    StrictMock<ICallModeMock> callModeMock;
    StrictMock<IDialModeMock> dialModeMock;

    IUeGui::Callback acceptCallback;
    IUeGui::Callback rejectCallback;
    IUeGui::CloseGuard closeGuard;

    Expectation storeAcceptCallback = EXPECT_CALL(ueGuiMock, setAcceptCallback(_)).WillRepeatedly(SaveArg<0>(&acceptCallback));
    Expectation storeRejectCallback = EXPECT_CALL(ueGuiMock, setRejectCallback(_)).WillRepeatedly(SaveArg<0>(&rejectCallback));
    Expectation storeCloseGuard = EXPECT_CALL(ueGuiMock, setCloseGuard(_)).WillRepeatedly(SaveArg<0>(&closeGuard));

    Expectation expectSetTitle = EXPECT_CALL(ueGuiMock, setTitle(HasSubstr(to_string(PHONE_NUMBER))));
    User objectUnderTest{PHONE_NUMBER,
                         ueGuiMock,
                         smsDBMock,
                         encryptionFactoryMock,
                         loggerMock};
};

class UserWithReceiverTestSuite : public UserTestSuite
{
protected:
    IUserCommands::ScopeGuard connectReceiver{objectUnderTest, userReceiverMock};
};

TEST_F(UserWithReceiverTestSuite, shallExitView)
{
    EXPECT_CALL(userReceiverMock, handleExitView());

    ASSERT_TRUE(rejectCallback);
    rejectCallback();
}


TEST_F(UserWithReceiverTestSuite, shallShowNotConnected)
{
    EXPECT_CALL(ueGuiMock, showNotConnected());
    objectUnderTest.showNotConnected();
}

TEST_F(UserWithReceiverTestSuite, shallShowConnecting)
{
    EXPECT_CALL(ueGuiMock, showConnecting());
    objectUnderTest.showConnecting();
}

TEST_F(UserWithReceiverTestSuite, shallShowSms)
{
    EXPECT_CALL(ueGuiMock, setViewTextMode()).WillOnce(ReturnRef(textModeMock));
    EXPECT_CALL(textModeMock, setText(SMS_TEXT));

    objectUnderTest.showSms(SMS_TEXT);
}

class UserConnectedTestSuite : public UserWithReceiverTestSuite
{
protected:
    UserConnectedTestSuite()
    {
        EXPECT_CALL(ueGuiMock, setListViewMode()).WillOnce(ReturnRef(listViewModeMock));
        EXPECT_CALL(listViewModeMock, clearSelectionList());

        EXPECT_CALL(listViewModeMock, addSelectionListItem(AllOf(HasSubstr("View"), HasSubstr("Sms")), _));
        EXPECT_CALL(listViewModeMock, addSelectionListItem(AllOf(HasSubstr("Send"), HasSubstr("Sms")), _));
        EXPECT_CALL(listViewModeMock, addSelectionListItem(AllOf(HasSubstr("Send"), HasSubstr("Sms"), HasSubstr("Xor")), _));
        EXPECT_CALL(listViewModeMock, addSelectionListItem(AllOf(HasSubstr("Send"), HasSubstr("Sms"), HasSubstr("Caesar")), _));
        EXPECT_CALL(listViewModeMock, addSelectionListItem(AllOf(HasSubstr("Send"), HasSubstr("Sms"), HasSubstr("Rsa")), _));
        EXPECT_CALL(listViewModeMock, addSelectionListItem(HasSubstr("Call"), _));
        EXPECT_CALL(listViewModeMock, addSelectionListItem(AllOf(HasSubstr("Call"), HasSubstr("Xor")), _));
        EXPECT_CALL(listViewModeMock, addSelectionListItem(AllOf(HasSubstr("Call"), HasSubstr("Caesar")), _));
        EXPECT_CALL(listViewModeMock, addSelectionListItem(AllOf(HasSubstr("Call"), HasSubstr("Rsa")), _));

        objectUnderTest.showConnected();
    }
};

TEST_F(UserConnectedTestSuite, shallShowMainMenuWhenConnected)
{
    // see TestSuite c-tor
}

TEST_F(UserConnectedTestSuite, shallIgnoreAcceptWhenNothingSelected)
{
    EXPECT_CALL(listViewModeMock, getCurrentItemIndex()).WillOnce(Return(std::make_pair(false, 0)));

    ASSERT_TRUE(acceptCallback);
    acceptCallback();
}

TEST_F(UserConnectedTestSuite, shallCallViewSmsesWhenSelectedFromMenu)
{
    EXPECT_CALL(listViewModeMock, getCurrentItemIndex()).WillOnce(Return(std::make_pair(true, User::VIEW_SMSES_MENU_INDEX)));
    EXPECT_CALL(userReceiverMock, handleViewSmses());

    ASSERT_TRUE(acceptCallback);
    acceptCallback();
}

TEST_F(UserConnectedTestSuite, shallCallEditSmsWhenSelectedFromMenu)
{
    EXPECT_CALL(listViewModeMock, getCurrentItemIndex()).WillOnce(Return(std::make_pair(true, User::SEND_SMS_MENU_INDEX)));
    EXPECT_CALL(userReceiverMock, handleEditSms(noneSmsEncryption.encoderMatch));

    ASSERT_TRUE(acceptCallback);
    acceptCallback();
}

TEST_F(UserConnectedTestSuite, shallCallEditSmsXorWhenSelectedFromMenu)
{
    EXPECT_CALL(listViewModeMock, getCurrentItemIndex()).WillOnce(Return(std::make_pair(true, User::SEND_SMS_XOR_MENU_INDEX)));
    EXPECT_CALL(userReceiverMock, handleEditSms(xorSmsEncryption.encoderMatch));

    ASSERT_TRUE(acceptCallback);
    acceptCallback();
}

TEST_F(UserConnectedTestSuite, shallCallEditSmsCaesarWhenSelectedFromMenu)
{
    EXPECT_CALL(listViewModeMock, getCurrentItemIndex()).WillOnce(Return(std::make_pair(true, User::SEND_SMS_CAESAR_MENU_INDEX)));
    EXPECT_CALL(userReceiverMock, handleEditSms(caesarSmsEncryption.encoderMatch));

    ASSERT_TRUE(acceptCallback);
    acceptCallback();
}

TEST_F(UserConnectedTestSuite, shallCallEditSmsRsaWhenSelectedFromMenu)
{
    EXPECT_CALL(listViewModeMock, getCurrentItemIndex()).WillOnce(Return(std::make_pair(true, User::SEND_SMS_RSA_MENU_INDEX)));
    EXPECT_CALL(userReceiverMock, handleEditSms(rsaSmsEncryption.encoderMatch));

    ASSERT_TRUE(acceptCallback);
    acceptCallback();
}

TEST_F(UserConnectedTestSuite, shallCallDialWhenSelectedFromMenu)
{
    EXPECT_CALL(listViewModeMock, getCurrentItemIndex()).WillOnce(Return(std::make_pair(true, User::CALL_MENU_INDEX)));
    EXPECT_CALL(userReceiverMock, handleDialing(noneCallDecryption.decoderMatch));

    ASSERT_TRUE(acceptCallback);
    acceptCallback();
}

TEST_F(UserConnectedTestSuite, shallCallDialXorWhenSelectedFromMenu)
{
    EXPECT_CALL(listViewModeMock, getCurrentItemIndex()).WillOnce(Return(std::make_pair(true, User::CALL_XOR_MENU_INDEX)));
    EXPECT_CALL(userReceiverMock, handleDialing(xorCallDecryption.decoderMatch));

    ASSERT_TRUE(acceptCallback);
    acceptCallback();
}

TEST_F(UserConnectedTestSuite, shallCallDialCaesarWhenSelectedFromMenu)
{
    EXPECT_CALL(listViewModeMock, getCurrentItemIndex()).WillOnce(Return(std::make_pair(true, User::CALL_CAESAR_MENU_INDEX)));
    EXPECT_CALL(userReceiverMock, handleDialing(caesarCallDecryption.decoderMatch));

    ASSERT_TRUE(acceptCallback);
    acceptCallback();
}

TEST_F(UserConnectedTestSuite, shallCallDialRsaWhenSelectedFromMenu)
{
    EXPECT_CALL(listViewModeMock, getCurrentItemIndex()).WillOnce(Return(std::make_pair(true, User::CALL_RSA_MENU_INDEX)));
    EXPECT_CALL(userReceiverMock, handleDialing(rsaCallDecryption.decoderMatch));

    ASSERT_TRUE(acceptCallback);
    acceptCallback();
}

class UserShowSmsesTestSuite : public UserWithReceiverTestSuite
{
protected:
    const IUeGui::IListViewMode::Selection FIRST_SMS_VIEW_INDEX = 0;
    const ISmsDB::Index FIRST_SMS_INDEX = 12;
    const std::string FIRST_SMS_TEXT = "first";

    const IUeGui::IListViewMode::Selection SECOND_SMS_VIEW_INDEX = 1;
    const ISmsDB::Index SECOND_SMS_INDEX = 23;
    const std::string SECOND_SMS_TEXT = "second";

    UserShowSmsesTestSuite()
    {
        EXPECT_CALL(ueGuiMock, setListViewMode()).WillOnce(ReturnRef(listViewModeMock));
        EXPECT_CALL(listViewModeMock, clearSelectionList());
        EXPECT_CALL(smsDBMock, visitAll(_)).WillOnce(
                    Invoke([&](auto visitSms)
        {
            visitSms(FIRST_SMS_INDEX, FIRST_SMS_TEXT);
            visitSms(SECOND_SMS_INDEX, SECOND_SMS_TEXT);
        }));

        EXPECT_CALL(listViewModeMock, addSelectionListItem(FIRST_SMS_TEXT, _));
        EXPECT_CALL(listViewModeMock, addSelectionListItem(SECOND_SMS_TEXT, _));

        objectUnderTest.showSmses();
    }
};

TEST_F(UserShowSmsesTestSuite, shallShowSmses)
{
    // See test suite constructor above
}

TEST_F(UserShowSmsesTestSuite, shallSelectFirstSms)
{
    EXPECT_CALL(listViewModeMock, getCurrentItemIndex()).WillOnce(Return(std::make_pair(true, FIRST_SMS_VIEW_INDEX)));
    EXPECT_CALL(userReceiverMock, handleViewSms(FIRST_SMS_INDEX));

    ASSERT_TRUE(acceptCallback);
    acceptCallback();
}

TEST_F(UserShowSmsesTestSuite, shallSelectSecondSms)
{
    EXPECT_CALL(listViewModeMock, getCurrentItemIndex()).WillOnce(Return(std::make_pair(true, SECOND_SMS_VIEW_INDEX)));
    EXPECT_CALL(userReceiverMock, handleViewSms(SECOND_SMS_INDEX));

    ASSERT_TRUE(acceptCallback);
    acceptCallback();
}

TEST_F(UserShowSmsesTestSuite, shallIgnoreAcceptWhenNothingSelected)
{
    EXPECT_CALL(listViewModeMock, getCurrentItemIndex()).WillOnce(Return(std::make_pair(false, 0)));

    ASSERT_TRUE(acceptCallback);
    acceptCallback();
}

class UserEditSmsTestSuite : public UserWithReceiverTestSuite
{
protected:
    UserEditSmsTestSuite()
    {
        EXPECT_CALL(ueGuiMock, setSmsComposeMode()).WillOnce(ReturnRef(smsComposeModeMock));
        EXPECT_CALL(smsComposeModeMock, clearSmsText());

        objectUnderTest.editSms();
    }
};

TEST_F(UserEditSmsTestSuite, shallEditSms)
{
    // see TestSuite c-tor
}

TEST_F(UserEditSmsTestSuite, shallSendSms)
{
    EXPECT_CALL(smsComposeModeMock, getPhoneNumber()).WillOnce(Return(PEER_PHONE_NUMBER));
    EXPECT_CALL(smsComposeModeMock, getSmsText()).WillOnce(Return(SMS_TEXT));
    EXPECT_CALL(userReceiverMock, handleSendSms(PEER_PHONE_NUMBER, SMS_TEXT));

    ASSERT_TRUE(acceptCallback);
    acceptCallback();
}

TEST_F(UserEditSmsTestSuite, shallIgnoreInvalidPhoneNumber)
{
    EXPECT_CALL(smsComposeModeMock, getPhoneNumber()).WillOnce(Return(common::PhoneNumber{}));

    ASSERT_TRUE(acceptCallback);
    acceptCallback();
}

class PeerCallingTestSuite : public UserWithReceiverTestSuite
{
protected:
    PeerCallingTestSuite()
    {
        EXPECT_CALL(ueGuiMock, setAlertMode()).WillOnce(ReturnRef(textModeMock));
        EXPECT_CALL(textModeMock, setText(
                        AllOf(
                            HasSubstr(to_string(PEER_PHONE_NUMBER)),
                            HasSubstr("from"),
                            HasSubstr("Call"))));

        objectUnderTest.showPeerCalling(PEER_PHONE_NUMBER);
    }
};

TEST_F(PeerCallingTestSuite, shallShowAlertMode)
{
    // see TestSuite c-tor
}

TEST_F(PeerCallingTestSuite, shallHandleUserAccept)
{
    EXPECT_CALL(userReceiverMock, handleUserAccept());

    ASSERT_TRUE(acceptCallback);
    acceptCallback();
}

class UserTalkingTestSuite : public UserWithReceiverTestSuite
{
protected:
    const std::string USER_TEXT = "user text";
    Sequence clearTextSequence;
    UserTalkingTestSuite()
    {
        EXPECT_CALL(ueGuiMock, setCallMode()).WillRepeatedly(ReturnRef(callModeMock));
        EXPECT_CALL(callModeMock, clearOutgoingText()).InSequence(clearTextSequence);

        objectUnderTest.showTalkingView(PEER_PHONE_NUMBER);
    }
};

TEST_F(UserTalkingTestSuite, shallShowCallMode)
{
    // see TestSuite c-tor
}

TEST_F(UserTalkingTestSuite, shallAddPeerUserText)
{
    EXPECT_CALL(callModeMock, appendIncomingText(
                    AllOf(HasSubstr(USER_TEXT),
                          HasSubstr(to_string(PEER_PHONE_NUMBER)))));


    objectUnderTest.addPeerUserTalk(PEER_PHONE_NUMBER, USER_TEXT);
}

TEST_F(UserTalkingTestSuite, shallAddUserText)
{
    EXPECT_CALL(callModeMock, appendIncomingText(
                    AllOf(HasSubstr(USER_TEXT),
                          HasSubstr(to_string(PEER_PHONE_NUMBER)))));

    objectUnderTest.addUserTalk(PEER_PHONE_NUMBER, USER_TEXT);
}

TEST_F(UserTalkingTestSuite, shallHandleUserText)
{
    EXPECT_CALL(callModeMock, getOutgoingText()).WillOnce(Return(USER_TEXT));
    EXPECT_CALL(callModeMock, clearOutgoingText()).InSequence(clearTextSequence);

    EXPECT_CALL(userReceiverMock, handleUserTalk(USER_TEXT));

    ASSERT_TRUE(acceptCallback);
    acceptCallback();
}

class UserDialingTestSuite : public UserWithReceiverTestSuite
{
protected:
    UserDialingTestSuite()
    {
        EXPECT_CALL(ueGuiMock, setDialMode()).Times(AtLeast(1)).WillRepeatedly(ReturnRef(dialModeMock));
        objectUnderTest.showDialing();
    }
};

TEST_F(UserDialingTestSuite, shallShowDialMode)
{
    // see TestSuite c-tor
}

TEST_F(UserDialingTestSuite, shallHandleUserCalling)
{
    EXPECT_CALL(dialModeMock, getPhoneNumber()).WillRepeatedly(Return(PEER_PHONE_NUMBER));
    EXPECT_CALL(userReceiverMock, handleUserCalling(PEER_PHONE_NUMBER));

    ASSERT_TRUE(acceptCallback);
    acceptCallback();
}

TEST_F(UserDialingTestSuite, shallIgnoreEmptyPhoneNumber)
{
    EXPECT_CALL(dialModeMock, getPhoneNumber()).WillRepeatedly(Return(common::PhoneNumber{}));

    ASSERT_TRUE(acceptCallback);
    acceptCallback();
}

class UserCallingTestSuite : public UserWithReceiverTestSuite
{
protected:
    UserCallingTestSuite()
    {
        EXPECT_CALL(ueGuiMock, setAlertMode()).WillOnce(ReturnRef(textModeMock));
        EXPECT_CALL(textModeMock, setText(
                        AllOf(
                            HasSubstr(to_string(PEER_PHONE_NUMBER)),
                            HasSubstr("Calling"))));

        objectUnderTest.showUserCalling(PEER_PHONE_NUMBER);
    }
};

TEST_F(UserCallingTestSuite, shallShowAlert)
{
    // see TestSuite c-tor
}

TEST_F(UserCallingTestSuite, shallHandleUserAccept)
{
    EXPECT_CALL(userReceiverMock, handleUserAccept());

    ASSERT_TRUE(acceptCallback);
    acceptCallback();
}

}

