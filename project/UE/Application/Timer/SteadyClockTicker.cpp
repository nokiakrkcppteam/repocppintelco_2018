#include "SteadyClockTicker.hpp"

#include <thread>

namespace ue
{

SteadyClockTicker::SteadyClockTicker(ITicker::Duration oneTick, common::ILogger &logger)
    : oneTick(oneTick), logger(logger, "[Ticker] ")
{}

ITicker::TimePoint SteadyClockTicker::getTimeAfter(Duration duration) const
{
    auto clockTime = std::chrono::steady_clock::now() + duration;
    return std::chrono::duration_cast<TimePoint>(clockTime.time_since_epoch());
}

bool SteadyClockTicker::isExpired(ITicker::TimePoint timePoint) const
{
    std::chrono::steady_clock::time_point clockTimePoint(timePoint);
    return std::chrono::steady_clock::now() >= clockTimePoint;
}

void SteadyClockTicker::waitOneTick()
{
    logger.logDebug("tick");
    std::this_thread::sleep_for(oneTick);
}

}
