#pragma once

namespace ue
{

class IUserReceiver;

class  IUserCommands
{
public:
    virtual ~IUserCommands() = default;

    virtual void setReceiver(IUserReceiver&) = 0;
    virtual void resetReceiver() = 0;

    class ScopeGuard
    {
    public:
        ScopeGuard(IUserCommands&commands, IUserReceiver&receiver);
        ~ScopeGuard();
    private:
        IUserCommands& userCommands;
    };
};

inline IUserCommands::ScopeGuard::ScopeGuard(IUserCommands &commands, IUserReceiver &receiver)
    : userCommands(commands)
{
    userCommands.setReceiver(receiver);
}

inline IUserCommands::ScopeGuard::~ScopeGuard()
{
    userCommands.resetReceiver();
}

}
