#pragma once

#include <gmock/gmock.h>
#include "Timer/ITimers.hpp"

namespace ue
{

struct ITimersMock : public ITimers
{
    ITimersMock();
    ~ITimersMock() override;

    MOCK_METHOD0(startAttachTimer, void());
    MOCK_METHOD0(stopAttachTimer, void());

    MOCK_METHOD0(startPeerCallingTimer, void());
    MOCK_METHOD0(stopPeerCallingTimer, void());

    MOCK_METHOD0(startUserCallingTimer, void());
    MOCK_METHOD0(stopUserCallingTimer, void());

    MOCK_METHOD0(startTalkingTimer, void());
    MOCK_METHOD0(stopTalkingTimer, void());

    MOCK_METHOD0(timeoutExpiredTimers, void());
};

}
