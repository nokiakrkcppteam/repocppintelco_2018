#include "EncryptionKind.hpp"

namespace ue
{

std::ostream &operator <<(std::ostream &os, EncryptionKind kind)
{
    switch (kind)
    {
    case EncryptionKind::None:
        return os << "None";
    case EncryptionKind::Caesar:
        return os << "Caesar";
    case EncryptionKind::Xor:
        return os << "Xor";
    case EncryptionKind::Rsa:
        return os << "Rsa";
    default:
        return os << "UnknownKind(" << static_cast<int>(kind) << ")";
    }
}

}
