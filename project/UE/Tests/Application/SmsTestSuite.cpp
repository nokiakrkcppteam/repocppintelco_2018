#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "Sms/Sms.hpp"

namespace ue
{
using namespace ::testing;

class SmsTestSuite : public Test
{
protected:
    const common::PhoneNumber PHONE_NUMBER{78};
    const common::PhoneNumber FROM_PHONE_NUMBER{12};
    const common::PhoneNumber TO_PHONE_NUMBER{34};

    const std::string textThatShouldNotBeVisibleInShortVersion = "abracadabra";
    const std::string shortText = "AAA";
    const std::string twoLineText = shortText + "\n" + textThatShouldNotBeVisibleInShortVersion;
    const std::string longFirstLineText = shortText + " something very very long " + textThatShouldNotBeVisibleInShortVersion;

    Matcher<std::string> hasToNumber = AllOf(HasSubstr("To: "), HasSubstr(to_string(TO_PHONE_NUMBER)));
    Matcher<std::string> hasFromNumber = AllOf(HasSubstr("From: "), HasSubstr(to_string(FROM_PHONE_NUMBER)));
    Matcher<std::string> hasBothNumbers = AllOf(HasSubstr(to_string(PHONE_NUMBER)), HasSubstr("To: "), HasSubstr("From: "));

    struct SmsUnderTest
    {
        SmsUnderTest(common::PhoneNumber from, common::PhoneNumber to, std::string text)
            :from(from),
              to(to),
              text(text),
              sms(from, to, text)
        {}
        common::PhoneNumber from;
        common::PhoneNumber to;
        std::string text;
        Sms sms;
        std::string sentShortRepresentation = sms.toShortString(from);
        std::string receivedShortRepresentation = sms.toShortString(to);
        std::string sentFullRepresentation = sms.toFullString(from);
        std::string receivedFullRepresentation = sms.toFullString(to);

        Matcher<std::string> hasText = HasSubstr(text);
    };

    SmsUnderTest shortSms{FROM_PHONE_NUMBER, TO_PHONE_NUMBER, shortText};
    SmsUnderTest twoLineTextSms{FROM_PHONE_NUMBER, TO_PHONE_NUMBER, twoLineText};
    SmsUnderTest longFirstLineTextSms{FROM_PHONE_NUMBER, TO_PHONE_NUMBER, longFirstLineText};

    SmsUnderTest smsFromAndToSameNumber{PHONE_NUMBER, PHONE_NUMBER, shortText};
};

TEST_F(SmsTestSuite, smsShallContainOnlyImportantNumbersInShortVersion)
{
    EXPECT_THAT(shortSms.receivedShortRepresentation, AllOf(hasFromNumber, Not(hasToNumber)));
    EXPECT_THAT(twoLineTextSms.receivedShortRepresentation, AllOf(hasFromNumber, Not(hasToNumber)));
    EXPECT_THAT(longFirstLineTextSms.receivedShortRepresentation, AllOf(hasFromNumber, Not(hasToNumber)));

    EXPECT_THAT(shortSms.sentShortRepresentation, AllOf(hasToNumber, Not(hasFromNumber)));
    EXPECT_THAT(twoLineTextSms.sentShortRepresentation, AllOf(hasToNumber, Not(hasFromNumber)));
    EXPECT_THAT(longFirstLineTextSms.sentShortRepresentation, AllOf(hasToNumber, Not(hasFromNumber)));
}

TEST_F(SmsTestSuite, smsToItselfShallContainBotNumbersInShortVersion)
{
    EXPECT_THAT(smsFromAndToSameNumber.receivedShortRepresentation, hasBothNumbers);
    EXPECT_THAT(smsFromAndToSameNumber.sentShortRepresentation, hasBothNumbers);
}

TEST_F(SmsTestSuite, shortSmsShallContainFullTextInShortVersion)
{
    EXPECT_THAT(shortSms.receivedShortRepresentation, HasSubstr(shortSms.text));
    EXPECT_THAT(shortSms.sentShortRepresentation, HasSubstr(shortSms.text));
}

TEST_F(SmsTestSuite, twoLineSmsShallContainOnlyFirstLineInShortVersion)
{
    Matcher<std::string> hasOnlyFirstLine = AllOf(HasSubstr(shortText), Not(HasSubstr(textThatShouldNotBeVisibleInShortVersion)));
    EXPECT_THAT(twoLineTextSms.receivedShortRepresentation, hasOnlyFirstLine);
    EXPECT_THAT(twoLineTextSms.sentShortRepresentation, hasOnlyFirstLine);
}

TEST_F(SmsTestSuite, smsWithLongFirstLineShallContainOnlyShortPrefixOfThatLine)
{
    Matcher<std::string> hasOnlyPrefixOfFirstLine = AllOf(HasSubstr(shortText), Not(HasSubstr(textThatShouldNotBeVisibleInShortVersion)));
    EXPECT_THAT(longFirstLineTextSms.receivedShortRepresentation, hasOnlyPrefixOfFirstLine);
    EXPECT_THAT(longFirstLineTextSms.sentShortRepresentation, hasOnlyPrefixOfFirstLine);
}

TEST_F(SmsTestSuite, smsShallContainOnlyImportantNumbersInFullVersion)
{
    EXPECT_THAT(shortSms.receivedFullRepresentation, AllOf(hasFromNumber, Not(hasToNumber)));
    EXPECT_THAT(twoLineTextSms.receivedFullRepresentation, AllOf(hasFromNumber, Not(hasToNumber)));
    EXPECT_THAT(longFirstLineTextSms.receivedFullRepresentation, AllOf(hasFromNumber, Not(hasToNumber)));

    EXPECT_THAT(shortSms.sentFullRepresentation, AllOf(hasToNumber, Not(hasFromNumber)));
    EXPECT_THAT(twoLineTextSms.sentFullRepresentation, AllOf(hasToNumber, Not(hasFromNumber)));
    EXPECT_THAT(longFirstLineTextSms.sentFullRepresentation, AllOf(hasToNumber, Not(hasFromNumber)));
}

TEST_F(SmsTestSuite, smsToItselfShallContainBotNumbersInFullVersion)
{
    EXPECT_THAT(smsFromAndToSameNumber.receivedFullRepresentation, hasBothNumbers);
    EXPECT_THAT(smsFromAndToSameNumber.sentFullRepresentation, hasBothNumbers);
}

TEST_F(SmsTestSuite, smsShallContainFullTextInFullVersion)
{
    EXPECT_THAT(shortSms.receivedFullRepresentation, shortSms.hasText);
    EXPECT_THAT(twoLineTextSms.receivedFullRepresentation, twoLineTextSms.hasText);
    EXPECT_THAT(longFirstLineTextSms.receivedFullRepresentation, longFirstLineTextSms.hasText);

    EXPECT_THAT(shortSms.sentFullRepresentation, shortSms.hasText);
    EXPECT_THAT(twoLineTextSms.sentFullRepresentation, twoLineTextSms.hasText);
    EXPECT_THAT(longFirstLineTextSms.sentFullRepresentation, longFirstLineTextSms.hasText);
}

}

