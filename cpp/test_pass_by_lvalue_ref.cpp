#include <iostream>
#include "AA.hpp"

void printAllOnRef(AA& aa) // Note & between type and variable
{
    for (int a : aa)
        std::cout << a << '-';
    std::cout << '\n';

    aa.push_back(3);
}

int main() 
{
    AA aa;
    aa.push_back(2);

    std::cout << ">> Function arguments passed by l-value reference: \n";
    // In the following two calls
    // it is expected second print will differ - since printAllOnLvalueRef
    // because "aa.push_back(3)" has observable effect to aa
    // on level of main:
    printAllOnRef(aa);
    printAllOnRef(aa);
}
