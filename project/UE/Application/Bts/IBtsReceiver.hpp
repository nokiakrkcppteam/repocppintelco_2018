#pragma once

#include "Messages.hpp"
#include "Crypto/IEncryption.hpp"

namespace ue
{

class IBtsReceiver
{
public:
    virtual ~IBtsReceiver() = default;

    virtual void handleSib() = 0;
    virtual void handleAttachAccept() = 0;
    virtual void handleAttachReject() = 0;
    virtual void handleSmsReceived(common::PhoneNumber from, IDecoderPtr smsDecoder, const std::string& text) = 0;
    virtual void handleCallRequestFrom(common::PhoneNumber from, IEncoderPtr callEncoder) = 0;
    virtual void handleCallAcceptFrom(common::PhoneNumber from, IEncoderPtr callEncoder) = 0;
    virtual void handleCallTalkFrom(common::PhoneNumber from, const std::string& text) = 0;
    virtual void handleCallDropFrom(common::PhoneNumber from) = 0;
};

}
