#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <mutex>
#include <condition_variable>
#include <memory>

#include "Timer/TimersRunner.hpp"
#include "Mocks/ILoggerMock.hpp"
#include "Mocks/ITimersMock.hpp"
#include "Mocks/ITickerMock.hpp"

namespace ue
{
using namespace ::testing;
using namespace std::chrono_literals;

class TimersRunnerTestSuite : public TestWithParam<unsigned>
{
protected:
    struct Semaphore
    {
    public:
        void notify()
        {
            std::unique_lock<std::mutex> lock(guard);
            ++count;
            queue.notify_one();
        }
        void notify(std::size_t times)
        {
            std::unique_lock<std::mutex> lock(guard);
            count += times;
            queue.notify_one();
        }
        void wait()
        {
            std::unique_lock<std::mutex> lock(guard);
            queue.wait(lock, [this] { return count > 0; });
            --count;
        }
        void wait(std::size_t times)
        {
            while (times-- > 0)
                wait();
        }

    private:
        std::size_t count = 0u;
        std::condition_variable queue;
        std::mutex guard;
    };

    NiceMock<common::ILoggerMock> loggerMock;
    StrictMock<ITimersMock> timersMock;
    StrictMock<ITickerMock> tickerMock;

    Semaphore tickerWaitGuard;
    Semaphore testWaitGuard;

    Expectation tickerTicking = EXPECT_CALL(tickerMock, waitOneTick()).WillRepeatedly(Invoke([this]
    {
        tickerWaitGuard.wait();
        testWaitGuard.notify();
    }));

    std::unique_ptr<TimersRunner> objectUnderTest;

    void SetUp() override
    {
        objectUnderTest.reset(new TimersRunner{tickerMock, timersMock, loggerMock});

    }
    void TearDown() override
    {
        EXPECT_CALL(timersMock, timeoutExpiredTimers()).Times(AtMost(2)); // if fast enough
        tickerWaitGuard.notify();
        objectUnderTest.reset();
    }
};

TEST_P(TimersRunnerTestSuite, shallRunNumberOfTimes)
{
    const auto NUMBER_OF_TIMES = GetParam();
    EXPECT_CALL(timersMock, timeoutExpiredTimers()).Times(NUMBER_OF_TIMES);
    tickerWaitGuard.notify(NUMBER_OF_TIMES);
    testWaitGuard.wait(NUMBER_OF_TIMES);
}

INSTANTIATE_TEST_CASE_P(runNumberOfTimes,
                        TimersRunnerTestSuite,
                        Range(0u, 10u));

}

