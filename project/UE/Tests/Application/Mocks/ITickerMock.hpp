#pragma once

#include <gmock/gmock.h>
#include "Timer/ITicker.hpp"

namespace ue
{

struct ITickerMock : public ITicker
{
    ITickerMock();
    ~ITickerMock() override;

    MOCK_CONST_METHOD1(getTimeAfter, TimePoint(Duration));
    MOCK_CONST_METHOD1(isExpired, bool(TimePoint));
    MOCK_METHOD0(waitOneTick, void());
};

}
