#pragma once

#include <chrono>

namespace ue
{

class ITicker
{
public:
    using Duration = std::chrono::milliseconds;
    using TimePoint = Duration;

    virtual ~ITicker() = default;

    virtual TimePoint getTimeAfter(Duration) const = 0;
    virtual bool isExpired(TimePoint) const = 0;
    virtual void waitOneTick() = 0;
};

}
