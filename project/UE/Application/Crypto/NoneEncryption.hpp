#pragma once

#include "IEncryption.hpp"

namespace ue
{

class NoneEncryption : public IEncoder, public IDecoder
{
public:
    EncryptionKind getKind() const override;
    std::string encodeSms(const std::string &text) const override;
    std::string decodeSms(const std::string &text) const override;
    std::string encodeTalk(const std::string &text) const override;
    std::string decodeTalk(const std::string &text) const override;
    void writeTo(common::OutgoingMessage &builder) const override;
    void printData(std::ostream& os) const;
};

}
