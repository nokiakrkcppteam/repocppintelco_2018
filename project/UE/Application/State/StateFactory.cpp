#include "StateFactory.hpp"

#include "NotConnectedState.hpp"
#include "ConnectingState.hpp"
#include "ConnectedState.hpp"
#include "EditSmsState.hpp"
#include "ViewSmsState.hpp"
#include "ViewSmsesState.hpp"
#include "PeerCallingState.hpp"
#include "UserCallingState.hpp"
#include "TalkingState.hpp"
#include "DialingState.hpp"

namespace ue
{

StateFactory::State StateFactory::createNotConnectedState(StateContext &stateContext)
{
    return std::make_unique<NotConnectedState>(stateContext);
}

StateFactory::State StateFactory::createConnectingState(StateContext &stateContext)
{
    return std::make_unique<ConnectingState>(stateContext);
}

StateFactory::State StateFactory::createConnectedState(StateContext &stateContext)
{
    return std::make_unique<ConnectedState>(stateContext);
}

StateFactory::State StateFactory::createEditSmsState(StateContext &stateContext, IEncoderPtr smsEncoder)
{
    return std::make_unique<EditSmsState>(stateContext, smsEncoder);
}

StateFactory::State StateFactory::createViewSmsState(StateContext &stateContext, ISmsDB::Index smsIndex)
{
    return std::make_unique<ViewSmsState>(stateContext, smsIndex);
}

StateFactory::State StateFactory::createViewSmsesState(StateContext &stateContext)
{
    return std::make_unique<ViewSmsesState>(stateContext);
}

StateFactory::State StateFactory::createPeerCallingState(StateContext &stateContext,
                                                         common::PhoneNumber from,
                                                         IEncoderPtr talkEncoder)
{
    return std::make_unique<PeerCallingState>(stateContext, from, talkEncoder);
}

StateFactory::State StateFactory::createTalkingState(StateContext &stateContext,
                                                     common::PhoneNumber to,
                                                     IEncoderPtr talkEncoder,
                                                     IDecoderPtr talkDecoder)
{
    return std::make_unique<TalkingState>(stateContext, to, talkEncoder, talkDecoder);
}

StateFactory::State StateFactory::createDialingState(StateContext &stateContext,
                                                     IDecoderPtr talkDecoder)
{
    return std::make_unique<DialingState>(stateContext, talkDecoder);
}

StateFactory::State StateFactory::createUserCallingState(StateContext &stateContext,
                                                         common::PhoneNumber to,
                                                         IDecoderPtr talkDecoder)
{
    return std::make_unique<UserCallingState>(stateContext, to, talkDecoder);
}

}
