#include "RsaEncryption.hpp"
#include "Messages/OutgoingMessage.hpp"
#include "EncryptionKind.hpp"

namespace ue
{

RsaEncryption::RsaEncryption(const RsaDetails::KeyPair &keyPair)
    : rsaKeys(keyPair)
{}

EncryptionKind RsaEncryption::getKind() const
{
    return EncryptionKind::Rsa;
}

std::string RsaEncryption::encodeSms(const std::string &text) const
{
    return rsaKeys.encryptWithPrivateKey(text);
}

std::string RsaEncryption::decodeSms(const std::string &text) const
{
    return rsaKeys.decryptWithPublicKey(text);
}

std::string RsaEncryption::encodeTalk(const std::string &text) const
{
    return rsaKeys.encryptWithPublicKey(text);
}

std::string RsaEncryption::decodeTalk(const std::string &text) const
{
    return rsaKeys.decryptWithPrivateKey(text);
}

void RsaEncryption::writeTo(common::OutgoingMessage &builder) const
{
    builder.writeNumber(static_cast<std::uint8_t>(EncryptionKind::Rsa));
    rsaKeys.dump(builder);
}

void RsaEncryption::printData(std::ostream &os) const
{
    os << "RsaEncryption(";
    rsaKeys.dump(os);
    os << ")";
}

}
