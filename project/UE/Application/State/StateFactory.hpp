#pragma once

#include "IEventsReceiver.hpp"
#include <memory>

namespace ue
{

class StateContext;

class StateFactory
{
public:
    using State = std::unique_ptr<IEventsReceiver>;

    State createNotConnectedState(StateContext& stateContext);
    State createConnectingState(StateContext& stateContext);
    State createConnectedState(StateContext& stateContext);
    State createEditSmsState(StateContext &stateContext, IEncoderPtr smsEncoder);
    State createViewSmsState(StateContext &stateContext, ISmsDB::Index smsIndex);
    State createViewSmsesState(StateContext &stateContext);
    State createPeerCallingState(StateContext &stateContext, common::PhoneNumber from, IEncoderPtr talkEncoder);
    State createTalkingState(StateContext &stateContext, common::PhoneNumber to, IEncoderPtr talkEncoder, IDecoderPtr talkDecoder);
    State createUserCallingState(StateContext &stateContext, common::PhoneNumber to, IDecoderPtr talkDecoder);
    State createDialingState(StateContext &stateContext, IDecoderPtr talkDecoder);
};

}
