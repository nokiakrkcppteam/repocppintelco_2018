#pragma once

#include "ITimers.hpp"
#include "ITicker.hpp"
#include "Logger/PrefixedLogger.hpp"

#include <thread>
#include <atomic>

namespace ue
{

class TimersRunner
{
public:
    TimersRunner(ITicker& ticker, ITimers& timers, common::ILogger& logger);
    ~TimersRunner();

private:
    void localThreadBody();

    ITicker& ticker;
    ITimers& timers;
    common::PrefixedLogger logger;

    std::atomic_bool running{true};
    std::thread localThread;
};

}
