#include "BtsSender.hpp"
#include "Messages/OutgoingMessage.hpp"

namespace ue
{

BtsSender::BtsSender(common::PhoneNumber phoneNumber,
                     common::ITransport& ITransport,
                     common::ILogger &iLogger)
    : phoneNumber(phoneNumber),
      transport(ITransport),
      logger(iLogger, [this](std::ostream& os) { printPrefix(os); })
{
    logger.logInfo("Started");
}

BtsSender::~BtsSender()
{
    logger.logInfo("Stopped");
}

void BtsSender::sendAttachRequest()
{
    logger.logInfo("Send attach request");
    common::OutgoingMessage builder(common::MessageId::AttachRequest,
                                    phoneNumber,
                                    PhoneNumber{});
    transport.sendMessage(builder.getMessage());
}

void BtsSender::sendSms(common::PhoneNumber to, IEncoderPtr smsEncoder, const std::string &smsText)
{
    logger.logInfo("Send Sms to: ", to);
    common::OutgoingMessage builder(common::MessageId::Sms,
                                    phoneNumber,
                                    to);
    smsEncoder->writeTo(builder);
    builder.writeText(smsEncoder->encodeSms(smsText));
    transport.sendMessage(builder.getMessage());
}

void BtsSender::sendCallRequest(common::PhoneNumber to, IDecoderPtr talkDecoder)
{
    logger.logInfo("Send CallRequest to: ", to, ", enc: ", *talkDecoder);
    common::OutgoingMessage builder(common::MessageId::CallRequest,
                                    phoneNumber,
                                    to);
    talkDecoder->writeTo(builder);
    transport.sendMessage(builder.getMessage());
}

void BtsSender::sendCallDrop(common::PhoneNumber to)
{
    logger.logInfo("Send CallDrop to: ", to);
    common::OutgoingMessage builder(common::MessageId::CallDropped,
                                    phoneNumber,
                                    to);
    transport.sendMessage(builder.getMessage());
}

void BtsSender::sendCallAccept(common::PhoneNumber to, IDecoderPtr talkDecoder)
{
    logger.logInfo("Send CallAccept to: ", to, ", enc: ", *talkDecoder);
    common::OutgoingMessage builder(common::MessageId::CallAccepted,
                                    phoneNumber,
                                    to);
    talkDecoder->writeTo(builder);
    transport.sendMessage(builder.getMessage());
}

void BtsSender::sendCallTalk(common::PhoneNumber to, const std::string &callTalk)
{
    logger.logInfo("Send CallTalk to: ", to);
    common::OutgoingMessage builder(common::MessageId::CallTalk,
                                    phoneNumber,
                                    to);
    builder.writeText(callTalk);
    transport.sendMessage(builder.getMessage());
}

void BtsSender::printPrefix(std::ostream &os) const
{
    os << "[BTS:Sender] ";
}

}
