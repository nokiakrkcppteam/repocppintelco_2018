#include "SmsDB.hpp"

namespace ue
{

SmsDB::SmsDB(common::PhoneNumber phoneNumber, common::ILogger &iLogger)
    : phoneNumber(phoneNumber),
      logger(iLogger, [this](std::ostream& os) { printPrefix(os); })
{
    logger.logInfo("Started");
}

SmsDB::~SmsDB()
{
    logger.logInfo("Stopped");
}

ISmsDB::Index SmsDB::addSentSms(common::PhoneNumber to, const std::string &text)
{
    smses.emplace_back(phoneNumber, to, text);
    logger.logDebug("Sms sent: ", smses.back().toShortString(phoneNumber));
    return smses.size() - 1;
}

ISmsDB::Index SmsDB::addReceivedSms(common::PhoneNumber from, const std::string &text)
{
    smses.emplace_back(from, phoneNumber, text);
    logger.logDebug("Sms received: ", smses.back().toShortString(phoneNumber));
    return smses.size() - 1;
}

void SmsDB::visitAll(ISmsDB::Visitor visit) const
{
    std::size_t index = 0;
    for (auto const& sms: smses)
    {
        auto shortForm = sms.toShortString(phoneNumber);
        if (not visit(index, shortForm))
        {
            logger.logDebug("Stopped visiting at: ", index, ", for: ", shortForm);
            break;
        }
        ++index;
    }
    logger.logDebug("All: ", index, " visited.");
}

std::string SmsDB::getSmsShortRepresentation(Index index) const
{
    return smses.at(index).toShortString(phoneNumber);
}

std::string SmsDB::getSmsFullRepresentation(Index index) const
{
    return smses.at(index).toFullString(phoneNumber);
}

void SmsDB::printPrefix(std::ostream &os) const
{
    os << "[SMS-DB] ";
}

}
