#include "StateLocator.hpp"

namespace ue
{

void StateLocator::setState(StateLocator::State newState)
{
    currentState = std::move(newState);
}

IEventsReceiver &StateLocator::getState()
{
    return *currentState;
}

}
