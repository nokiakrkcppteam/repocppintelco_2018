#pragma once

#include "Bts/IBtsReceiver.hpp"
#include "User/IUserReceiver.hpp"
#include "Timer/ITimeoutsReceiver.hpp"

namespace ue
{

class IEventsReceiver : public IBtsReceiver,
                        public IUserReceiver,
                        public ITimeoutsReceiver
{};

}
