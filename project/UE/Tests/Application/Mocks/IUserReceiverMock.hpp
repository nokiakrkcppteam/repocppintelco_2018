#pragma once

#include <gmock/gmock.h>
#include "User/IUserReceiver.hpp"

namespace ue
{

struct IUserReceiverMock : public IUserReceiver
{
    IUserReceiverMock();
    ~IUserReceiverMock() override;

    MOCK_METHOD0(handleExitView, void());
    MOCK_METHOD2(handleSendSms, void(common::PhoneNumber to, const std::string& text));
    MOCK_METHOD1(handleEditSms, void(IEncoderPtr smsEncoder));
    MOCK_METHOD1(handleViewSms, void(ISmsDB::Index smsIndex));
    MOCK_METHOD0(handleViewSmses, void());
    MOCK_METHOD0(handleUserAccept, void());
    MOCK_METHOD1(handleUserTalk, void(const std::string& text));
    MOCK_METHOD1(handleDialing, void(IDecoderPtr talkDecoder));
    MOCK_METHOD1(handleUserCalling, void(common::PhoneNumber to));
};

}
