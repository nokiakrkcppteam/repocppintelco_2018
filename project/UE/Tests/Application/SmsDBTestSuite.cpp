#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "Sms/SmsDB.hpp"
#include "Mocks/ILoggerMock.hpp"

#include <initializer_list>

namespace ue
{
using namespace ::testing;

class SmsDBTestSuite : public Test
{
protected:
    const common::PhoneNumber PHONE_NUMBER{78};
    const common::PhoneNumber FROM_PHONE_NUMBER{12};
    const common::PhoneNumber TO_PHONE_NUMBER{34};

    Matcher<std::string> hasToNumber = AllOf(HasSubstr("To: "), HasSubstr(to_string(TO_PHONE_NUMBER)));
    Matcher<std::string> hasFromNumber = AllOf(HasSubstr("From: "), HasSubstr(to_string(FROM_PHONE_NUMBER)));

    const std::string text1 = "AAA";
    const std::string text2 = "BBB";
    const std::string text3 = "CCC";

    NiceMock<common::ILoggerMock> loggerMock;

    using VisitorMock = MockFunction<bool(ISmsDB::Index, const std::string& shortVersion)>;
    VisitorMock visitorMock;
    void testVisitAll()
    {
        objectUnderTest.visitAll([&](ISmsDB::Index index, const std::string& shortText)
        {
            return visitorMock.Call(index, shortText);
        });
    }
    struct VisitSms
    {
        Matcher<ISmsDB::Index> index;
        Matcher<std::string> text;
        Matcher<std::string> phone;
        bool result = true;
    };
    void testVisitSmses(std::initializer_list<VisitSms> expectedResults)
    {
        for (auto&& expectedResult : expectedResults)
        {
            auto matchRepresentation = AllOf(expectedResult.text, expectedResult.phone);
            EXPECT_CALL(visitorMock, Call(expectedResult.index, matchRepresentation)).WillOnce(Return(expectedResult.result));
        }
        testVisitAll();
    }

    SmsDB objectUnderTest{PHONE_NUMBER, loggerMock};
};

TEST_F(SmsDBTestSuite, shallAddReceivedSmsesFromOneNumberInOrder)
{
    auto index1 = objectUnderTest.addReceivedSms(FROM_PHONE_NUMBER, text1);
    auto index2 = objectUnderTest.addReceivedSms(FROM_PHONE_NUMBER, text2);
    auto index3 = objectUnderTest.addReceivedSms(FROM_PHONE_NUMBER, text3);

    ASSERT_LT(index1, index2);
    ASSERT_LT(index2, index3);


    testVisitSmses({
        {index1, HasSubstr(text1), hasFromNumber },
        {index2, HasSubstr(text2), hasFromNumber },
        {index3, HasSubstr(text3), hasFromNumber }
   });
}

TEST_F(SmsDBTestSuite, shallAddSentSmsesToOneNumberInOrder)
{
    auto index1 = objectUnderTest.addSentSms(TO_PHONE_NUMBER, text1);
    auto index2 = objectUnderTest.addSentSms(TO_PHONE_NUMBER, text2);
    auto index3 = objectUnderTest.addSentSms(TO_PHONE_NUMBER, text3);

    ASSERT_LT(index1, index2);
    ASSERT_LT(index2, index3);

    testVisitSmses({
        {index1, HasSubstr(text1), hasToNumber },
        {index2, HasSubstr(text2), hasToNumber },
        {index3, HasSubstr(text3), hasToNumber }
   });
}

TEST_F(SmsDBTestSuite, shallVisitAllSmses)
{
    objectUnderTest.addSentSms(TO_PHONE_NUMBER, text1);
    objectUnderTest.addReceivedSms(FROM_PHONE_NUMBER, text2);
    objectUnderTest.addSentSms(TO_PHONE_NUMBER, text3);

    testVisitSmses({
        {_, HasSubstr(text1), hasToNumber },
        {_, HasSubstr(text2), hasFromNumber },
        {_, HasSubstr(text3), hasToNumber }
   });
}

TEST_F(SmsDBTestSuite, shallBreakVisitAllSmsesOnFalse)
{
    objectUnderTest.addSentSms(TO_PHONE_NUMBER, text1);
    objectUnderTest.addReceivedSms(FROM_PHONE_NUMBER, text2);
    objectUnderTest.addSentSms(TO_PHONE_NUMBER, text3);

    testVisitSmses({
        {_, HasSubstr(text1), hasToNumber },
        {_, HasSubstr(text2), hasFromNumber, false }
   });
}

TEST_F(SmsDBTestSuite, shallFullRepresentationMatchAddedSmses)
{
    auto index1 = objectUnderTest.addSentSms(TO_PHONE_NUMBER, text1);
    auto index2 = objectUnderTest.addReceivedSms(FROM_PHONE_NUMBER, text2);
    auto index3 = objectUnderTest.addSentSms(TO_PHONE_NUMBER, text3);

    EXPECT_THAT(objectUnderTest.getSmsFullRepresentation(index1), AllOf(hasToNumber, HasSubstr(text1)));
    EXPECT_THAT(objectUnderTest.getSmsFullRepresentation(index2), AllOf(hasFromNumber, HasSubstr(text2)));
    EXPECT_THAT(objectUnderTest.getSmsFullRepresentation(index3), AllOf(hasToNumber, HasSubstr(text3)));
}

TEST_F(SmsDBTestSuite, shallShortRepresentationMatchAddedSmses)
{
    auto index1 = objectUnderTest.addSentSms(TO_PHONE_NUMBER, text1);
    auto index2 = objectUnderTest.addReceivedSms(FROM_PHONE_NUMBER, text2);
    auto index3 = objectUnderTest.addSentSms(TO_PHONE_NUMBER, text3);

    EXPECT_THAT(objectUnderTest.getSmsShortRepresentation(index1), AllOf(hasToNumber, HasSubstr(text1)));
    EXPECT_THAT(objectUnderTest.getSmsShortRepresentation(index2), AllOf(hasFromNumber, HasSubstr(text2)));
    EXPECT_THAT(objectUnderTest.getSmsShortRepresentation(index3), AllOf(hasToNumber, HasSubstr(text3)));
}

}

