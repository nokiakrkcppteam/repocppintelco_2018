#include "Application.hpp"
#include "Bts/BtsSender.hpp"
#include "Bts/BtsPort.hpp"
#include "Sms/SmsDB.hpp"
#include "User/User.hpp"
#include "Timer/Timers.hpp"
#include "Timer/TimersRunner.hpp"
#include "Timer/SteadyClockTicker.hpp"
#include "Crypto/EncryptionFactory.hpp"
#include "ApplicationEnvironmentFactory.hpp"

int main(int argc, char* argv[])
{
    using namespace ue;
    using namespace std::chrono_literals;

    auto appEnv = ue::createApplicationEnvironment(argc, argv);
    auto& logger = appEnv->getLogger();
    auto& transport = appEnv->getTransportToBts();
    auto& ueGui = appEnv->getUeGui();
    auto phoneNumber = appEnv->getMyPhoneNumber();

    EncryptionFactory encryptionFactory(*appEnv, logger);
    SmsDB smsDB(phoneNumber, logger);
    BtsSender btsSender(phoneNumber, transport, logger);
    User user(phoneNumber, ueGui, smsDB, encryptionFactory, logger);
    SteadyClockTicker ticker(50ms, logger);
    Timers timers{ticker, logger};
    Application app(phoneNumber, smsDB, btsSender, user, timers, encryptionFactory, logger);
    BtsPort btsPort(app, phoneNumber, encryptionFactory, transport, logger);

    IUserCommands::ScopeGuard connectUserReceiver(user, app);
    Timers::ReceiverConnector connectTimersReceiver(timers, app);

    TimersRunner timersRunner{ticker, timers, logger};
    appEnv->startMessageLoop();
}

