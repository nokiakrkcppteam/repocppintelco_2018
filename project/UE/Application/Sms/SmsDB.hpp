#pragma once

#include "ISmsDB.hpp"
#include "Sms.hpp"
#include <vector>
#include "Logger/PrefixedLogger.hpp"

namespace ue
{

class SmsDB : public ISmsDB
{
public:
    SmsDB(common::PhoneNumber phoneNumber, common::ILogger& iLogger);
    ~SmsDB() override;

    Index addSentSms(common::PhoneNumber to, const std::string &text) override;
    Index addReceivedSms(common::PhoneNumber from, const std::string &text) override;
    void visitAll(Visitor visit) const override;
    std::string getSmsShortRepresentation(Index index) const override;
    std::string getSmsFullRepresentation(Index index) const override;

private:
    void printPrefix(std::ostream& os) const;

    common::PhoneNumber phoneNumber;
    mutable common::PrefixedLogger logger;
    std::vector<Sms> smses;
};

}
